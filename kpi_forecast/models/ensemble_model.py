import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
from pandas.plotting import register_matplotlib_converters
from sklearn.metrics import mean_squared_error
from sys import platform
from kpi_forecast.models.catboost import CatBoostRegressor
from kpi_forecast.models.lightgbm import LightGbmRegressor
from kpi_forecast.models.prophet import Prophet
from kpi_forecast.models.xgboost import XgBoostRegressor

class EnsembleModel(object):
    """ Ensemble of machine learning models """
    __models = {}
    __parameters = None
    __defaults = {
        'catboost': {
            'model': {
                'iterations': 100,
                'learning_rate': 0.03,
                'l2_leaf_reg': 3.0,
                'depth': 6,
                'one_hot_max_size': 2,
                'colsample_bylevel': 1.0,
                'early_stopping_rounds': 100
            },
            'search': {
                'bayesian': {
                    'learning_rate': (0.01, 0.16),
                    'l2_leaf_reg': (0.5, 8.0),
                    'depth': (4, 64),
                    'one_hot_max_size': (4, 64),
                    'colsample_bylevel': (0.6, 1.0)
                },
                'grid': {
                    'learning_rate': [0.01, 0.02, 0.04, 0.08, 0.16],
                    'l2_leaf_reg': [0.5, 1.0, 2.0, 4.0, 8.0],
                    'depth': [4, 8, 16, 32, 64],
                    'one_hot_max_size': [4, 8, 16, 32, 64],
                    'colsample_bylevel': [0.6, 0.7, 0.8, 0.9, 1.0]
                },
                'random': {
                    'learning_rate': [0.01, 0.02, 0.04, 0.08, 0.16],
                    'l2_leaf_reg': [0.5, 1.0, 2.0, 4.0, 8.0],
                    'depth': [4, 8, 16, 32, 64],
                    'one_hot_max_size': [4, 8, 16, 32, 64],
                    'colsample_bylevel': [0.6, 0.7, 0.8, 0.9, 1.0]
                }
            }
        },
        'lightgbm': {
            'model': {
                'n_estimators': 10000,
                'learning_rate': 0.05,
                'num_leaves': 31,
                'max_depth': -1,
                'min_child_samples': 20,
                'bagging_fraction': 0.9,
                'bagging_freq': 1,
                'feature_fraction': 0.9,
                'early_stopping_rounds': 100,
                'lambda_l1': 0.1
            },
            'search': {
                'bayesian': {
                    'learning_rate': (0.001, 0.1),
                    'num_leaves': (16, 256),
                    'max_depth': (4, 64),
                    'min_child_samples': (8, 128),
                    'min_child_weight': (0.001, 0.1),
                    'bagging_fraction': (0.5, 1.0),
                    'bagging_freq': (1, 8),
                    'feature_fraction': (0.5, 1.0),
                    'lambda_l1': (0.001, 1),
                    'lambda_l2': (0.001, 1),
                    'min_split_gain': (0.001, 1)
                },
                'grid': {
                    'learning_rate': [0.01, 0.02, 0.04, 0.08, 0.16],
                    'num_leaves': [16, 32, 64, 128, 256],
                    'max_depth': [4, 8, 16, 32, 64],
                    'min_child_weight': [0.001, 0.005, 0.01, 0.05, 0.1],
                    'min_child_samples': [8, 16, 32, 64, 128],
                    'bagging_fraction': [0.6, 0.7, 0.8, 0.9, 1.0],
                    'feature_fraction': [0.6, 0.7, 0.8, 0.9, 1.0],
                    'lambda_l1': [0.0, 0.1, 0.2, 0.3, 0.4],
                    'lambda_l2': [0.0, 0.1, 0.2, 0.3, 0.4],
                    'min_split_gain': [0.001, 0.005, 0.01, 0.05, 0.1]
                },
                'random': {
                    'learning_rate': [0.01, 0.02, 0.04, 0.08, 0.16],
                    'num_leaves': [16, 32, 64, 128, 256],
                    'max_depth': [4, 8, 16, 32, 64],
                    'min_child_weight': [0.001, 0.005, 0.01, 0.05, 0.1],
                    'min_child_samples': [8, 16, 32, 64, 128],
                    'bagging_fraction': [0.6, 0.7, 0.8, 0.9, 1.0],
                    'feature_fraction': [0.6, 0.7, 0.8, 0.9, 1.0],
                    'lambda_l1': [0.0, 0.1, 0.2, 0.3, 0.4],
                    'lambda_l2': [0.0, 0.1, 0.2, 0.3, 0.4],
                    'min_split_gain': [0.001, 0.005, 0.01, 0.05, 0.1]
                }
            }
        },
        'prophet': {
            'search': {
                'bayesian': {
                    'n_changepoints': (15, 35),
                    'seasonality_prior_scale': (10, 30),
                    'holidays_prior_scale': (10, 30),
                    'changepoint_prior_scale': (0.005, 0.5),
                    'fourier_order': (5, 25)
                },
                'grid': {
                    'growth': ['linear', 'logistic'],
                    'seasonality_mode': ['additive', 'multiplicative'],
                    'n_changepoints': [15, 20, 35, 30, 35],
                    'seasonality_prior_scale': [10, 15, 20, 25, 30],
                    'holidays_prior_scale': [10, 15, 20, 25, 30],
                    'changepoint_prior_scale': [0.005, 0.01, 0.05, 0.1, 0.5],
                    'fourier_order': [5, 10, 15, 20, 25]
                },
                'random': {
                    'growth': ['linear', 'logistic'],
                    'seasonality_mode': ['additive', 'multiplicative'],
                    'n_changepoints': [15, 20, 35, 30, 35],
                    'seasonality_prior_scale': [10, 15, 20, 25, 30],
                    'holidays_prior_scale': [10, 15, 20, 25, 30],
                    'changepoint_prior_scale': [0.005, 0.01, 0.05, 0.1, 0.5],
                    'fourier_order': [5, 10, 15, 20, 25]
                }
            }
        },
        'xgboost': {
            'model': {
                'learning_rate': 0.3,
                'max_depth': 6,
                'min_child_weight': 1.0,
                'subsample': 1.0,
                'colsample_bytree': 1.0,
                'n_estimators': 100,
                'early_stopping_rounds': 100
            },
            'search': {
                'bayesian': {
                    'learning_rate': (0.01, 0.16),
                    'max_depth': (4, 64),
                    'min_child_weight': (0.5, 8.0),
                    'subsample': (0.6, 1.0),
                    'colsample_bytree': (0.6, 1.0)
                },
                'grid': {
                    'learning_rate': [0.01, 0.02, 0.04, 0.08, 0.16],
                    'max_depth': [4, 8, 16, 32, 64],
                    'min_child_weight': [0.5, 1.0, 2.0, 4.0, 8.0],
                    'subsample': [0.6, 0.7, 0.8, 0.9, 1.0],
                    'colsample_bytree': [0.6, 0.7, 0.8, 0.9, 1.0]
                },
                'random': {
                    'learning_rate': [0.01, 0.02, 0.04, 0.08, 0.16],
                    'max_depth': [4, 8, 16, 32, 64],
                    'min_child_weight': [0.5, 1.0, 2.0, 4.0, 8.0],
                    'subsample': [0.6, 0.7, 0.8, 0.9, 1.0],
                    'colsample_bytree': [0.6, 0.7, 0.8, 0.9, 1.0]
                }
            }
        },
        'parameter_tuning': {
            'bayesian_search': {
                'init_points': 16,
                'n_iter': 16,
                'verbose': 1,
                'random_state': 0
            },
            'grid_search': {
                'processes': -1,
                'folds': 2,
                'verbosity': 1
            },
            'randomized_search': {
                'iterations': 10,
                'processes': -1,
                'folds': 2,
                'verbosity': 1,
                'random_seed': 0
            }
        },
        'setting': {
            'is_incremental': True,
            'is_ensemble': True,
            'ensemble_name': 'ensemble',
            'feature_selection_method': 'permutation',
            'hyperparameter_tuning_methods': ['bayesian'],
            'confidence_interval': 0.9
        }
    }

    def __init__(self, model_names, parameters=None):
        """ Constructor """
        self.__parameters = self.__defaults.copy()
        # Set defined parameter values if present
        if parameters is not None:
            self.__parameters.update(parameters)
        # Instantiate the model(s)
        for model_name in model_names:
            if model_name.lower() == 'catboost':
                self.__models['catboost'] = CatBoostRegressor(self.__parameters['catboost'], self.__parameters['parameter_tuning'])
            elif model_name.lower() == 'lightgbm':
                self.__models['lightgbm'] = LightGbmRegressor(self.__parameters['lightgbm'], self.__parameters['parameter_tuning'])
            elif model_name.lower() == 'prophet':
                self.__models['prophet'] = Prophet(self.__parameters['prophet'], self.__parameters['parameter_tuning'])
            elif model_name.lower() == 'xgboost':
                self.__models['xgboost'] = XgBoostRegressor(self.__parameters['xgboost'], self.__parameters['parameter_tuning'])
        # Create a default CatBoost model if no models are specified
        if len(self.__models.keys()) == 0:
            self.__models['catboost'] = CatBoostRegressor(self.__parameters['catboost'], self.__parameters['parameter_tuning'])

    def get_model_names(self):
        """ Get the list of models """
        return list(self.__models.keys())

    def load_models(self, identifier=None):
        """ Load the trained models from files """
        print('Loading the trained model(s)')
        for model_name in self.__models.keys():
            self.__models[model_name].load_model(identifier=identifier, is_incremental=self.__parameters['setting']['is_incremental'])

    def load_quantile_models(self, identifier=None):
        """ Load the trained quantile regression models from files """
        print('Loading the trained quantile regression model(s)')
        for model_name in self.__models.keys():
            self.__models[model_name].load_quantile_model(identifier=identifier, is_incremental=self.__parameters['setting']['is_incremental'])

    def save_models(self, identifier=None):
        """ Save the trained model(s) into files """
        print('Saving the trained model(s)')
        for model_name in self.__models.keys():
            self.__models[model_name].save_model(identifier=identifier)

    def save_quantile_models(self, identifier=None):
        """ Save the trained quantile regression model(s) into files """
        print('Saving the trained regression model(s)')
        for model_name in self.__models.keys():
            self.__models[model_name].save_quantile_model(identifier=identifier)

    def tune_hyperparameters(self, train_data, train_labels, categorical_features, validate_data=None, validate_labels=None):
        """ Perform hyperparameter tuning """
        tuned_parameters = {}
        for model_name in self.__models.keys():
            if model_name == 'catboost':
                _, tuned_parameters[model_name], best_score = self.__models[model_name].tune_hyperparameters(
                    self.__parameters[model_name]['search'],
                    train_data, train_labels, 
                    categorical_features,
                    validate_data, validate_labels,
                    is_incremental=self.__parameters['setting']['is_incremental'], 
                    methods=self.__parameters['setting']['hyperparameter_tuning_methods']
                )
            elif model_name == 'lightgbm' or model_name == 'xgboost':
                _, tuned_parameters[model_name], best_score = self.__models[model_name].tune_hyperparameters(
                    self.__parameters[model_name]['search'],
                    train_data, train_labels, validate_data, validate_labels,
                    is_incremental=self.__parameters['setting']['is_incremental'], 
                    methods=self.__parameters['setting']['hyperparameter_tuning_methods']
                )
            elif model_name == 'prophet':
                _, tuned_parameters[model_name], best_score = self.__models[model_name].tune_hyperparameters(
                    self.__parameters[model_name]['search'],
                    train_data, train_labels, validate_data, validate_labels,
                    is_incremental=self.__parameters['setting']['is_incremental'], 
                    methods=self.__parameters['setting']['hyperparameter_tuning_methods']
                )
            print('Model:', model_name)
            print('Best parameters:', tuned_parameters[model_name])
            print('Best score:', best_score)
        return tuned_parameters

    def cross_validate(self, data, labels, categorical_features, num_splits=2, shuffle=True):
        """ Perform cross validation """
        for model_name in self.__models.keys():
            print('Perform cross validation on %s model' % model_name)
            if model_name == 'catboost':
                score = self.__models[model_name].cross_validate(data, labels, categorical_features)
            elif model_name == 'lightgbm' or model_name == 'xgboost':
                score = self.__models[model_name].cross_validate(data, labels)
            elif model_name == 'prophet':
                score = self.__models[model_name].cross_validate(data, labels)
            print('Cross validation score of %s model: %s' % (model_name, score))

    def train(self, train_data, train_labels, categorical_features, validate_data=None, validate_labels=None, save_model=True, identifier=None):
        """ Train the model(s) and optionally test on validation data """
        print('Training the model(s)')
        for model_name in self.__models.keys():
            if model_name == 'catboost':
                self.__models[model_name].train(
                    train_data,
                    train_labels,
                    categorical_features,
                    validate_data,
                    validate_labels,
                    self.__parameters['setting']['is_incremental']
                )
            elif model_name == 'lightgbm' or model_name == 'xgboost':
                self.__models[model_name].train(
                    train_data,
                    train_labels,
                    validate_data,
                    validate_labels,
                    self.__parameters['setting']['is_incremental']
                )
            elif model_name == 'prophet':
                self.__models[model_name].train(train_data, train_labels, self.__parameters['setting']['is_incremental'])
            if save_model == True:
                self.__models[model_name].save_model(identifier=identifier)

    def train_quantile(self, train_data, train_labels, categorical_features, validate_data=None, validate_labels=None, save_model=True, identifier=None):
        print('Training the quantile regression model(s)')
        for model_name in self.__models.keys():
            if model_name == 'catboost':
                self.__models[model_name].train_quantile(
                    train_data,
                    train_labels,
                    categorical_features,
                    validate_data,
                    validate_labels,
                    self.__parameters['setting']['is_incremental'],
                    self.__parameters['setting']['confidence_interval']
                )
            elif model_name == 'lightgbm' or model_name == 'xgboost':
                self.__models[model_name].train_quantile(
                    train_data,
                    train_labels,
                    validate_data,
                    validate_labels,
                    self.__parameters['setting']['is_incremental'],
                    self.__parameters['setting']['confidence_interval']
                )
            elif model_name == 'prophet':
                self.__models[model_name].train_quantile(
                    train_data, 
                    train_labels, 
                    self.__parameters['setting']['is_incremental'], 
                    self.__parameters['setting']['confidence_interval']
                )
            if save_model == True:
                self.__models[model_name].save_quantile_model(identifier=identifier)

    def predict(self, test_data):
        """ Perform prediction(s) on the data """
        print('Performing prediction(s) on the data')
        output_data = {}
        for model_name in self.__models.keys():
            output_data[model_name] = np.asarray(self.__models[model_name].predict(test_data))
        if self.__parameters['setting']['is_ensemble'] == True:
            output_data[self.__parameters['setting']['ensemble_name']] = np.mean(list(output_data.values()), axis=0)
        return output_data

    def predict_quantile(self, test_data):
        """ Perform quantile prediction on the data """
        print('Perform quantile prediction(s) on the data')
        lower_quantile_outputs = {}
        upper_quantile_outputs = {}
        for model_name in self.__models.keys():
            lower_quantiles, upper_quantiles = self.__models[model_name].predict_quantile(test_data)
            lower_quantile_outputs[model_name] = np.asarray(lower_quantiles)
            upper_quantile_outputs[model_name] = np.asarray(upper_quantiles)
        if self.__parameters['setting']['is_ensemble'] == True:
            lower_quantile_outputs[self.__parameters['setting']['ensemble_name']] = np.mean(list(lower_quantile_outputs.values()), axis=0) 
            upper_quantile_outputs[self.__parameters['setting']['ensemble_name']] = np.mean(list(upper_quantile_outputs.values()), axis=0) 
        return lower_quantile_outputs, upper_quantile_outputs

    def get_performance_metrics(self, data, labels, identifier, is_incremental=False):
        """ Get the performance metrics of the trained model(s) """
        models = list(self.__models.keys())
        if is_incremental == False:
            values = [
                np.mean(list(self.__models[model_name].get_performance_metrics(data, labels).values())) for model_name in self.__models.keys()
            ]
        else:
            values = [np.mean([np.mean(
                list(self.__models[model_name].get_performance_metrics(data_partition, label_partition).values())
            ) for data_partition, label_partition in zip(data, labels)]) for model_name in self.__models.keys()]
        if self.__parameters['setting']['is_ensemble'] == True:
            models += [self.__parameters['setting']['ensemble_name']]
            if is_incremental == False:
                values += [np.sqrt(mean_squared_error(self.predict(data)[self.__parameters['setting']['ensemble_name']], labels))]
            else:
                values += [np.mean([np.sqrt(
                    mean_squared_error(self.predict(data_partition)[self.__parameters['setting']['ensemble_name']], label_partition)
                ) for data_partition, label_partition in zip(data, labels)])]
        return pd.DataFrame({
            'Model': models,
            (identifier + '_RMSE'): values
        })

    def get_feature_importance(self, output_variable, train_data, train_labels, categorical_features, validate_data=None, validate_labels=None, identifier=None, max_features=-1, aggregate=False, plot_figure=False, save_figure=True, show_plot=False):
        """ Get the importance of each feature """
        print('Analyzing the feature importance (%s)' % self.__parameters['setting']['feature_selection_method'])
        name = (' (' + self.__parameters['setting']['feature_selection_method'] + ')') + (' (' + identifier + ')') if identifier is not None else ""
        feature_importances = {}
        if aggregate == True:
            feature_importances['ensemble'] = pd.DataFrame()
        for model_name in self.__models.keys():
            if model_name == 'catboost':
                model_feature_importance = self.__models[model_name].get_feature_importance(
                    train_data,
                    train_labels,
                    categorical_features,
                    validate_data,
                    validate_labels,
                    self.__parameters['setting']['is_incremental'],
                    self.__parameters['setting']['feature_selection_method']
                )
            else:
                model_feature_importance = self.__models[model_name].get_feature_importance(
                    train_data,
                    train_labels,
                    validate_data,
                    validate_labels,
                    self.__parameters['setting']['is_incremental'],
                    self.__parameters['setting']['feature_selection_method']
                )
            feature_importances[model_name] = model_feature_importance
            if aggregate == True:
                feature_importances['ensemble'] = feature_importances['ensemble'].append(model_feature_importance)
            # Plot the model's feature importance
            if plot_figure == True:
                self.plot_feature_importance(
                    model_name,
                    (train_data[0] if self.__parameters['setting']['is_incremental'] == True else train_data),
                    model_feature_importance,
                    max_features,
                    ('[%s] Feature importance' % output_variable) + name, 
                    save_figure=save_figure,
                    show_plot=show_plot
                )
        if aggregate == True:
            feature_importances['ensemble'] = feature_importances['ensemble'].groupby(['Feature']).mean().sort_values(
                by='Score', ascending=False, inplace=False, na_position='last'
            ).reset_index()
            # Plot the model's feature importance
            if plot_figure == True:
                self.plot_feature_importance(
                    'ensemble',
                    (train_data[0] if self.__parameters['setting']['is_incremental'] == True else train_data),
                    feature_importances['ensemble'],
                    max_features,
                    ('[%s] Feature importance' % output_variable) + name, 
                    save_figure=save_figure,
                    show_plot=show_plot
                )
        return feature_importances

    def plot_train_validate_test(self, train_data, train_labels, validate_data, validate_labels, test_data, test_labels, full_data, full_outputs, title, output_variable, save_figure=True, show_plot=False):
        """ Visualize the training outcome """
        register_matplotlib_converters()
        # Plot all data in a single graph
        for model in full_outputs.keys():
            plt.figure(figsize=(16, 9), dpi=300)
            plot_title = title + ' (' + model + ')'
            plt.title(plot_title)    
            plt.plot(pd.to_datetime(train_data['Date']).values, train_labels[output_variable].values, 'k', label='Training')
            plt.plot(pd.to_datetime(validate_data['Date']).values, validate_labels[output_variable].values, 'b', label='Validation')
            plt.plot(pd.to_datetime(test_data['Date']).values, test_labels[output_variable].values, 'g', label='Testing (labels)')
            plt.plot(pd.to_datetime(full_data['Date']).values, full_outputs[model], 'r', alpha=0.7, label='Testing (outputs)')
            plt.legend()
            plt.xlabel('Date')
            plt.ylabel(output_variable)
            plt.tight_layout()
            if save_figure == True:
                plt.savefig('outputs/plots/' + plot_title + '.png', dpi=300)
            if show_plot == True:
                if platform == 'win32':
                    plt.get_current_fig_manager().window.state('zoomed')
                elif platform == 'linux' or platform == 'linux2':
                    mng = plt.get_current_fig_manager()
                    mng.resize(*mng.window.maxsize())
                plt.show()
            plt.close()

    def plot_train_validate_test_metrics(self, train_data, train_labels, validate_data, validate_labels, test_data, test_labels, title, save_figure=True, show_plot=False):
        """ Plot the performance metrics for training outcome """
        register_matplotlib_converters()
        train_metrics = self.get_performance_metrics(train_data, train_labels, 'train', self.__parameters['setting']['is_incremental'])
        validate_metrics = self.get_performance_metrics(validate_data, validate_labels, 'validate', self.__parameters['setting']['is_incremental'])
        test_metrics = self.get_performance_metrics(test_data, test_labels, 'test')
        indices = np.arange(len(train_metrics.index))
        bar_width = 0.2
        opacity = 0.5
        plt.figure(figsize=(16, 9), dpi=300)
        plt.title(title)
        plt.bar(
            indices, train_metrics['train_RMSE'].values, bar_width,
            alpha=opacity, color='r', label='Training RMSE'
        )
        plt.bar(
            indices + bar_width, validate_metrics['validate_RMSE'].values, bar_width, 
            alpha=opacity, color='g', label='Validation RMSE'
        )
        plt.bar(
            indices + 2 * bar_width, test_metrics['test_RMSE'].values, bar_width, 
            alpha=opacity, color='b', label='Testing RMSE'
        )
        plt.xticks(indices + bar_width, train_metrics['Model'].values)
        plt.xlabel('Model')
        plt.ylabel('Value')
        plt.legend()
        plt.tight_layout()
        if save_figure == True:
            plt.savefig('outputs/plots/' + title + '.png', dpi=300)
        if platform == 'win32':
            plt.get_current_fig_manager().window.state('zoomed')
        elif platform == 'linux' or platform == 'linux2':
            mng = plt.get_current_fig_manager()
            mng.resize(*mng.window.maxsize())
        if show_plot == True:
            plt.show()
        plt.close()

    def plot_data(self, data, labels, outputs, title, output_variable, lower_quantiles=None, upper_quantiles=None, save_figure=True, show_plot=False):
        """ Visualize the training outcome using all data samples """
        register_matplotlib_converters()
        for model in outputs.keys():
            plt.figure(figsize=(16, 9), dpi=300)
            plot_title = title + ' (' + model + ')'
            plt.title(plot_title)
            if labels is not None:
                plt.plot(pd.to_datetime(data['Date']).values, labels[output_variable].values, 'k', label='Raw data')
            plt.plot(pd.to_datetime(data['Date']).values, outputs[model], 'r', alpha=0.7, label='Predicted data')
            if lower_quantiles is not None:
                plt.plot(pd.to_datetime(data['Date']).values, lower_quantiles[model], 'b', alpha=0.3)
            if upper_quantiles is not None:
                plt.plot(pd.to_datetime(data['Date']).values, upper_quantiles[model], 'b', alpha=0.3)
            if lower_quantiles is not None and upper_quantiles is not None:
                plt.fill_between(data['Date'].values, lower_quantiles[model], upper_quantiles[model], color='b', alpha=0.1)
            plt.legend()
            plt.xlabel('Date')
            plt.ylabel(output_variable)
            plt.tight_layout()
            if save_figure == True:
                plt.savefig('outputs/plots/' + plot_title + '.png', dpi=300)
            if show_plot == True:
                if platform == 'win32':
                    plt.get_current_fig_manager().window.state('zoomed')
                elif platform == 'linux' or platform == 'linux2':
                    mng = plt.get_current_fig_manager()
                    mng.resize(*mng.window.maxsize())
                plt.show()
            plt.close()

    def plot_data_metrics(self, data, labels, title, save_figure=True, show_plot=False):
        """ Plot the performance metrics for full training outcome """
        register_matplotlib_converters()
        metrics = self.get_performance_metrics(data, labels, 'train', self.__parameters['setting']['is_incremental'])
        indices = np.arange(len(metrics.index))
        bar_width = 0.2
        opacity = 0.5
        plt.figure(figsize=(16, 9), dpi=300)
        plt.title(title)
        plt.bar(indices, metrics['train_RMSE'].values, bar_width, alpha=opacity)
        plt.xticks(indices, metrics['Model'].values)
        plt.xlabel('Model')
        plt.ylabel('Value')
        plt.tight_layout()
        if save_figure == True:
            plt.savefig('outputs/plots/' + title + '.png', dpi=300)
        if platform == 'win32':
            plt.get_current_fig_manager().window.state('zoomed')
        elif platform == 'linux' or platform == 'linux2':
            mng = plt.get_current_fig_manager()
            mng.resize(*mng.window.maxsize())
        if show_plot == True:
            plt.show()
        plt.close()

    def plot_feature_importance(self, model_name, data, feature_scores, max_features, title, save_figure=True, show_plot=False):
        """ Visualize the feature importance """
        register_matplotlib_converters()
        plot_title = title + ' (' + model_name + ')'
        if model_name == 'prophet':
            self.__models[model_name].plot_changepoints(data, 'outputs/plots/', plot_title, save_figure=save_figure, show_plot=show_plot)
            self.__models[model_name].plot_components(data, 'outputs/plots/', plot_title, save_figure=save_figure, show_plot=show_plot)
        num_features = max_features if feature_scores.shape[0] > max_features else feature_scores.shape[0]
        plt.figure(figsize=(16, 9), dpi=300)
        plt.title(plot_title)
        sns.barplot(
            x='Score',
            y='Feature',
            data=feature_scores.head(num_features),
        )
        plt.tight_layout()
        if save_figure == True:
            plt.savefig('outputs/plots/' + plot_title + '.png', dpi=300)
        if show_plot == True:
            if platform == 'win32':
                plt.get_current_fig_manager().window.state('zoomed')
            elif platform == 'linux' or platform == 'linux2':
                mng = plt.get_current_fig_manager()
                mng.resize(*mng.window.maxsize())
            plt.show()
        plt.close()
