import copy
import catboost
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from bayes_opt import BayesianOptimization
from catboost import Pool
from eli5.sklearn import PermutationImportance
from pandas.plotting import register_matplotlib_converters
from sklearn.externals import joblib
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import ParameterGrid, ParameterSampler, StratifiedKFold
from tqdm import tqdm
from kpi_forecast.utils.hyperparameter_tuner import HyperparameterTuner

class CatBoostRegressor(object):
    """ CatBoost regressor model """
    __model = None
    __models = []
    __lower_quantile_model = None
    __lower_quantile_models = []
    __upper_quantile_model = None
    __upper_quantile_models = []
    __best_score = None    
    __best_model = None
    __tuner = None
    __is_incremental = False
    __is_trained = False
    __is_quantile_trained = False
    __parameters = None
    __defaults = {
        'model': {
            'loss_function': 'RMSE',                # Evaluation matrics
            'custom_metric': 'RMSE',                # Metric values to output during training
            'eval_metric': 'RMSE',                  # Metric used for overfitting detection and best model selection
            'iterations': 1000,                     # Max iterations / max number of trees
            'learning_rate': 0.03,                  # Learning rate
            'random_seed': 0,                       # Random seed for training
            'l2_leaf_reg': 3.0,                     # L2 regularization coefficient
            'bootstrap_type': 'Bayesian',           # Weight sampling method (Bayesian, Bernoulli, Poisson, No)
            'bagging_temperature': 1.0,             # (Bayesian) Bootstrap setting (higher value denotes more aggresive bagging)
            'random_strength': 1.0,                 # Amount of randomness when splitting the tree
            'use_best_model': True,                 # Save the best model based on the validation set
            'best_model_min_trees': None,           # Min number of trees that the best model should have
            'depth': 6,                             # Tree depth
            'ignored_features': None,               # Indices of features to exclude from training (e.g. 1, 3, 4, 7 stated as 1:3-4:7)
            'one_hot_max_size': 2,                  # Max different values of a feature to apply one-hot encoding
            'has_time': False,                      # Use the order of input data samples when transforming categorical features and choosing the tree structure
            'colsample_bylevel': 1,                 # Percentage of features to use at each split selection
            'nan_mode': 'Min',                      # Method for processing missing values (Forbidden, Min, Max)
            'leaf_estimation_method': 'Gradient',   # Leaf estimation method (Gradient, Newton)
            'leaf_estimation_iterations': None,     # Number of gradient steps when calculating the leaves' values
            'fold_len_multiplier': 2,               # Coefficient for changing the length of folds
            'boosting_type': 'Plain',               # Boosting scheme
            'allow_const_label': False,             # Allow to train with data that have equal label values for all data objects
            'early_stopping_rounds': 100,           # Early stopping iterations
            'border_count': 128,                    # Total splits for numerical features
            'feature_border_type': 'GreedyLogSum',  # Binarization mode for numerical features
            'used_ram_limit': None,                 # CPU RAM usage limit (None: no limit)
            'gpu_ram_part': 0.95,                   # Proportion of GPU RAM used for training
            'pinned_memory_size': 1073741824,       # Pinned CPU RAM to use per GPU
            'gpu_cat_features_storage': 'GpuRam',   # Method for storing the categorical features' values (CpuPinnedMemory, GpuRam)
            'task_type': 'GPU',                     # Processor type for training
            'name': 'experiment',                   # Experiment name to display in visualization tools
            'logging_level': None,                  # Logging level to output (None, Silent, Verbose, Info, Debug)
            'metric_period': 1,                     # Frequency to calculate metric values
            'verbose': True,                        # Verbose mode
            'train_dir': 'models/catboost_info',    # Directory that stores the files generated during training
            'model_size_reg': 0.5,                  # Model size regularization coefficient (larger value denotes smaller model size)
            'allow_writing_files': False,           # Allow to write files during training
            'save_snapshot': None,                  # Enable snapshotting for restoring the training progress after an interruption
            'snapshot_file': 'models/snapshots',    # Filename to save the training process
            'snapshot_interval': 100,               # Interval between saving snapshots in seconds
            'counter_calc_method': 'Full',          # Method for calculating the Counter CTR type (SkipTest, Full)
            'max_ctr_complexity': 4,                # Maximum number of categorical features that can be combined
            'ctr_leaf_count_limit': None,           # Maximum number of leaves with categorical features (None: not limited)
            'final_ctr_computation_mode': 'Default' # Final CTR computation mode (Default, Skip)
        },
        'extras': {
            'alpha': 0.9                            # # Parameter for Quantile regression
        },
        'file': {
            'path': 'models',                       # Path to load and save the model
            'name': 'catboost',                     # Filename of the model to be saved
            'format': 'pkl'                         # File format of the model file
        }
    }

    def __init__(self, model_parameters=None, tuner_parameters=None):
        """ Constructor """
        self.__parameters = copy.deepcopy(self.__defaults)
        # Set defined parameter values if present
        if model_parameters is not None:
            self.__parameters['model'].update(model_parameters['model'])
        self.__model = catboost.CatBoostRegressor(**(self.__parameters['model']))
        # Instantiate hyperparameter tuner
        self.__tuner = HyperparameterTuner(tuner_parameters)

    def create_quantile_models(self, alpha=None):
        """ Create quantile regression models """
        if alpha is not None:
            self.__parameters['extras']['alpha'] = alpha
        alpha = self.__parameters['extras']['alpha'] if self.__parameters['extras']['alpha'] < 0.5 else (1 - self.__parameters['extras']['alpha'])
        parameters = self.__parameters['model']
        parameters['loss_function'] = 'Quantile:alpha=' + str(alpha)
        self.__lower_quantile_model = catboost.CatBoostRegressor(**(self.__parameters['model']))
        parameters['loss_function'] = 'Quantile:alpha=' + str(1 - alpha)
        self.__upper_quantile_model = catboost.CatBoostRegressor(**(self.__parameters['model']))

    def load_model(self, identifier=None, is_incremental=False):
        """ Load the model """
        print('Loading the model from a file')
        self.__is_incremental = is_incremental
        identifier = ('_' + identifier) if identifier is not None else ''
        name = self.__parameters['file']['path'] + '/' + \
               self.__parameters['file']['name'] + identifier + '.' + self.__parameters['file']['format']
        self.__model = joblib.load(name)
        if self.__is_incremental == True:
            name = self.__parameters['file']['path'] + '/' + \
                   self.__parameters['file']['name'] + '_incremental' + identifier + '.' + self.__parameters['file']['format']
            self.__models = joblib.load(name)

    def load_quantile_model(self, identifier=None, is_incremental=False):
        """ Load the quantile regression model """
        print('Loading the quantile regression model from a file')
        self.__is_incremental = is_incremental
        identifier = ('_' + identifier) if identifier is not None else ''
        name = self.__parameters['file']['path'] + '/' + \
               self.__parameters['file']['name'] + '_lower_quantile' + identifier + '.' + self.__parameters['file']['format']
        self.__lower_quantile_model = joblib.load(name)
        self.__upper_quantile_model = joblib.load(name.replace('lower_quantile', 'upper_quantile'))
        if self.__is_incremental == True:
            name = self.__parameters['file']['path'] + '/' + \
                   self.__parameters['file']['name'] + '_lower_quantile_incremental' + identifier + '.' + self.__parameters['file']['format']
            self.__lower_quantile_models = joblib.load(name)
            self.__upper_quantile_models = joblib.load(name.replace('lower_quantile', 'upper_quantile'))

    def save_model(self, identifier=None):
        """ Save the model """
        print('Saving the model to a file')
        identifier = ('_' + identifier) if identifier is not None else ''
        if self.__is_trained == True:
            name = self.__parameters['file']['path'] + '/' + \
                   self.__parameters['file']['name'] + identifier + '.' + self.__parameters['file']['format']
            joblib.dump(self.__model, name)
            if self.__is_incremental == True:
                name = self.__parameters['file']['path'] + '/' + \
                       self.__parameters['file']['name'] + '_incremental' + identifier + '.' + self.__parameters['file']['format']
                joblib.dump(self.__models, name)

    def save_quantile_model(self, identifier=None):
        """ Save the quantile regression model """
        print('Saving the quantile regression model to a file')
        identifier = ('_' + identifier) if identifier is not None else ''
        if self.__is_trained == True:
            name = self.__parameters['file']['path'] + '/' + \
                   self.__parameters['file']['name'] + '_lower_quantile' + identifier + '.' + self.__parameters['file']['format']
            joblib.dump(self.__lower_quantile_model, name)
            joblib.dump(self.__upper_quantile_model, name.replace('lower_quantile', 'upper_quantile'))
            if self.__is_incremental == True:
                name = self.__parameters['file']['path'] + '/' + \
                       self.__parameters['file']['name'] + '_lower_quantile_incremental' + identifier + '.' + self.__parameters['file']['format']
                joblib.dump(self.__lower_quantile_models, name)
                joblib.dump(self.__upper_quantile_models, name.replace('lower_quantile', 'upper_quantile'))

    def get_parameters(self):
        """ Get the model parameters """
        return self.__parameters

    def tune_hyperparameters(self, parameters_list, train_data, train_labels, categorical_features, validate_data=None, validate_labels=None, is_incremental=False, methods='random'):
        """ Perform hyperparameter tuning """
        print('Performing hyperparameter tuning on the CatBoost model')

        def optimize_bayesian(learning_rate, l2_leaf_reg, depth, one_hot_max_size, colsample_bylevel):
            """ Perform Bayesian optimization """
            parameters = self.__parameters['model']
            parameters.update({
                'learning_rate': learning_rate,
                'l2_leaf_reg': l2_leaf_reg,
                'depth': int(round(depth)),
                'one_hot_max_size': int(round(one_hot_max_size)),
                'colsample_bylevel': colsample_bylevel
            })
            self.__model = catboost.CatBoostRegressor(**parameters)
            self.train(train_data, train_labels, categorical_features, validate_data, validate_labels, is_incremental=is_incremental)
            if validate_data is not None and validate_labels is not None:
                if is_incremental == False:
                    score = np.mean(list(self.get_performance_metrics(validate_data, validate_labels).values()))
                else:
                    score = np.mean([np.mean(
                        list(self.get_performance_metrics(data_partition, label_partition).values())
                    ) for data_partition, label_partition in zip(validate_data, validate_labels)])
            else:
                if is_incremental == False:
                    score = np.mean(list(self.get_performance_metrics(train_data, train_labels).values()))
                else:
                    score = np.mean([np.mean(
                        list(self.get_performance_metrics(data_partition, label_partition).values())
                    ) for data_partition, label_partition in zip(train_data, train_labels)])
            if self.__best_score is None or self.__best_score > score:
                self.__best_model = copy.deepcopy(self.__model)
                self.__best_score = score
            return -score

        final_model = None
        final_parameters = None
        final_score = None
        train_parameters = {
            'cat_features': (categorical_features if len(categorical_features) > 0 else None),
            'eval_set': (validate_data, validate_labels) if validate_data is not None and validate_labels is not None else (None, None)
        }
        tuner_parameters = self.__tuner.get_parameters()
        for method in methods:
            best_model = None
            best_parameters = None
            best_score = None
            if 'bayesian' in method.lower():
                print('Performing the Bayesian search')
                parameters = self.__tuner.get_parameters()['bayesian_search']
                optimizer = BayesianOptimization(
                    f=optimize_bayesian,
                    pbounds=parameters_list['bayesian'],
                    random_state=parameters['random_state'],
                    verbose=parameters['verbose']
                )
                optimizer.maximize(init_points=parameters['init_points'], n_iter=parameters['n_iter'])
                best_model = self.__best_model
                best_parameters = optimizer.max['params']
                best_parameters['depth'] = int(round(best_parameters['depth']))
                best_parameters['one_hot_max_size'] = int(round(best_parameters['one_hot_max_size']))
                best_score = -optimizer.max['target']
            elif 'grid' in method.lower():
                print('Performing the grid search')
                if is_incremental == False:
                    best_model, best_parameters, best_score, _ = self.__tuner.perform_grid_search(
                        self.__model, parameters_list['grid'], train_data, train_labels, train_parameters
                    )
                else:
                    for parameters in tqdm(list(ParameterGrid(parameters_list['grid'])), ascii=True):
                        model_parameters = self.get_parameters().update(parameters)
                        self.__model = catboost.CatBoostRegressor(**model_parameters)
                        self.train_incremental(self.__model, model_parameters, train_data, train_labels, validate_data, validate_labels)
                        if validate_data is not None and validate_labels is not None:
                            score = np.mean(list(self.get_performance_metrics(validate_data, validate_labels).values()))
                        else:
                            score = np.mean(list(self.get_performance_metrics(train_data, train_labels).values()))
                        if best_score is None or best_score > score:
                            best_model = copy.deepcopy(self.__model)
                            best_parameters = parameters
                            best_score = score
            elif 'random' in method.lower():
                print('Performing the randomized search')
                if is_incremental == False:
                    best_model, best_parameters, best_score, _ = self.__tuner.perform_randomized_search(
                        self.__model, parameters_list['random'], train_data, train_labels, train_parameters
                    )
                else:
                    random_parameters = list(ParameterSampler(
                        parameters_list['random'],
                        n_iter=tuner_parameters['randomized_search']['iterations'],
                        random_state=tuner_parameters['randomized_search']['random_seed']
                    ))
                    for parameters in tqdm(random_parameters, ascii=True):
                        model_parameters = self.get_parameters().update(parameters)
                        self.__model = catboost.CatBoostRegressor(**model_parameters)
                        self.train_incremental(self.__model, model_parameters, train_data, train_labels, validate_data, validate_labels)
                        if validate_data is not None and validate_labels is not None:
                            score = np.mean(list(self.get_performance_metrics(validate_data, validate_labels).values()))
                        else:
                            score = np.mean(list(self.get_performance_metrics(train_data, train_labels).values()))
                        if best_score is None or best_score > score:
                            best_model = copy.deepcopy(self.__model)
                            best_parameters = parameters
                            best_score = score
            if best_score is not None and (final_model is None or final_score < best_score):
                parameters = self.__parameters
                parameters['model'].update(best_parameters)
                final_model = best_model
                final_parameters = parameters
                final_score = best_score
        return final_model, final_parameters, final_score

    def cross_validate(self, data, labels, categorical_features, num_splits=2, shuffle=True):
        """ Perform cross validation (incremental learning is not supported) """
        scores = 0
        folds = StratifiedKFold(n_splits=num_splits, random_state=self.__parameters['model']['random_seed'], shuffle=shuffle)
        sorted_data = data.reset_index(drop=True)
        sorted_labels = labels.reset_index(drop=True)
        for train_indices, validate_indices in folds.split(data, labels):
            model = copy.deepcopy(self.__model)
            model.fit(
                X=sorted_data.ix[train_indices],
                y=sorted_labels.ix[train_indices],
                cat_features=(categorical_features if len(categorical_features) > 0 else None),
                eval_set=(sorted_data.ix[validate_indices], sorted_labels.ix[validate_indices]),
            )
            scores += model.get_best_score()['validation_0'][self.__parameters['model']['loss_function']]
        return scores / num_splits

    def train_single(self, model, train_data, train_labels, categorical_features, validate_data=None, validate_labels=None):
        """ Train the single model """
        model.fit(
            X=train_data,
            y=train_labels,
            cat_features=(categorical_features if len(categorical_features) > 0 else None),
            eval_set=(validate_data, validate_labels) if validate_data is not None and validate_labels is not None else (None, None)
        )
        return model

    def train_incremental(self, model, train_data, train_labels, categorical_features, validate_data=None, validate_labels=None):
        """ Train the model incrementally """
        models = []
        for i in range(len(train_data)):
            print('Fold:', i)
            models.append(copy.deepcopy(model))
            models[i].fit(
                X=train_data[i],
                y=train_labels[i],
                cat_features=(categorical_features if len(categorical_features) > 0 else None),
                eval_set=(validate_data[i], validate_labels[i]) if validate_data is not None and validate_labels is not None else (None, None)
            )
        return models

    def train(self, train_data, train_labels, categorical_features, validate_data=None, validate_labels=None, is_incremental=False):
        """ Train the model and optionally test it on validation data """
        self.__is_incremental = is_incremental
        if self.__is_incremental == False:
            self.__model = self.train_single(self.__model, train_data, train_labels, categorical_features, validate_data, validate_labels)
        else:
            self.__models = self.train_incremental(self.__model, train_data, train_labels, categorical_features, validate_data, validate_labels)
        self.__is_trained = True

    def train_quantile(self, train_data, train_labels, categorical_features, validate_data=None, validate_labels=None, is_incremental=False, alpha=None):
        """ Train the quantile regression models and optionallt test them on validation data """
        # Create quantile regression models
        self.create_quantile_models(alpha)
        # Train the models
        self.__is_incremental = is_incremental
        if self.__is_incremental == False:
            self.__lower_quantile_model = self.train_single(self.__lower_quantile_model, self.__parameters, train_data, train_labels, validate_data, validate_labels)
            self.__upper_quantile_model = self.train_single(self.__upper_quantile_model, self.__parameters, train_data, train_labels, validate_data, validate_labels)
        else:
            self.__lower_quantile_models = self.train_incremental(self.__lower_quantile_model, self.__parameters, train_data, train_labels, validate_data, validate_labels)
            self.__upper_quantile_models = self.train_incremental(self.__upper_quantile_model, self.__parameters, train_data, train_labels, validate_data, validate_labels)
        self.__is_quantile_trained = True

    def predict(self, test_data):
        """ Perform prediction on the data """
        if self.__is_incremental == False:
            return self.__model.predict(test_data)
        else:
            return np.mean([model.predict(test_data) for model in self.__models], axis=0)

    def predict_quantile(self, test_data):
        """ Perform quantile prediction on the data """
        if self.__is_incremental == False:
            lower_quantiles = self.__lower_quantile_model.predict(test_data)
            upper_quantiles = self.__upper_quantile_model.predict(test_data)
        else:
            lower_quantiles = np.mean([model.predict(test_data) for model in self.__lower_quantile_models], axis=0)
            upper_quantiles = np.mean([model.predict(test_data) for model in self.__upper_quantile_models], axis=0)
        return lower_quantiles, upper_quantiles

    def get_performance_metrics(self, data, labels):
        """ Get the performance metrics of the trained model """
        metrics = {}
        if self.__is_incremental == False:
            metrics[self.__parameters['model']['loss_function']] = np.sqrt(mean_squared_error(self.predict(data), labels))
        else:
            for index, model in enumerate(self.__models):
                metrics[(self.__parameters['model']['loss_function'] + '_' + str(index))] = np.sqrt(mean_squared_error(model.predict(data), labels))
        return metrics

    def get_feature_importance(self, train_data, train_labels, categorical_features, validate_data=None, validate_labels=None, is_incremental=False, method='permutation'):
        """ Get the importance of each feature """
        self.__is_incremental = is_incremental
        feature_names = (train_data[0] if self.__is_incremental == True else train_data).columns
        feature_importances = np.zeros(len(feature_names))
        feature_importances_std = np.zeros(len(feature_names))
        # Train the model(s)
        self.train(train_data, train_labels, categorical_features, validate_data, validate_labels, is_incremental)
        # Get the permutation importance
        if method.lower() == 'permutation':
            if self.__is_incremental == False:
                permutation = PermutationImportance(
                    self.__model, 
                    random_state=self.__parameters['model']['random_seed']
                ).fit(
                    train_data.replace([np.inf, -np.inf], np.nan).fillna(0),
                    train_labels.replace([np.inf, -np.inf], np.nan).fillna(0),
                    cat_features=(categorical_features if len(categorical_features) > 0 else None),
                    eval_set=(validate_data, validate_labels) if validate_data is not None and validate_labels is not None else (None, None)
                )
                feature_importances = permutation.feature_importances_
                feature_importances_std = permutation.feature_importances_std_
            else:
                for model in self.__models:
                    for i in range(len(train_data)):
                        permutation = PermutationImportance(
                            model, 
                            random_state=self.__parameters['model']['random_seed']
                        ).fit(
                            train_data[i].replace([np.inf, -np.inf], np.nan).fillna(0),
                            train_labels[i].replace([np.inf, -np.inf], np.nan).fillna(0),
                            cat_features=(categorical_features if len(categorical_features) > 0 else None),
                            eval_set=(validate_data[i], validate_labels[i]) if validate_data is not None and validate_labels is not None else (None, None)
                        )
                        feature_importances += permutation.feature_importances_
                        feature_importances_std += permutation.feature_importances_std_
                feature_importances /= (len(self.__models) * len(train_data))
                feature_importances_std /= (len(self.__models) * len(train_data))
            features = pd.DataFrame({
                'Feature': feature_names,
                'Score': feature_importances,
                'Score Std': feature_importances_std
            }).sort_values(by='Score', ascending=False, inplace=False, na_position='last')
        # Get the simple permutation importance
        else:
            if self.__is_incremental == False:
                feature_importances = self.__model.feature_importances_
            else:
                for model in self.__models:
                    feature_importances += model.feature_importances_
                feature_importances /= len(self.__models)
            features = pd.DataFrame({
                'Feature': feature_names,
                'Score': feature_importances
            }).sort_values(by='Score', ascending=False, inplace=False, na_position='last')
        return features
