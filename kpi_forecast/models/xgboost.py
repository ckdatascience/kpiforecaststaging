import copy
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import xgboost as xgb
from bayes_opt import BayesianOptimization
from eli5.sklearn import PermutationImportance
from sklearn.ensemble import GradientBoostingRegressor
from sklearn.externals import joblib
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import ParameterGrid, ParameterSampler, StratifiedKFold
from tqdm import tqdm
from kpi_forecast.utils.hyperparameter_tuner import HyperparameterTuner

class XgBoostRegressor(object):
    """ XGBoost regressor model """
    __model = None
    __models = []
    __lower_quantile_model = None
    __lower_quantile_models = []
    __upper_quantile_model = None
    __upper_quantile_models = []
    __best_score = None    
    __best_model = None
    __tuner = None
    __is_incremental = False
    __is_trained = False
    __is_quantile_trained = False
    __parameters = None
    __defaults = {
        'model': {
            'booster': 'gbtree',                # Booster type
            'verbosity': 2,                     # Verbosity (0: silent, 1: warning, 2: info, 3: debug)
            'disable_default_eval_metric': 0,   # Flag to disable default metric (>0: disabled)
            'learning_rate': 0.3,               # Learning rate
            'min_split_loss': 0.0,              # Min loss reduction before making further partition on a leaf node
            'max_depth': 6,                     # Tree depth
            'min_child_weight': 1.0,            # Min sum of instance weight needed in a child (leaf)
            'max_delta_step': 0,                # Max delta step allowable for each leaf output (0: no constraint)
            'subsample': 1.0,                   # Subsample ratio of the training instances (to reduce overfitting)
            'colsample_bytree': 1.0,            # Subsample ratio of columns when constructing each tree
            'colsample_bylevel': 1.0,           # Subsample ratio of columns for each level
            'colsample_bynode': 1.0,            # Subsample ratio of columns for each node
            'reg_lambda': 1,                    # L2 regularization coefficient (higher value denotes more conservative model)
            'reg_alpha': 0,                     # L1 regularization coefficient (higher value denotes more conservative model)
            'tree_method': 'gpu_exact',         # Tree construction algorithm (auto, exact, approx, hist, gpu_exact, gpu_hist)
            'scale_pos_weight': 1,              # Control the balance of positive and negative weights
            'updater': 'grow_colmaker,prune',   # Sequence of tree updaters to run
            'refresh_leaf': 1,                  # Parameter of the refresh updater plugin
            'process_type': 'default',          # Type of boosting process to run (default, update)
            'grow_policy': 'depthwise',         # Controls a way new nodes are added to the tree (depthwise, lossguide)
            'max_leaves': 0,                    # Maximum number of nodes to be added
            'max_bin': 256,                     # Maximum number of discrete bins to bucket continuous features
            'predictor': 'gpu_predictor',       # Type of predictor algorithm to use
            'num_parallel_tree': 1,             # Total parallel trees constructed during each iteration
            'objective': 'reg:linear',          # Objective option
            'base_score': 0.5,                  # Initial prediction score of all instances (global bias)
            'eval_metric': 'rmse',              # Evaluation metrics (rmse, mae, logloss, auc, map, etc.)
            'random_state': 0,                  # Random seed for training
            'n_estimators': 100,                # Max iterations
            'missing': None,                    # Value in the data which needs to be present as a missing value
            'early_stopping_rounds': None       # Early stopping iterations
        },
        'extras': {
            'alpha': 0.9                        # Parameter for Quantile regression
        },
        'file': {
            'path': 'models',                   # Path to load and save the model
            'name': 'xgboost',                  # Filename of the model to be saved
            'format': 'pkl'                     # File extension of the model file
        }
    }

    def __init__(self, model_parameters=None, tuner_parameters=None):
        """ Constructor """
        self.__parameters = copy.deepcopy(self.__defaults)
        # Set defined parameter values if present
        if model_parameters is not None:
            self.__parameters['model'].update(model_parameters['model'])
        self.__model = xgb.XGBRegressor(**(self.__parameters['model']))
        # Instantiate hyperparameter tuner
        self.__tuner = HyperparameterTuner(tuner_parameters)

    def create_quantile_models(self, alpha=None):
        """ Create quantile regression models """
        if alpha is not None:
            self.__parameters['extras']['alpha'] = alpha
        alpha = self.__parameters['extras']['alpha'] if self.__parameters['extras']['alpha'] < 0.5 else (1 - self.__parameters['extras']['alpha'])
        parameters = {
            'loss': 'quantile',
            'learning_rate': self.__parameters['model']['learning_rate'],
            'n_estimators': self.__parameters['model']['n_estimators'],
            'subsample': self.__parameters['model']['subsample'],
            'max_depth': self.__parameters['model']['max_depth'],
            'random_state': self.__parameters['model']['random_state'],
            'alpha': alpha,
            'verbose': self.__parameters['model']['verbosity']
        }
        self.__lower_quantile_model = GradientBoostingRegressor(**parameters)
        parameters['alpha'] = 1 - parameters['alpha']
        self.__upper_quantile_model = GradientBoostingRegressor(**parameters)

    def load_model(self, identifier=None, is_incremental=False):
        """ Load the model """
        print('Loading the model from a file')
        self.__is_incremental = is_incremental
        identifier = ('_' + identifier) if identifier is not None else ''
        name = self.__parameters['file']['path'] + '/' + \
               self.__parameters['file']['name'] + identifier + '.' + self.__parameters['file']['format']
        self.__model = joblib.load(name)
        if self.__is_incremental == True:
            name = self.__parameters['file']['path'] + '/' + \
                   self.__parameters['file']['name'] + '_incremental' + identifier + '.' + self.__parameters['file']['format']
            self.__models = joblib.load(name)

    def load_quantile_model(self, identifier=None, is_incremental=False):
        """ Load the quantile regression model """
        print('Loading the quantile regression model from a file')
        self.__is_incremental = is_incremental
        identifier = ('_' + identifier) if identifier is not None else ''
        name = self.__parameters['file']['path'] + '/' + \
               self.__parameters['file']['name'] + '_lower_quantile' + identifier + '.' + self.__parameters['file']['format']
        self.__lower_quantile_model = joblib.load(name)
        self.__upper_quantile_model = joblib.load(name.replace('lower_quantile', 'upper_quantile'))
        if self.__is_incremental == True:
            name = self.__parameters['file']['path'] + '/' + \
                   self.__parameters['file']['name'] + '_lower_quantile_incremental' + identifier + '.' + self.__parameters['file']['format']
            self.__lower_quantile_models = joblib.load(name)
            self.__upper_quantile_models = joblib.load(name.replace('lower_quantile', 'upper_quantile'))

    def save_model(self, identifier=None):
        """ Save the model """
        print('Saving the model to a file')
        identifier = ('_' + identifier) if identifier is not None else ''
        if self.__is_trained == True:
            name = self.__parameters['file']['path'] + '/' + \
                   self.__parameters['file']['name'] + identifier + '.' + self.__parameters['file']['format']
            joblib.dump(self.__model, name)
            if self.__is_incremental == True:
                name = self.__parameters['file']['path'] + '/' + \
                       self.__parameters['file']['name'] + '_incremental' + identifier + '.' + self.__parameters['file']['format']
                joblib.dump(self.__models, name)

    def save_quantile_model(self, identifier=None):
        """ Save the quantile regression model """
        print('Saving the quantile regression model to a file')
        identifier = ('_' + identifier) if identifier is not None else ''
        if self.__is_trained == True:
            name = self.__parameters['file']['path'] + '/' + \
                   self.__parameters['file']['name'] + '_lower_quantile' + identifier + '.' + self.__parameters['file']['format']
            joblib.dump(self.__lower_quantile_model, name)
            joblib.dump(self.__upper_quantile_model, name.replace('lower_quantile', 'upper_quantile'))
            if self.__is_incremental == True:
                name = self.__parameters['file']['path'] + '/' + \
                       self.__parameters['file']['name'] + '_lower_quantile_incremental' + identifier + '.' + self.__parameters['file']['format']
                joblib.dump(self.__lower_quantile_models, name)
                joblib.dump(self.__upper_quantile_models, name.replace('lower_quantile', 'upper_quantile'))

    def get_parameters(self):
        """ Get the model parameters """
        return self.__parameters

    def tune_hyperparameters(self, parameters_list, train_data, train_labels, validate_data=None, validate_labels=None, is_incremental=False, methods='grid'):
        """ Perform hyperparameter tuning """
        print('Performing hyperparameter tuning on the XGBoost model')

        def optimize_bayesian(learning_rate, max_depth, min_child_weight, subsample, colsample_bytree):
            """ Perform Bayesian optimization """
            parameters = self.__parameters['model']
            parameters.update({
                'learning_rate': learning_rate,
                'max_depth': int(round(max_depth)),
                'min_child_weight': int(round(min_child_weight)),
                'subsample': subsample,
                'colsample_bytree': colsample_bytree
            })
            self.__model = xgb.XGBRegressor(**parameters)
            self.train(train_data, train_labels, validate_data, validate_labels, is_incremental=is_incremental)
            if validate_data is not None and validate_labels is not None:
                if is_incremental == False:
                    score = np.mean(list(self.get_performance_metrics(validate_data, validate_labels).values()))
                else:
                    score = np.mean([np.mean(
                        list(self.get_performance_metrics(data_partition, label_partition).values())
                    ) for data_partition, label_partition in zip(validate_data, validate_labels)])
            else:
                if is_incremental == False:
                    score = np.mean(list(self.get_performance_metrics(train_data, train_labels).values()))
                else:
                    score = np.mean([np.mean(
                        list(self.get_performance_metrics(data_partition, label_partition).values())
                    ) for data_partition, label_partition in zip(train_data, train_labels)])
            if self.__best_score is None or self.__best_score > score:
                self.__best_model = copy.deepcopy(self.__model)
                self.__best_score = score
            return -score

        final_model = None
        final_parameters = None
        final_score = None
        train_parameters = {
            'eval_set': [(validate_data.values, validate_labels.values)],
            'eval_metric': self.__parameters['model']['eval_metric'],
            'early_stopping_rounds': self.__parameters['model']['early_stopping_rounds'],
            'verbose': (True if self.__parameters['model']['verbosity'] > 0 else False)
        }
        tuner_parameters = self.__tuner.get_parameters()
        for method in methods:
            best_model = None
            best_parameters = None
            best_score = None
            if 'bayesian' in method.lower():
                print('Performing the Bayesian search')
                parameters = self.__tuner.get_parameters()['bayesian_search']
                optimizer = BayesianOptimization(
                    f=optimize_bayesian,
                    pbounds=parameters_list['bayesian'],
                    random_state=parameters['random_state'],
                    verbose=parameters['verbose']
                )
                optimizer.maximize(init_points=parameters['init_points'], n_iter=parameters['n_iter'])
                best_model = self.__best_model
                best_parameters = optimizer.max['params']
                best_parameters['max_depth'] = int(round(best_parameters['max_depth']))
                best_parameters['min_child_weight'] = int(round(best_parameters['min_child_weight']))
                best_score = -optimizer.max['target']
            elif 'grid' in method.lower():
                print('Performing the grid search')
                if is_incremental == False:
                    best_model, best_parameters, best_score, _ = self.__tuner.perform_grid_search(
                        self.__model, parameters_list['grid'], train_data, train_labels, train_parameters
                    )
                else:
                    for parameters in tqdm(list(ParameterGrid(parameters_list['grid'])), ascii=True):
                        model_parameters = self.get_parameters().update(parameters)
                        self.__model = xgb.XGBRegressor(**parameters)
                        self.train_incremental(self.__model, model_parameters, train_data, train_labels, validate_data, validate_labels)
                        if validate_data is not None and validate_labels is not None:
                            score = np.mean(list(self.get_performance_metrics(validate_data, validate_labels).values()))
                        else:
                            score = np.mean(list(self.get_performance_metrics(train_data, train_labels).values()))
                        if best_score is None or best_score > score:
                            best_model = copy.deepcopy(self.__model)
                            best_parameters = parameters
                            best_score = score
            elif 'random' in method.lower():
                print('Performing the randomized search')
                if is_incremental == False:
                    best_model, best_parameters, best_score, _ = self.__tuner.perform_randomized_search(
                        self.__model, parameters_list['random'], train_data, train_labels, train_parameters
                    )
                else:
                    random_parameters = list(ParameterSampler(
                        parameters_list['random'],
                        n_iter=tuner_parameters['randomized_search']['iterations'],
                        random_state=tuner_parameters['randomized_search']['random_seed']
                    ))
                    for parameters in tqdm(random_parameters, ascii=True):
                        model_parameters = self.get_parameters().update(parameters)
                        self.__model = xgb.XGBRegressor(**parameters)
                        self.train_incremental(self.__model, model_parameters, train_data, train_labels, validate_data, validate_labels)
                        if validate_data is not None and validate_labels is not None:
                            score = np.mean(list(self.get_performance_metrics(validate_data, validate_labels).values()))
                        else:
                            score = np.mean(list(self.get_performance_metrics(train_data, train_labels).values()))
                        if best_score is None or best_score > score:
                            best_model = copy.deepcopy(self.__model)
                            best_parameters = parameters
                            best_score = score
            if best_score is not None and (final_model is None or final_score < best_score):
                parameters = self.__parameters
                parameters['model'].update(best_parameters)
                final_model = best_model
                final_parameters = parameters
                final_score = best_score
        return final_model, final_parameters, final_score

    def cross_validate(self, data, labels, num_splits=2, shuffle=True):
        """ Perform cross validation (incremental learning is not supported) """
        scores = 0
        folds = StratifiedKFold(n_splits=num_splits, random_state=self.__parameters['model']['random_state'], shuffle=shuffle)
        sorted_data = data.reset_index(drop=True)
        sorted_labels = labels.reset_index(drop=True)
        for train_indices, validate_indices in folds.split(data, labels):
            model = copy.deepcopy(self.__model)
            model.fit(
                X=sorted_data.ix[train_indices],
                y=sorted_labels.ix[train_indices],
                eval_set=[(sorted_data.ix[validate_indices], sorted_labels.ix[validate_indices])],
                eval_metric=self.__parameters['model']['eval_metric'],
                early_stopping_rounds=self.__parameters['model']['early_stopping_rounds'],
                verbose=(True if self.__parameters['model']['verbosity'] > 0 else False)
            )
            scores += model.best_score
        return scores / num_splits

    def train_single(self, model, parameters, train_data, train_labels, validate_data=None, validate_labels=None):
        """ Train the single model """
        model.fit(
            X=train_data.values,
            y=train_labels.values,
            eval_set=[(validate_data.values, validate_labels.values)],
            eval_metric=self.__parameters['model']['eval_metric'],
            early_stopping_rounds=self.__parameters['model']['early_stopping_rounds'],
            verbose=(True if self.__parameters['model']['verbosity'] > 0 else False)
        )
        return model

    def train_incremental(self, model, parameters, train_data, train_labels, validate_data=None, validate_labels=None):
        """ Train the model incrementally """
        models = []
        for i in range(len(train_data)):
            print('Fold:', i)
            models.append(copy.deepcopy(model))
            if validate_data is not None and validate_labels is not None:
                validate_set = [(validate_data[i].values, validate_labels[i].values)] 
            else:
                validate_set = [(None, None)]
            models[i].fit(
                X=train_data[i].values,
                y=train_labels[i].values,
                eval_set=validate_set,
                eval_metric=parameters['model']['eval_metric'],
                early_stopping_rounds=parameters['model']['early_stopping_rounds'],
                verbose=(True if parameters['model']['verbosity'] > 0 else False)
            )
        return models

    def train(self, train_data, train_labels, validate_data=None, validate_labels=None, is_incremental=False):
        """ Train the model and optionally test it on validation data """
        self.__is_incremental = is_incremental
        if self.__is_incremental == False:
            self.__model = self.train_single(self.__model, self.__parameters, train_data, train_labels, validate_data, validate_labels)
        else:
            self.__models = self.train_incremental(self.__model, self.__parameters, train_data, train_labels, validate_data, validate_labels)
        self.__is_trained = True

    def train_quantile(self, train_data, train_labels, is_incremental=False, alpha=None):
        """ Train the quantile regression models and optionally test them on validation data """
        # Create quantile regression models
        self.create_quantile_models(alpha)
        # Train the models
        self.__is_incremental = is_incremental
        if self.__is_incremental == False:
            self.__lower_quantile_model.fit(train_data, train_labels)
            self.__upper_quantile_model.fit(train_data, train_labels)
        else:
            self.__lower_quantile_models = []
            self.__upper_quantile_models = []
            for i in range(len(train_data)):
                print('Fold:', i)
                self.__lower_quantile_models.append(copy.deepcopy(self.__lower_quantile_model))
                self.__lower_quantile_models[i].fit(train_data[i], train_labels[i])
                self.__upper_quantile_models.append(copy.deepcopy(self.__upper_quantile_model))
                self.__upper_quantile_models[i].fit(train_data[i], train_labels[i])
        self.__is_quantile_trained = True

    def predict(self, test_data):
        """ Perform prediction on the data """
        if self.__is_incremental == False:
            return self.__model.predict(test_data.values)
        else:
            return np.mean([model.predict(test_data.values) for model in self.__models], axis=0)

    def predict_quantile(self, test_data):
        """ Perform quantile prediction on the data """
        if self.__is_incremental == False:
            lower_quantiles = self.__lower_quantile_model.predict(test_data)
            upper_quantiles = self.__upper_quantile_model.predict(test_data)
        else:
            lower_quantiles = np.mean([model.predict(test_data) for model in self.__lower_quantile_models], axis=0)
            upper_quantiles = np.mean([model.predict(test_data) for model in self.__upper_quantile_models], axis=0)
        return lower_quantiles, upper_quantiles

    def get_performance_metrics(self, data, labels):
        """ Get the performance metrics of the trained model """
        metrics = {}
        if self.__is_incremental == False:
            metrics[self.__parameters['model']['eval_metric']] = np.sqrt(mean_squared_error(self.predict(data), labels))
        else:
            for index, model in enumerate(self.__models):
                metrics[(self.__parameters['model']['eval_metric'] + '_' + str(index))] = np.sqrt(mean_squared_error(model.predict(data), labels))
        return metrics

    def get_feature_importance(self, train_data, train_labels, validate_data=None, validate_labels=None, is_incremental=False, method='permutation'):
        """ Get the importance of each feature """
        self.__is_incremental = is_incremental
        feature_names = (train_data[0] if self.__is_incremental == True else train_data).columns
        feature_importances = np.zeros(len(feature_names))
        feature_importances_std = np.zeros(len(feature_names))
        # Train the model(s)
        self.train(train_data, train_labels, validate_data, validate_labels, is_incremental)
        # Get the permutation importance
        if method.lower() == 'permutation':
            if self.__is_incremental == False:
                permutation = PermutationImportance(
                    self.__model, 
                    random_state=self.__parameters['model']['random_state']
                ).fit(
                    train_data.replace([np.inf, -np.inf], np.nan).fillna(0),
                    train_labels.replace([np.inf, -np.inf], np.nan).fillna(0),
                    eval_set=[(validate_data, validate_labels)] if validate_data is not None and validate_labels is not None else [(None, None)],
                    eval_metric=self.__parameters['model']['eval_metric'],
                    early_stopping_rounds=self.__parameters['model']['early_stopping_rounds'],
                    verbose=(True if self.__parameters['model']['verbosity'] > 0 else False)
                )
                feature_importances = permutation.feature_importances_
                feature_importances_std = permutation.feature_importances_std_
            else:
                for model in self.__models:
                    for i in range(len(train_data)):
                        permutation = PermutationImportance(
                            model, 
                            random_state=self.__parameters['model']['random_state']
                        ).fit(
                            train_data[i].replace([np.inf, -np.inf], np.nan).fillna(0),
                            train_labels[i].replace([np.inf, -np.inf], np.nan).fillna(0),
                            eval_set=[(validate_data[i], validate_labels[i])] if validate_data is not None and validate_labels is not None else [(None, None)],
                            eval_metric=self.__parameters['model']['eval_metric'],
                            early_stopping_rounds=self.__parameters['model']['early_stopping_rounds'],
                            verbose=(True if self.__parameters['model']['verbosity'] > 0 else False)
                        )
                        feature_importances += permutation.feature_importances_
                        feature_importances_std += permutation.feature_importances_std_
                feature_importances /= (len(self.__models) * len(train_data))
                feature_importances_std /= (len(self.__models) * len(train_data))
            features = pd.DataFrame({
                'Feature': feature_names,
                'Score': feature_importances,
                'Score Std': feature_importances_std
            }).sort_values(by='Score', ascending=False, inplace=False, na_position='last')
        # Get the simple permutation importance
        else:
            if self.__is_incremental == False:
                feature_importances = self.__model.feature_importances_
            else:
                for model in self.__models:
                    feature_importances += model.feature_importances_
                feature_importances /= len(self.__models)
            features = pd.DataFrame({
                'Feature': feature_names,
                'Score': feature_importances
            }).sort_values(by='Score', ascending=False, inplace=False, na_position='last')
        return features

    def plot_tree(self, num_trees=0):
        """ Visualize the boosting tree """
        print('Visualizing the boosting tree')
        models = []
        if self.__is_incremental == False:
            models.append(self.__model)
        else:
            models = self.__models
        for model in models:
            xgb.plot_tree(model, num_trees=num_trees)
            plt.show()
