import copy
import lightgbm as lgb
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from bayes_opt import BayesianOptimization
from eli5.sklearn import PermutationImportance
from pandas.plotting import register_matplotlib_converters
from sklearn.externals import joblib
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import ParameterGrid, ParameterSampler, StratifiedKFold
from tqdm import tqdm
from kpi_forecast.utils.hyperparameter_tuner import HyperparameterTuner

class LightGbmRegressor(object):
    """ Light GBM regressor model """
    __model = None
    __models = []
    __lower_quantile_model = None
    __lower_quantile_models = []
    __upper_quantile_model = None
    __upper_quantile_models = []
    __best_score = None    
    __best_model = None
    __tuner = None
    __is_incremental = False
    __is_trained = False
    __is_quantile_trained = False
    __parameters = None
    __defaults = {
        'model': {
            'objective': 'regression',                          # Objective option
            'boosting_type': 'gbdt',                            # Booster type (gbdt, rf, dart, goss)
            'n_estimators': 1000,                               # Max iterations / max number of trees
            'learning_rate': 0.1,                               # Learning rate
            'num_leaves': 31,                                   # Max tree leaves for base learners
            'tree_learner': 'serial',                           # Tree learner (serial, feature, data, voting)
            'num_threads': 0,                                   # Total threads (0: default number of threads in OpenMP)
            'device_type': 'cpu',                               # Device of the tree learning (cpu, gpu)
            'random_state': 0,                                  # Random seed for training
            'max_depth': -1,                                    # Tree depth (<0: no limit)
            'min_child_samples': 20,                            # Min data samples needed in a child (leaf)
            'min_child_weight': 0.001,                          # Min sum of instance weight needed in a child (leaf)
            'bagging_fraction': 1.0,                            # Bagging fraction
            'bagging_freq': 1,                                  # Bagging frequency
            'bagging_seed': 0,                                  # Random seed for bagging
            'feature_fraction': 1.0,                            # Feature fraction
            'feature_fraction_seed': 0,                         # Random seed for feature fraction
            'early_stopping_rounds': 0,                         # Early stopping iterations (<=0: disabled)
            'max_delta_step': 0.0,                              # Limit the max output of tree leaves (<=0: no constraint)
            'lambda_l1': 0.0,                                   # L1 regularization
            'lambda_l2': 0.0,                                   # L2 regularization
            'min_split_gain': 0.0,                              # Min loss reduction before making further partition on a leaf node
            'min_data_per_group': 100,                          # Min number of data per categorical group
            'max_cat_threshold': 32,                            # Max threshold points in categorical features
            'cat_l2': 10.0,                                     # L2 regularization in categorical split
            'cat_smooth': 10.0,                                 # Categorical features smoothing
            'max_cat_to_onehot': 4,                             # Max number of categories to apply one-hot encoding
            'top_k': 20,                                        # Top K features in voting parallel
            'monotone_constraints': None,                       # Constraints of monotonic features (-1: decreasing, 0: non-constraint, 1: increasing)
            'feature_contri': None,                             # Control feature’s split gain
            'forcedsplits_filename': '',                        # JSON file that specifies splits to force at the top of every decision tree before best-first learning commences
            'refit_decay_rate': 0.9,                            # Decay rate of refit task
            'verbosity': 1,                                     # Verbose mode (<0: fatal, 0: error (warning), 1: info, >1: debug)
            'max_bin': 255,                                     # Max number of bins that feature values will be bucketed in
            'min_data_in_bin': 3,                               # Min number of data inside one bin
            'subsample_for_bin': 200000,                        # Total samples for constructing bins
            'histogram_pool_size': -1.0,                        # Max cache size in MB for historical histogram (<0: no limit)
            'data_random_seed': 1,                              # Random seed for data partition in parallel learning
            'output_model': 'models/lightgbm.txt',              # Filename of output model in training
            'snapshot_freq': -1,                                # Frequency of saving model file snapshot
            'input_model': '',                                  # Filename of input model
            'output_result': 'outputs/lightgbm_result.txt',     # Filename of prediction result in prediction task
            'initscore_filename': '',                           # Filename with training initial scores 
            'valid_data_initscores': '',                        # Filename with validation initial scores
            'pre_partition': False,                             # Used for parallel learning
            'enable_bundle': True,                              # Enable or disable Exclusive Feature Bundling (EFB)
            'max_conflict_rate': 0.0,                           # Max conflict rate for bundles in EFB
            'is_enable_sparse': True,                           # Enable/disable sparse optimization
            'sparse_threshold': 0.8,                            # Threshold of zero elements percentage for treating a feature as a sparse one
            'use_missing': True,                                # Enable or disable the special handle of missing value
            'zero_as_missing': False,                           # Method to treat zero values
            'two_round': False,                                 # Map data file into two rounds if data file is too big to fit in memory
            'save_binary': False,                               # Save the dataset (including validation data) to a binary file
            'header': False,                                    # Set to True if input data has header
            'label_column': '',                                 # Specify the label column
            'weight_column': '',                                # Specify the weight column
            'group_column': '',                                 # Specify the group column
            'ignore_column': '',                                # Specify some ignoring columns in training
            'categorical_feature': '',                          # Specify categorical features
            'predict_raw_score': False,                         # Prediction method (True: raw scores, False: transformed scores)
            'predict_leaf_index': False,                        # Specify whether to predict with leaf index of all trees
            'predict_contrib': False,                           # Specify whether to estimate SHAP values
            'num_iteration_predict': -1,                        # Specify how many trained iterations will be used in prediction (<=0: no limit)
            'pred_early_stop': False,                           # Use early-stopping to speed up the prediction
            'pred_early_stop_freq': 10,                         # Frequency of checking early-stopping prediction
            'pred_early_stop_margin': 10.0,                     # Threshold of margin in early-stopping prediction
            'boost_from_average': True,                         # Adjusts initial score to the mean of labels for faster convergence
            'reg_sqrt': False,                                  # Used to fit sqrt(label) instead of original values 
            'alpha': 0.9,                                       # Parameter for Huber loss and Quantile regression
            'fair_c': 1.0,                                      # Parameter for Fair loss
            'poisson_max_delta_step': 0.7,                      # Parameter for Poisson regression to safeguard optimization
            'tweedie_variance_power': 1.5,                      # Control the variance of the tweedie distribution
            'metric': 'rmse',                                   # Evaluation metrics
            'metric_freq': 1,                                   # Frequency to calculate metric values
            'num_machines': 1,                                  # Number of machines for parallel learning application
            'local_listen_port': 12400,                         # TCP listen port for local machines
            'time_out': 120,                                    # Socket time-out in minutes
            'machine_list_filename': '',                        # Filename that lists machines for parallel learning application
            'machines': '',                                     # List of machines in the following format: ip1:port1,ip2:port2
            'gpu_platform_id': -1,                              # OpenCL platform ID (-1: system-wide default platform)
            'gpu_device_id': -1,                                # OpenCL device ID in the specified platform (-1: default device in the selected platform)
            'gpu_use_dp': False,                                # Specify whether to use double precision math on GPU
            'init_score': None,                                 # Initial prediction score of all instances (global bias)
            'eval_init_score': None,                            # Initial prediction score of all evaluation instances (global bias)
            'verbose_eval': 50                                  # Evaluation verbose frequency
        },
        'file': {
            'path': 'models',                                   # Path to load and save the model
            'name': 'lightgbm',                                 # Filename of the model to be saved
            'format': 'pkl'                                     # File format of the model file
        }
    }

    def __init__(self, model_parameters=None, tuner_parameters=None):
        """ Constructor """
        self.__parameters = copy.deepcopy(self.__defaults)
        # Set defined parameter values if present
        if model_parameters is not None:
            self.__parameters['model'].update(model_parameters['model'])
        self.__model = lgb.LGBMRegressor(**(self.__parameters['model']))
        # Instantiate hyperparameter tuner
        self.__tuner = HyperparameterTuner(tuner_parameters)

    def create_quantile_models(self, alpha=None):
        """ Create quantile regression models """
        if alpha is not None:
            self.__parameters['model']['alpha'] = alpha
        parameters = self.__parameters['model']
        parameters['objective'] = 'quantile'
        parameters['alpha'] = parameters['alpha'] if parameters['alpha'] < 0.5 else (1 - parameters['alpha'])
        self.__lower_quantile_model = lgb.LGBMRegressor(**parameters)
        parameters['alpha'] = 1 - parameters['alpha']
        self.__upper_quantile_model = lgb.LGBMRegressor(**parameters)

    def load_model(self, identifier=None, is_incremental=False):
        """ Load the model """
        print('Loading the model from a file')
        self.__is_incremental = is_incremental
        identifier = ('_' + identifier) if identifier is not None else ''
        name = self.__parameters['file']['path'] + '/' + \
               self.__parameters['file']['name'] + identifier + '.' + self.__parameters['file']['format']
        self.__model = joblib.load(name)
        if self.__is_incremental == True:
            name = self.__parameters['file']['path'] + '/' + \
                   self.__parameters['file']['name'] + '_incremental' + identifier + '.' + self.__parameters['file']['format']
            self.__models = joblib.load(name)

    def load_quantile_model(self, identifier=None, is_incremental=False):
        """ Load the quantile regression model """
        print('Loading the quantile regression model from a file')
        self.__is_incremental = is_incremental
        identifier = ('_' + identifier) if identifier is not None else ''
        name = self.__parameters['file']['path'] + '/' + \
               self.__parameters['file']['name'] + '_lower_quantile' + identifier + '.' + self.__parameters['file']['format']
        self.__lower_quantile_model = joblib.load(name)
        self.__upper_quantile_model = joblib.load(name.replace('lower_quantile', 'upper_quantile'))
        if self.__is_incremental == True:
            name = self.__parameters['file']['path'] + '/' + \
                   self.__parameters['file']['name'] + '_lower_quantile_incremental' + identifier + '.' + self.__parameters['file']['format']
            self.__lower_quantile_models = joblib.load(name)
            self.__upper_quantile_models = joblib.load(name.replace('lower_quantile', 'upper_quantile'))

    def save_model(self, identifier=None):
        """ Save the model """
        print('Saving the model to a file')
        identifier = ('_' + identifier) if identifier is not None else ''
        if self.__is_trained == True:
            name = self.__parameters['file']['path'] + '/' + \
                   self.__parameters['file']['name'] + identifier + '.' + self.__parameters['file']['format']
            joblib.dump(self.__model, name)
            if self.__is_incremental == True:
                name = self.__parameters['file']['path'] + '/' + \
                       self.__parameters['file']['name'] + '_incremental' + identifier + '.' + self.__parameters['file']['format']
                joblib.dump(self.__models, name)

    def save_quantile_model(self, identifier=None):
        """ Save the quantile regression model """
        print('Saving the quantile regression model to a file')
        identifier = ('_' + identifier) if identifier is not None else ''
        if self.__is_trained == True:
            name = self.__parameters['file']['path'] + '/' + \
                   self.__parameters['file']['name'] + '_lower_quantile' + identifier + '.' + self.__parameters['file']['format']
            joblib.dump(self.__lower_quantile_model, name)
            joblib.dump(self.__upper_quantile_model, name.replace('lower_quantile', 'upper_quantile'))
            if self.__is_incremental == True:
                name = self.__parameters['file']['path'] + '/' + \
                       self.__parameters['file']['name'] + '_lower_quantile_incremental' + identifier + '.' + self.__parameters['file']['format']
                joblib.dump(self.__lower_quantile_models, name)
                joblib.dump(self.__upper_quantile_models, name.replace('lower_quantile', 'upper_quantile'))

    def get_parameters(self):
        """ Get the model parameters """
        return self.__parameters

    def tune_hyperparameters(self, parameters_list, train_data, train_labels, validate_data=None, validate_labels=None, is_incremental=False, methods='random'):
        """ Perform hyperparameter tuning """
        print('Performing hyperparameter tuning on the Light GBM model')

        def optimize_bayesian(
            learning_rate, num_leaves, max_depth, min_child_samples, min_child_weight, bagging_fraction, 
            bagging_freq, feature_fraction, lambda_l1, lambda_l2, min_split_gain  
        ):
            """ Perform Bayesian optimization """
            parameters = self.__parameters['model']
            parameters.update({
                'learning_rate': learning_rate,
                'num_leaves': int(round(num_leaves)),
                'max_depth': int(round(max_depth)),
                'min_child_samples': int(round(min_child_samples)),
                'min_child_weight': min_child_weight,
                'bagging_fraction': bagging_fraction,
                'bagging_freq': int(round(bagging_freq)),
                'feature_fraction': feature_fraction,
                'lambda_l1': lambda_l1,
                'lambda_l2': lambda_l2,
                'min_split_gain': min_split_gain
            })
            self.__model = lgb.LGBMRegressor(**parameters)
            self.train(train_data, train_labels, validate_data, validate_labels, is_incremental=is_incremental)
            if validate_data is not None and validate_labels is not None:
                if is_incremental == False:
                    score = np.mean(list(self.get_performance_metrics(validate_data, validate_labels).values()))
                else:
                    score = np.mean([np.mean(
                        list(self.get_performance_metrics(data_partition, label_partition).values())
                    ) for data_partition, label_partition in zip(validate_data, validate_labels)])
            else:
                if is_incremental == False:
                    score = np.mean(list(self.get_performance_metrics(train_data, train_labels).values()))
                else:
                    score = np.mean([np.mean(
                        list(self.get_performance_metrics(data_partition, label_partition).values())
                    ) for data_partition, label_partition in zip(train_data, train_labels)])
            if self.__best_score is None or self.__best_score > score:
                self.__best_model = copy.deepcopy(self.__model)
                self.__best_score = score
            return -score

        final_model = None
        final_parameters = None
        final_score = None
        train_parameters = {
            'eval_set': [(validate_data, validate_labels)] if validate_data is not None and validate_labels is not None else [(None, None)],
            'eval_metric': self.__parameters['model']['metric'],
            'init_score': self.__parameters['model']['init_score'],
            'eval_init_score': self.__parameters['model']['eval_init_score'],
            'early_stopping_rounds': self.__parameters['model']['early_stopping_rounds'],
            'verbose': (self.__parameters['model']['verbose_eval'] if self.__parameters['model']['verbosity'] > 0 else False)
        }
        tuner_parameters = self.__tuner.get_parameters()
        for method in methods:
            best_model = None
            best_parameters = None
            best_score = None
            if 'bayesian' in method.lower():
                print('Performing the Bayesian search')
                parameters = self.__tuner.get_parameters()['bayesian_search']
                optimizer = BayesianOptimization(
                    f=optimize_bayesian,
                    pbounds=parameters_list['bayesian'],
                    random_state=parameters['random_state'],
                    verbose=parameters['verbose']
                )
                optimizer.maximize(init_points=parameters['init_points'], n_iter=parameters['n_iter'])
                best_model = self.__best_model
                best_parameters = optimizer.max['params']
                best_parameters['num_leaves'] = int(round(best_parameters['num_leaves']))
                best_parameters['max_depth'] = int(round(best_parameters['max_depth']))
                best_parameters['min_child_samples'] = int(round(best_parameters['min_child_samples']))
                best_parameters['bagging_freq'] = int(round(best_parameters['bagging_freq']))
                best_score = -optimizer.max['target']
            elif 'grid' in method.lower():
                print('Performing the grid search')
                if is_incremental == False:
                    best_model, best_parameters, best_score, _ = self.__tuner.perform_grid_search(
                        self.__model, parameters_list['grid'], train_data, train_labels, train_parameters
                    )
                else:
                    for parameters in tqdm(list(ParameterGrid(parameters_list['grid'])), ascii=True):
                        model_parameters = self.get_parameters().update(parameters)
                        self.__model = lgb.LGBMRegressor(**model_parameters)
                        self.train_incremental(self.__model, model_parameters, train_data, train_labels, validate_data, validate_labels)
                        if validate_data is not None and validate_labels is not None:
                            score = np.mean(list(self.get_performance_metrics(validate_data, validate_labels).values()))
                        else:
                            score = np.mean(list(self.get_performance_metrics(train_data, train_labels).values()))
                        if best_score is None or best_score > score:
                            best_model = copy.deepcopy(self.__model)
                            best_parameters = parameters
                            best_score = score
            elif 'random' in method.lower():
                print('Performing the randomized search')
                if is_incremental == False:
                    best_model, best_parameters, best_score, _ = self.__tuner.perform_randomized_search(
                        self.__model, parameters_list['random'], train_data, train_labels, train_parameters
                    )
                else:
                    random_parameters = list(ParameterSampler(
                        parameters_list['random'],
                        n_iter=tuner_parameters['randomized_search']['iterations'],
                        random_state=tuner_parameters['randomized_search']['random_seed']
                    ))
                    for parameters in tqdm(random_parameters, ascii=True):
                        model_parameters = self.get_parameters().update(parameters)
                        self.__model = lgb.LGBMRegressor(**model_parameters)
                        self.train_incremental(self.__model, model_parameters, train_data, train_labels, validate_data, validate_labels)
                        if validate_data is not None and validate_labels is not None:
                            score = np.mean(list(self.get_performance_metrics(validate_data, validate_labels).values()))
                        else:
                            score = np.mean(list(self.get_performance_metrics(train_data, train_labels).values()))
                        if best_score is None or best_score > score:
                            best_model = copy.deepcopy(self.__model)
                            best_parameters = parameters
                            best_score = score
            if best_score is not None and (final_model is None or final_score < best_score):
                parameters = self.__parameters
                parameters['model'].update(best_parameters)
                final_model = best_model
                final_parameters = parameters
                final_score = best_score
        return final_model, final_parameters, final_score

    def cross_validate(self, data, labels, num_splits=2, shuffle=True):
        """ Perform cross validation (incremental learning is not supported) """
        scores = 0
        folds = StratifiedKFold(n_splits=num_splits, random_state=self.__parameters['model']['random_state'], shuffle=shuffle)
        sorted_data = data.reset_index(drop=True)
        sorted_labels = labels.reset_index(drop=True)
        for train_indices, validate_indices in folds.split(data, labels):
            model = copy.deepcopy(self.__model)
            model.fit(
                sorted_data.ix[train_indices],
                sorted_labels.ix[train_indices],
                eval_set=[(sorted_data.ix[validate_indices], sorted_labels.ix[validate_indices])],
                eval_metric=self.__parameters['model']['metric'],
                init_score=self.__parameters['model']['init_score'],
                eval_init_score=self.__parameters['model']['eval_init_score'],
                early_stopping_rounds=self.__parameters['model']['early_stopping_rounds'],
                verbose=(self.__parameters['model']['verbose_eval'] if self.__parameters['model']['verbosity'] > 0 else False)
            )
            scores += model.best_score_['valid_0'][self.__parameters['model']['metric']]
        return scores / num_splits

    def train_single(self, model, parameters, train_data, train_labels, validate_data=None, validate_labels=None):
        """ Train the single model """
        model.fit(
            train_data,
            train_labels,
            eval_set=[(validate_data, validate_labels)],
            eval_metric=self.__parameters['model']['metric'],
            init_score=self.__parameters['model']['init_score'],
            eval_init_score=self.__parameters['model']['eval_init_score'],
            early_stopping_rounds=self.__parameters['model']['early_stopping_rounds'],
            verbose=(self.__parameters['model']['verbose_eval'] if self.__parameters['model']['verbosity'] > 0 else False)
        )
        return model

    def train_incremental(self, model, parameters, train_data, train_labels, validate_data=None, validate_labels=None):
        """ Train the model incrementally """
        models = []
        for i in range(len(train_data)):
            print('Fold:', i)
            models.append(copy.deepcopy(model))
            validate_set = [(validate_data[i], validate_labels[i])] if validate_data is not None and validate_labels is not None else [(None, None)]
            models[i].fit(
                train_data[i],
                train_labels[i],
                eval_set=validate_set,
                eval_metric=parameters['model']['metric'],
                init_score=parameters['model']['init_score'],
                eval_init_score=parameters['model']['eval_init_score'],
                early_stopping_rounds=parameters['model']['early_stopping_rounds'],
                verbose=(parameters['model']['verbose_eval'] if parameters['model']['verbosity'] > 0 else False)
            )
        return models

    def train(self, train_data, train_labels, validate_data=None, validate_labels=None, is_incremental=False):
        """ Train the model and optionally test it on validation data """
        self.__is_incremental = is_incremental
        if self.__is_incremental == False:
            self.__model = self.train_single(self.__model, self.__parameters, train_data, train_labels, validate_data, validate_labels)
        else:
            self.__models = self.train_incremental(self.__model, self.__parameters, train_data, train_labels, validate_data, validate_labels)
        self.__is_trained = True

    def train_quantile(self, train_data, train_labels, validate_data=None, validate_labels=None, is_incremental=False, alpha=None):
        """ Train the quantile regression models and optionally test them on validation data """
        # Create quantile regression models
        self.create_quantile_models(alpha)
        # Train the models
        self.__is_incremental = is_incremental
        if self.__is_incremental == False:
            self.__lower_quantile_model = self.train_single(self.__lower_quantile_model, self.__parameters, train_data, train_labels, validate_data, validate_labels)
            self.__upper_quantile_model = self.train_single(self.__upper_quantile_model, self.__parameters, train_data, train_labels, validate_data, validate_labels)
        else:
            self.__lower_quantile_models = self.train_incremental(self.__lower_quantile_model, self.__parameters, train_data, train_labels, validate_data, validate_labels)
            self.__upper_quantile_models = self.train_incremental(self.__upper_quantile_model, self.__parameters, train_data, train_labels, validate_data, validate_labels)
        self.__is_quantile_trained = True

    def predict(self, test_data):
        """ Perform prediction on the data """
        if self.__is_incremental == False:
            return self.__model.predict(test_data, num_iteration=self.__model._best_iteration)
        else:
            return np.mean([model.predict(test_data, num_iteration=model._best_iteration) for model in self.__models], axis=0)

    def predict_quantile(self, test_data):
        """ Perform quantile prediction on the data """
        if self.__is_incremental == False:
            lower_quantiles = self.__lower_quantile_model.predict(test_data, num_iteration=self.__model._best_iteration)
            upper_quantiles = self.__upper_quantile_model.predict(test_data, num_iteration=self.__model._best_iteration)
        else:
            lower_quantiles = np.mean([model.predict(test_data, num_iteration=model._best_iteration) for model in self.__lower_quantile_models], axis=0)
            upper_quantiles = np.mean([model.predict(test_data, num_iteration=model._best_iteration) for model in self.__upper_quantile_models], axis=0)
        return lower_quantiles, upper_quantiles

    def get_performance_metrics(self, data, labels):
        """ Get the performance metrics of the trained model """
        metrics = {}
        if self.__is_incremental == False:
            metrics[self.__parameters['model']['metric']] = np.sqrt(mean_squared_error(self.predict(data), labels))
        else:
            for index, model in enumerate(self.__models):
                metrics[(self.__parameters['model']['metric'] + '_' + str(index))] = np.sqrt(mean_squared_error(model.predict(data), labels))
        return metrics

    def get_feature_importance(self, train_data, train_labels, validate_data=None, validate_labels=None, is_incremental=False, method='permutation'):
        """ Get the importance of each feature """
        self.__is_incremental = is_incremental
        feature_names = (train_data[0] if self.__is_incremental == True else train_data).columns
        feature_importances = np.zeros(len(feature_names))
        feature_importances_std = np.zeros(len(feature_names))
        # Train the model(s)
        self.train(train_data, train_labels, validate_data, validate_labels, is_incremental)
        # Get the permutation importance
        if method.lower() == 'permutation':
            if self.__is_incremental == False:
                permutation = PermutationImportance(
                    self.__model, 
                    random_state=self.__parameters['model']['random_state']
                ).fit(
                    train_data.replace([np.inf, -np.inf], np.nan).fillna(0),
                    train_labels.replace([np.inf, -np.inf], np.nan).fillna(0),
                    eval_set=[(validate_data, validate_labels)] if validate_data is not None and validate_labels is not None else [(None, None)],
                    eval_metric=self.__parameters['model']['metric'],
                    init_score=self.__parameters['model']['init_score'],
                    eval_init_score=self.__parameters['model']['eval_init_score'],
                    early_stopping_rounds=self.__parameters['model']['early_stopping_rounds'],
                    verbose=(self.__parameters['model']['verbose_eval'] if self.__parameters['model']['verbosity'] > 0 else False)
                )
                feature_importances = permutation.feature_importances_
                feature_importances_std = permutation.feature_importances_std_
            else:
                for model in self.__models:
                    for i in range(len(train_data)):
                        permutation = PermutationImportance(
                            model,
                            random_state=self.__parameters['model']['random_state']
                        ).fit(
                            train_data[i].replace([np.inf, -np.inf], np.nan).fillna(0),
                            train_labels[i].replace([np.inf, -np.inf], np.nan).fillna(0),
                            eval_set=[(validate_data[i], validate_labels[i])] if validate_data is not None and validate_labels is not None else [(None, None)],
                            eval_metric=self.__parameters['model']['metric'],
                            init_score=self.__parameters['model']['init_score'],
                            eval_init_score=self.__parameters['model']['eval_init_score'],
                            early_stopping_rounds=self.__parameters['model']['early_stopping_rounds'],
                            verbose=(self.__parameters['model']['verbose_eval'] if self.__parameters['model']['verbosity'] > 0 else False)
                        )
                        feature_importances += permutation.feature_importances_
                        feature_importances_std += permutation.feature_importances_std_
                feature_importances /= (len(self.__models) * len(train_data))
                feature_importances_std /= (len(self.__models) * len(train_data))
            features = pd.DataFrame({
                'Feature': feature_names,
                'Score': feature_importances,
                'Score Std': feature_importances_std
            }).sort_values(by='Score', ascending=False, inplace=False, na_position='last')
        # Get the simple permutation importance
        else:
            if self.__is_incremental == False:
                feature_importances = self.__model.feature_importances_
            else:
                for model in self.__models:
                    feature_importances += model.feature_importances_
                feature_importances /= len(self.__models)
            features = pd.DataFrame({
                'Feature': feature_names,
                'Score': feature_importances
            }).sort_values(by='Score', ascending=False, inplace=False, na_position='last')
        return features
