import copy
import fbprophet
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from bayes_opt import BayesianOptimization
from fbprophet.plot import add_changepoints_to_plot
from sklearn.ensemble import GradientBoostingRegressor
from sklearn.externals import joblib
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import ParameterGrid, ParameterSampler, StratifiedKFold
from sys import platform
from tqdm import tqdm
from kpi_forecast.utils.hyperparameter_tuner import HyperparameterTuner

class Prophet(object):
    """ Prophet regressor model """
    __model = None
    __models = []
    __lower_quantile_model = None
    __lower_quantile_models = []
    __upper_quantile_model = None
    __upper_quantile_models = []
    __best_score = None    
    __best_model = None
    __tuner = None
    __is_incremental = False
    __is_trained = False
    __is_quantile_trained = False
    __parameters = None
    __defaults = {
        'model': {
            'growth': 'linear',                 # Trend (linear, logistic)
            'changepoints': None,               # List of dates to include potential trend changepoints
            'n_changepoints': 25,               # Total potential changepoints to include
            'changepoint_range': 0.8,           # Proportion of history data in which changepoints will be estimated
            'yearly_seasonality': True,         # Fit yearly seasonality (auto, True, False, total Fourier terms)
            'quarterly_seasonality': True,      # Fit quarterly seasonality (auto, True, False, total Fourier terms)
            'monthly_seasonality': True,        # Fit monthly seasonality (auto, True, False, total Fourier terms)
            'weekly_seasonality': True,         # Fit weekly seasonality (auto, True, False, total Fourier terms)
            'daily_seasonality': True,          # Fit daily seasonality (auto, True, False, total Fourier terms)
            'holidays': None,                   # DataFrame (columns = [holiday, ds]), optional columns = [lower_window, upper_window] which specify days around the date to be included as holidays
            'seasonality_mode': 'additive',     # Seasonality mode (additive, multiplicative)
            'seasonality_prior_scale': 10.0,    # Modulates the seasonality model strength (larger values allows the model to fit larger seasonal fluctuations)
            'holidays_prior_scale': 10.0,       # Modulates the holiday components model strength
            'changepoint_prior_scale': 0.25,    # Modulates the flexibility of the automatic changepoint selection (larger values denote more changepoints)
            'mcmc_samples': 0,                  # Total MCMC samples (0: MAP estimation, >0: full Bayesian inference with specified total MCMC samples)
            'interval_width': 0.8,              # Width of the uncertainty intervals 
            'uncertainty_samples': 1000,        # Total simulated draws used to estimate uncertainty intervals
            'logistic_growth_floor': 1.5,       # Saturating minimum for logistic growth model
            'logistic_growth_cap': 6.0,         # Saturating maximum for logistic growth model
            'periods_before_holiday': 0,        # Periods before the holiday to be considered having the holiday effect
            'periods_after_holiday': 0,         # Periods after the holiday to be considered having the holiday effect
            'periods_per_quarter': 90.5625,     # Total periods per quarter
            'periods_per_month': 30.4375,       # Total periods per month
            'fourier_order': 5,                 # Default Fourier order
            'random_seed': 0                    # Random seed
        },
        'extras': {
            'learning_rate': 0.1,               # Learning rate
            'n_estimators': 100,                # Total boosting stages to perform
            'subsample': 1.0,                   # Fraction of samples used for fitting the individual base learners
            'max_depth': 3,                     # Maximum depth of the individual regression estimators
            'alpha': 0.9,                       # Parameter for Quantile regression
            'verbose': 0                        # Verbosity (higher value denotes more verbosity)
        },
        'file': {
            'path': 'models',                   # Path to load and save the model
            'name': 'prophet',                  # Filename of the model to be saved
            'format': 'pkl'                     # File format of the model file
        }
    }

    def __init__(self, model_parameters=None, tuner_parameters=None):
        """ Constructor """
        self.__parameters = copy.deepcopy(self.__defaults)
        # Set defined parameter values if present
        if model_parameters is not None:
            self.__parameters['model'].update(model_parameters['model'])
        # Create a new Prophet model
        self.__model = self.create_model()
        # Instantiate hyperparameter tuner
        self.__tuner = HyperparameterTuner(tuner_parameters)

    def create_model(self):
        """ Create a new Prophet model """
        model = fbprophet.Prophet(
            growth=self.__parameters['model']['growth'],
            changepoints=self.__parameters['model']['changepoints'],
            n_changepoints=self.__parameters['model']['n_changepoints'],
            changepoint_range=self.__parameters['model']['changepoint_range'],
            yearly_seasonality=self.__parameters['model']['yearly_seasonality'],
            weekly_seasonality=self.__parameters['model']['weekly_seasonality'],
            daily_seasonality=self.__parameters['model']['daily_seasonality'],
            holidays=self.__parameters['model']['holidays'],
            seasonality_mode=self.__parameters['model']['seasonality_mode'],
            seasonality_prior_scale=self.__parameters['model']['seasonality_prior_scale'],
            holidays_prior_scale=self.__parameters['model']['holidays_prior_scale'],
            changepoint_prior_scale=self.__parameters['model']['changepoint_prior_scale'],
            mcmc_samples=self.__parameters['model']['mcmc_samples'],
            interval_width=self.__parameters['model']['interval_width'],
            uncertainty_samples=self.__parameters['model']['uncertainty_samples']
        )
        # Add quarterly seasonality
        if self.__parameters['model']['quarterly_seasonality'] != False:
            if isinstance(self.__parameters['model']['quarterly_seasonality'], int) and self.__parameters['model']['quarterly_seasonality'] > 0:
                model.add_seasonality(
                    name='quarterly',
                    period=self.__parameters['model']['periods_per_quarter'],
                    mode=model.seasonality_mode,
                    fourier_order=self.__parameters['model']['quarterly_seasonality']
                )
            else:
                model.add_seasonality(
                    name='quarterly', 
                    period=self.__parameters['model']['periods_per_quarter'],
                    mode=model.seasonality_mode,
                    fourier_order=self.__parameters['model']['fourier_order']
                )
        # Add monthly seasonality
        if self.__parameters['model']['monthly_seasonality'] != False:
            if isinstance(self.__parameters['model']['monthly_seasonality'], int) and self.__parameters['model']['monthly_seasonality'] > 0:
                model.add_seasonality(
                    name='monthly', 
                    period=self.__parameters['model']['periods_per_month'],
                    mode=model.seasonality_mode,
                    fourier_order=self.__parameters['model']['quarterly_seasonality']
                )
            else:
                model.add_seasonality(
                    name='monthly',
                    period=self.__parameters['model']['periods_per_month'],
                    mode=model.seasonality_mode,
                    fourier_order=self.__parameters['model']['fourier_order']
                )
        return model

    def create_quantile_models(self, alpha=None):
        """ Create quantile regression models """
        if alpha is not None:
            self.__parameters['extras']['alpha'] = alpha
        alpha = self.__parameters['extras']['alpha'] if self.__parameters['extras']['alpha'] < 0.5 else (1 - self.__parameters['extras']['alpha'])
        parameters = {
            'loss': 'quantile',
            'learning_rate': self.__parameters['extras']['learning_rate'],
            'n_estimators': self.__parameters['extras']['n_estimators'],
            'subsample': self.__parameters['extras']['subsample'],
            'max_depth': self.__parameters['extras']['max_depth'],
            'random_state': self.__parameters['model']['random_seed'],
            'alpha': alpha,
            'verbose': self.__parameters['extras']['verbose']
        }
        self.__lower_quantile_model = GradientBoostingRegressor(**parameters)
        parameters['alpha'] = 1 - parameters['alpha']
        self.__upper_quantile_model = GradientBoostingRegressor(**parameters)

    def load_model(self, identifier=None, is_incremental=False):
        """ Load the model """
        print('Loading the model from a file')
        self.__is_incremental = is_incremental
        identifier = ('_' + identifier) if identifier is not None else ''
        name = self.__parameters['file']['path'] + '/' + \
               self.__parameters['file']['name'] + identifier + '.' + self.__parameters['file']['format']
        self.__model = joblib.load(name)
        if self.__is_incremental == True:
            name = self.__parameters['file']['path'] + '/' + \
                   self.__parameters['file']['name'] + '_incremental' + identifier + '.' + self.__parameters['file']['format']
            self.__models = joblib.load(name)

    def load_quantile_model(self, identifier=None, is_incremental=False):
        """ Load the quantile regression model """
        print('Loading the quantile regression model from a file')
        self.__is_incremental = is_incremental
        identifier = ('_' + identifier) if identifier is not None else ''
        name = self.__parameters['file']['path'] + '/' + \
               self.__parameters['file']['name'] + '_lower_quantile' + identifier + '.' + self.__parameters['file']['format']
        self.__lower_quantile_model = joblib.load(name)
        self.__upper_quantile_model = joblib.load(name.replace('lower_quantile', 'upper_quantile'))
        if self.__is_incremental == True:
            name = self.__parameters['file']['path'] + '/' + \
                   self.__parameters['file']['name'] + '_lower_quantile_incremental' + identifier + '.' + self.__parameters['file']['format']
            self.__lower_quantile_models = joblib.load(name)
            self.__upper_quantile_models = joblib.load(name.replace('lower_quantile', 'upper_quantile'))

    def save_model(self, identifier=None):
        """ Save the model """
        print('Saving the model to a file')
        identifier = ('_' + identifier) if identifier is not None else ''
        if self.__is_trained == True:
            name = self.__parameters['file']['path'] + '/' + \
                   self.__parameters['file']['name'] + identifier + '.' + self.__parameters['file']['format']
            joblib.dump(self.__model, name)
            if self.__is_incremental == True:
                name = self.__parameters['file']['path'] + '/' + \
                       self.__parameters['file']['name'] + '_incremental' + identifier + '.' + self.__parameters['file']['format']
                joblib.dump(self.__models, name)

    def save_quantile_model(self, identifier=None):
        """ Save the quantile regression model """
        print('Saving the quantile regression model to a file')
        identifier = ('_' + identifier) if identifier is not None else ''
        if self.__is_trained == True:
            name = self.__parameters['file']['path'] + '/' + \
                   self.__parameters['file']['name'] + '_lower_quantile' + identifier + '.' + self.__parameters['file']['format']
            joblib.dump(self.__lower_quantile_model, name)
            joblib.dump(self.__upper_quantile_model, name.replace('lower_quantile', 'upper_quantile'))
            if self.__is_incremental == True:
                name = self.__parameters['file']['path'] + '/' + \
                       self.__parameters['file']['name'] + '_lower_quantile_incremental' + identifier + '.' + self.__parameters['file']['format']
                joblib.dump(self.__lower_quantile_models, name)
                joblib.dump(self.__upper_quantile_models, name.replace('lower_quantile', 'upper_quantile'))

    def get_parameters(self):
        """ Get the model parameters """
        return self.__parameters

    def set_changepoints(self, dates, changepoint_prior_scale=None):
        """ Set changepoints based on the dates """
        self.__model.changepoints = dates.tolist()
        if changepoint_prior_scale is not None:
            self.__model.changepoint_prior_scale = changepoint_prior_scale

    def set_holidays(self, holidays, holidays_prior_scale=None):
        holiday_data = holidays.copy()
        holiday_data.rename(columns={
            'Holiday': 'holiday',
            'Date': 'ds'
        }, inplace=True)
        holiday_data['lower_window'] = self.__parameters['model']['periods_before_holiday']
        holiday_data['upper_window'] = self.__parameters['model']['periods_after_holiday']
        self.__model.holidays = holiday_data
        if holidays_prior_scale is not None:
            self.__model.holidays_prior_scale = holidays_prior_scale

    def add_new_regressor(self, regressor_data, column_name, data):
        """ Add new regressor to include additional effect 

            Parameters
            ----------
            regressor_data: DataFrame containing dates with additional effect
            column_name: The column in the regressor_data DataFrame to be included
            data: input DataFrame
        """
        data[column_name] = regressor_data[column_name]
        self.__model.add_regressor(column_name)

    def parse_train_data(self, data, labels):
        """ Parse the train data """
        parsed_data = pd.DataFrame(pd.to_datetime(dict(
            year=data['Year'], 
            month=data['MonthOfYear'], 
            day=data['DayOfMonth'])),
            columns={'ds'}
        )
        merged_data = pd.concat([parsed_data, labels], axis=1)
        merged_data.rename(columns={list(labels.columns.values)[0]: 'y'}, inplace=True)
        return merged_data

    def parse_test_data(self, data):
        """ Parse the test data """
        return pd.DataFrame(pd.to_datetime(dict(
            year=data['Year'], 
            month=data['MonthOfYear'], 
            day=data['DayOfMonth'])),
            columns={'ds'}
        )

    def process_data(self, data):
        """ Process the data based on the specified trend """
        if self.__model.growth == 'logistic':
            output_data = data.copy()
            output_data['floor'] = self.__parameters['model']['logistic_growth_floor']
            output_data['cap'] = self.__parameters['model']['logistic_growth_cap']
            return output_data
        else:
            return data

    def tune_hyperparameters(self, parameters_list, train_data, train_labels, validate_data=None, validate_labels=None, is_incremental=False, methods='random'):
        """ Perform hyperparameter tuning """
        print('Perform hyperparameter tuning on the Prophet model')

        def optimize_bayesian(n_changepoints, seasonality_prior_scale, holidays_prior_scale, changepoint_prior_scale, fourier_order):
            """ Perform Bayesian optimization """
            parameters = self.__parameters['model']
            parameters.update({
                'n_changepoints': int(round(n_changepoints)),
                'seasonality_prior_scale': int(round(seasonality_prior_scale)),
                'holidays_prior_scale': int(round(holidays_prior_scale)),
                'changepoint_prior_scale': changepoint_prior_scale,
                'fourier_order': int(round(fourier_order))
            })
            self.__model = self.create_model()
            self.train(train_data, train_labels, is_incremental=is_incremental)
            if validate_data is not None and validate_labels is not None:
                if is_incremental == False:
                    score = np.mean(list(self.get_performance_metrics(validate_data, validate_labels).values()))
                else:
                    score = np.mean([np.mean(
                        list(self.get_performance_metrics(data_partition, label_partition).values())
                    ) for data_partition, label_partition in zip(validate_data, validate_labels)])
            else:
                if is_incremental == False:
                    score = np.mean(list(self.get_performance_metrics(train_data, train_labels).values()))
                else:
                    score = np.mean([np.mean(
                        list(self.get_performance_metrics(data_partition, label_partition).values())
                    ) for data_partition, label_partition in zip(train_data, train_labels)])
            if self.__best_score is None or self.__best_score > score:
                self.__best_model = copy.deepcopy(self.__model)
                self.__best_score = score
            return -score

        final_model = None
        final_parameters = None
        final_score = None
        tuner_parameters = self.__tuner.get_parameters()
        for method in methods:
            best_model = None
            best_parameters = None
            best_score = None
            if 'bayesian' in method.lower():
                print('Performing the Bayesian search')
                parameters = self.__tuner.get_parameters()['bayesian_search']
                optimizer = BayesianOptimization(
                    f=optimize_bayesian,
                    pbounds=parameters_list['bayesian'],
                    random_state=parameters['random_state'],
                    verbose=parameters['verbose']
                )
                optimizer.maximize(init_points=parameters['init_points'], n_iter=parameters['n_iter'])
                best_parameters = optimizer.max['params']
                best_parameters['n_changepoints'] = int(round(best_parameters['n_changepoints']))
                best_parameters['seasonality_prior_scale'] = int(round(best_parameters['seasonality_prior_scale']))
                best_parameters['holidays_prior_scale'] = int(round(best_parameters['holidays_prior_scale']))
                best_parameters['fourier_order'] = int(round(best_parameters['fourier_order']))
                self.__parameters['model'].update(best_parameters)
                best_model = self.create_model()
                best_score = -optimizer.max['target']
            elif 'grid' in method.lower():
                print('Performing the grid search')
                for parameters in tqdm(list(ParameterGrid(parameters_list['grid'])), ascii=True):
                    self.__parameters['model'].update(parameters)
                    self.__model = self.create_model()
                    if is_incremental == False:
                        score = self.cross_validate(train_data, train_labels, num_splits=tuner_parameters['grid_search']['folds'])
                    else:
                        self.train_incremental(self.__model, train_data, train_labels)
                        if validate_data is not None and validate_labels is not None:
                            score = np.mean(list(self.get_performance_metrics(validate_data, validate_labels).values()))
                        else:
                            score = np.mean(list(self.get_performance_metrics(train_data, train_labels).values()))
                    if best_score is None or best_score > score:
                        best_model = copy.deepcopy(self.__model)
                        best_parameters = parameters
                        best_score = score
            elif 'random' in method.lower():
                print('Performing the randomized search')
                random_parameters = list(ParameterSampler(
                    parameters_list['random'],
                    n_iter=tuner_parameters['randomized_search']['iterations'],
                    random_state=tuner_parameters['randomized_search']['random_seed']
                ))
                for parameters in tqdm(random_parameters, ascii=True):
                    self.__parameters['model'].update(parameters)
                    self.__model = self.create_model()
                    if is_incremental == False:
                        score = self.cross_validate(train_data, train_labels, num_splits=tuner_parameters['randomized_search']['folds'])
                    else:
                        self.train_incremental(self.__model, train_data, train_labels)
                        if validate_data is not None and validate_labels is not None:
                            score = np.mean(list(self.get_performance_metrics(validate_data, validate_labels).values()))
                        else:
                            score = np.mean(list(self.get_performance_metrics(train_data, train_labels).values()))
                    if best_score is None or best_score > score:
                        best_model = copy.deepcopy(self.__model)
                        best_parameters = parameters
                        best_score = score
            if best_score is not None and (final_model is None or final_score > best_score):
                parameters = self.__parameters
                parameters['model'].update(best_parameters)
                final_model = best_model
                final_parameters = parameters
                final_score = best_score
        return final_model, final_parameters, final_score

    def cross_validate(self, data, labels, num_splits=2, shuffle=True):
        """ Perform cross validation """
        scores = 0
        folds = StratifiedKFold(n_splits=num_splits, random_state=self.__parameters['model']['random_seed'], shuffle=shuffle)
        sorted_data = data.reset_index(drop=True)
        sorted_labels = labels.reset_index(drop=True)
        train_data = self.parse_train_data(sorted_data, sorted_labels)
        test_data = self.parse_test_data(sorted_data)
        # Create a new model if the existing model has been trained
        if self.__is_trained == True:
            self.__model = self.create_model()
        for train_indices, validate_indices in folds.split(data, labels):
            model = copy.deepcopy(self.__model)
            model.fit(self.process_data(train_data.ix[train_indices]))
            output_data = model.predict(self.process_data(test_data.ix[validate_indices]))['yhat']
            scores += np.sqrt(mean_squared_error(output_data, sorted_labels.ix[validate_indices]))
        return scores / num_splits

    def train_single(self, model, data, labels):
        """ Train the single model """
        model.fit(self.process_data(self.parse_train_data(data, labels)))
        return model

    def train_incremental(self, model, data, labels):
        """ Train the model incrementally """
        models = []
        for i in range(len(data)):
            print('Fold:', i)
            models.append(copy.deepcopy(model))
            models[i].fit(self.process_data(self.parse_train_data(data[i], labels[i])))
        return models

    def train(self, data, labels, is_incremental=False):
        """ Train the model """
        self.__is_incremental = is_incremental
        # Create a new model if the existing model has been trained
        if self.__is_trained == True:
            self.__model = self.create_model()
        if self.__is_incremental == False:
            self.__model = self.train_single(self.__model, data, labels)
        else:
            self.__models = self.train_incremental(self.__model, data, labels)
        self.__is_trained = True

    def train_quantile(self, train_data, train_labels, is_incremental=False, alpha=None):
        """ Train the quantile regression models and optionally test them on validation data """
        # Create quantile regression models
        self.create_quantile_models(alpha)
        # Train the models
        self.__is_incremental = is_incremental
        if self.__is_incremental == False:
            self.__lower_quantile_model.fit(train_data, train_labels)
            self.__upper_quantile_model.fit(train_data, train_labels)
        else:
            self.__lower_quantile_models = []
            self.__upper_quantile_models = []
            for i in range(len(train_data)):
                print('Fold:', i)
                self.__lower_quantile_models.append(copy.deepcopy(self.__lower_quantile_model))
                self.__lower_quantile_models[i].fit(train_data[i], train_labels[i])
                self.__upper_quantile_models.append(copy.deepcopy(self.__upper_quantile_model))
                self.__upper_quantile_models[i].fit(train_data[i], train_labels[i])
        self.__is_quantile_trained = True

    def predict(self, data, all_outputs=False):
        """ Perform prediction on the data """
        if self.__is_incremental == False:
            output_data = self.__model.predict(self.process_data(self.parse_test_data(data)))
            return output_data['yhat'] if all_outputs == False else output_data
        else:
            if all_outputs == False:
                return np.mean([model.predict(self.process_data(self.parse_test_data(data)))['yhat'] for model in self.__models], axis=0)
            else:
                output_data = {}
                outputs = [model.predict(self.process_data(self.parse_test_data(data))) for model in self.__models]
                keys = outputs[0].keys()
                for key in keys:
                    output_data[key] = np.mean([output[key] for output in outputs], axis=0)
                return output_data

    def predict_quantile(self, test_data):
        """ Perform quantile prediction on the data """
        if self.__is_incremental == False:
            lower_quantiles = self.__lower_quantile_model.predict(test_data)
            upper_quantiles = self.__upper_quantile_model.predict(test_data)
        else:
            lower_quantiles = np.mean([model.predict(test_data) for model in self.__lower_quantile_models], axis=0)
            upper_quantiles = np.mean([model.predict(test_data) for model in self.__upper_quantile_models], axis=0)
        return lower_quantiles, upper_quantiles

    def get_performance_metrics(self, data, labels):
        """ Get the performance metrics of the trained model """
        metrics = {}
        if self.__is_incremental == False:
            metrics['rmse'] = np.sqrt(mean_squared_error(self.predict(data), labels))
        else:
            for index, model in enumerate(self.__models):
                metrics[('rmse_' + str(index))] = np.sqrt(mean_squared_error(model.predict(data), labels))
        return metrics

    def get_feature_importance(self, data, labels, is_incremental=False, method='permutation'):
        """ Get the importance of each feature (this feature is not available in Prophet) """
        self.__is_incremental = is_incremental
        features = pd.DataFrame()
        features['Feature'] = data.columns
        features['Score'] = 0
        if method.lower() == 'permutation':
            features['Score Std'] = 0
        return features

    def plot_changepoints(self, data, output_path, title, save_figure=True, show_plot=False):
        """ Plot the prediction outputs and changepoints """
        models = []
        if self.__is_incremental == False:
            models.append(self.__model)
        else:
            models = self.__models
        output_data = self.predict(data, all_outputs=True)
        for index, model in enumerate(models):
            figure = model.plot(output_data)
            add_changepoints_to_plot(figure.gca(), model, output_data)
            plt.title('Forecast outputs and trend changepoints (prophet)')
            plt.xlabel('Date')
            plt.ylabel('Value')
            plt.tight_layout()
            if save_figure == True:
                plt.savefig(output_path + title + ' (changepoints - model ' + str(index) + ').png', dpi=300)
            if show_plot == True:
                if platform == 'win32':
                    plt.get_current_fig_manager().window.state('zoomed')
                elif platform == 'linux' or platform == 'linux2':
                    mng = plt.get_current_fig_manager()
                    mng.resize(*mng.window.maxsize())
                plt.show()
            plt.close()

    def plot_components(self, data, output_path, title, save_figure=True, show_plot=False):
        """ Plot the prediction output components """
        models = []
        if self.__is_incremental == False:
            models.append(self.__model)
        else:
            models = self.__models
        for index, model in enumerate(models):
            model.plot_components(self.predict(data, all_outputs=True))
            plt.tight_layout()
            if save_figure == True:
                plt.savefig(output_path + title + ' (seasonality importance - model ' + str(index) + ').png', dpi=300)
            if show_plot == True:
                if platform == 'win32':
                    plt.get_current_fig_manager().window.state('zoomed')
                elif platform == 'linux' or platform == 'linux2':
                    mng = plt.get_current_fig_manager()
                    mng.resize(*mng.window.maxsize())
                plt.show()
            plt.close()