import copy
import pandas as pd
from datetime import datetime
from kpi_forecast.utils.db_parser import MicrosoftSqlParser

class DataParser(object):
    """ A module that parses and combines the data from different sources """
    __sql_parser = None
    __parameters = None
    __defaults = {
        'table': {
            'singapore':  {
                'new_sales_data': 'CKS.dbo.DailySalesFact',
                'old_sales_data': 'History_DW.dbo.DailySalesFact',
                'pnl_data': 'CKS.dbo.OwnBusinessProfitLossFact',
                'item_data': 'CKS.dbo.Item_Master',
                'material_data': 'CKS.dbo.MaterialGroupText',
                'shop_data': 'CKS.dbo.ShopMaster',
                'traffic_data': 'CKS.dbo.pplcount_day'
            }
        },
        'table_columns': {
            'pnl_data': [
                'COGS',
                'StaffPayrollCPF',
                'OutletExpenditure',
                'PUB',
                'Depreciation',
                'OfficeFixedCost',
                'Rental',
                'TotalCost',
                'TotalOperationCost'
            ],
            'sales_data': [
                'SoldQty',
                'SoldQtyExReturns',
                'TotalGrossPrice',
                'TotalLandedCost',
                'TotalNetPrice',
                'TransactionsExReturns'
            ],
            'traffic_data': [
                'Traffic'
            ]
        },
        'database': {
            'name': 'production'
        }
    }

    def __init__(self, parameters=None):
        """ Constructor """
        self.__parameters = copy.deepcopy(self.__defaults)
        # Set defined parameter values if present
        if parameters is not None:
            self.__parameters.update(parameters)
        self.__sql_parser = MicrosoftSqlParser()

    def aggregate_day_to_week(self, data, aggregate_columns):
        """ Aggregate daily data on weekly basis """
        weekly_data = data.copy()
        # Extract additional time info from the existing data
        weekly_data['Year'] = weekly_data['Date'].dt.year
        weekly_data['WeekOfYear'] = weekly_data['Date'].dt.week
        # Aggregate the data
        group_columns = ['ShopNo', 'Year', 'WeekOfYear']
        weekly_data = weekly_data.groupby(group_columns)[aggregate_columns].sum().reset_index().sort_values(group_columns)
        weekly_data['Date'] = pd.to_datetime([
            datetime.strptime((str(year) + '-W' + str(week) + '-1'), '%Y-W%W-%w') for year, week in zip(weekly_data['Year'], weekly_data['WeekOfYear'])
        ])
        return weekly_data

    def aggregate_day_to_month(self, data, aggregate_columns):
        """ Aggregate daily data on monthly basis """
        monthly_data = data.copy()
        # Extract additional time info from the existing data
        monthly_data['Year'] = monthly_data['Date'].dt.year
        monthly_data['MonthOfYear'] = monthly_data['Date'].dt.month
        # Aggregate the data
        group_columns = ['ShopNo', 'Year', 'MonthOfYear']
        monthly_data = monthly_data.groupby(group_columns)[aggregate_columns].sum().reset_index().sort_values(group_columns)
        monthly_data['Date'] = pd.to_datetime([
            datetime.strptime((str(year) + '-' + str(month) + '-01'), '%Y-%m-%d') for year, month in zip(monthly_data['Year'], monthly_data['MonthOfYear'])
        ])
        return monthly_data

    def aggregate_day_to_year(self, data, aggregate_columns):
        """ Aggregate daily data on yearly basis """
        yearly_data = data.copy()
        # Extract additional time info from the existing data
        yearly_data['Year'] = yearly_data['Date'].dt.year
        # Aggregate the data
        group_columns = ['ShopNo', 'Year']
        yearly_data = yearly_data.groupby(group_columns)[aggregate_columns].sum().reset_index().sort_values(group_columns)
        yearly_data['Date'] = pd.to_datetime([
            datetime.strptime((str(year) + '-01-01'), '%Y-%m-%d') for year in yearly_data['Year']
        ])
        return yearly_data

    def parse_pnl_data(self, pnl_data, operation_data, time_units='day'):
        """ Parse the PnL data in Pandas DataFrame format """
        print('Processing the PnL data')
        output_data = {}

        # Define nested function that aggregates PnL data on daily basis
        def parse_pnl_data_daily(pnl_data, operation_data):
            pnl_data_daily = pnl_data.copy()
            # Extract additional time info from the existing data
            operation_data['Date'] = pd.to_datetime(operation_data['Date'])
            operation_data['Year'] = operation_data['Date'].dt.year
            operation_data['MonthOfYear'] = operation_data['Date'].dt.month
            # Determine total operating days per month for each store
            days_per_month = []
            filtered_operation_data = pd.DataFrame()
            for i in range(pnl_data_daily.shape[0]):
                matched_data = operation_data[
                    (operation_data['ShopNo'] == pnl_data_daily['ShopNo'][i]) &
                    (operation_data['Year'] == pnl_data_daily['Year'][i]) &
                    (operation_data['MonthOfYear'] == pnl_data_daily['MonthOfYear'][i])]
                filtered_operation_data = filtered_operation_data.append(matched_data)
                days_per_month.append(matched_data.shape[0])
            pnl_data_daily['DaysPerMonth'] = days_per_month
            # Normalize the data to per day unit
            for column in self.__parameters['table_columns']['pnl_data']:
                pnl_data_daily[column] = pnl_data_daily[column] / pnl_data_daily['DaysPerMonth']
            # Sort the values
            sort_columns = ['ShopNo', 'Year', 'MonthOfYear']
            pnl_data_daily = pnl_data_daily.sort_values(sort_columns)
            sort_columns = ['ShopNo', 'Date']
            filtered_operation_data = filtered_operation_data.sort_values(sort_columns)
            # Parse the monthly data to daily data
            pnl_data_daily = pnl_data_daily.loc[pnl_data_daily.index.repeat(pnl_data_daily['DaysPerMonth'])]
            pnl_data_daily['Date'] = filtered_operation_data['Date'].values
            # Remove redundant columns
            drop_columns = ['Year', 'MonthOfYear', 'DaysPerMonth']
            return pnl_data_daily.drop(drop_columns, axis=1)

        # Aggregate PnL data on daily basis
        if any('day' in time.lower() for time in time_units):
            print('Aggregating the PnL data on daily basis')
            pnl_data_daily = parse_pnl_data_daily(pnl_data, operation_data)
            output_data['day'] = pnl_data_daily
        # Aggregate PnL data on weekly basis
        if any('week' in time.lower() for time in time_units):
            print('Aggregating the PnL data on weekly basis')
            # Get daily PnL data
            daily_data = pnl_data_daily if not pnl_data_daily.empty else parse_pnl_data_daily(pnl_data, operation_data)
            output_data['week'] = self.aggregate_day_to_week(daily_data, self.__parameters['table_columns']['pnl_data'])
        # Aggregate PnL data on monthly basis
        if any('month' in time.lower() for time in time_units):
            print('Aggregating the PnL data on monthly basis')
            # Aggregate PnL data
            sort_columns = ['ShopNo', 'Year', 'MonthOfYear']
            monthly_data = pnl_data.copy().sort_values(sort_columns)
            monthly_data['Date'] = pd.to_datetime([
                datetime.strptime((str(year) + '-' + str(month) + '-01'), '%Y-%m-%d') for year, month in zip(monthly_data['Year'], monthly_data['MonthOfYear'])
            ])
            output_data['month'] = monthly_data
        # Aggregate PnL data on yearly basis
        if any('year' in time.lower() for time in time_units):
            print('Aggregating the PnL data on yearly basis')
            # Aggregate PnL data
            group_columns = ['ShopNo', 'Year']
            aggregate_columns = {}
            for column in self.__parameters['table_columns']['pnl_data']:
                aggregate_columns[column] = 'sum'
            yearly_data = pnl_data.groupby(group_columns).agg(aggregate_columns).reset_index().sort_values(group_columns)
            yearly_data['Date'] = pd.to_datetime([
                datetime.strptime((str(year) + '-01-01'), '%Y-%m-%d') for year in yearly_data['Year']
            ])
            output_data['year'] = yearly_data
        return output_data

    def parse_sales_data(self, sales_data, time_units='day'):
        """ Parse the sales data in Pandas DataFrame format """
        print('Processing the sales data')
        output_data = {}
        sales_data['Date'] = pd.to_datetime(sales_data['Date'])
        # Aggregate sales data on daily basis
        if any('day' in time.lower() for time in time_units):
            print('Aggregating the sales data on daily basis')
            output_data['day'] = sales_data.sort_values(['ShopNo', 'Date'])
        # Aggregate sales data on weekly basis
        if any('week' in time.lower() for time in time_units):
            print('Aggregating the sales data on weekly basis')
            output_data['week'] = self.aggregate_day_to_week(sales_data, self.__parameters['table_columns']['sales_data'])
        # Aggregate sales data on monthly basis
        if any('month' in time.lower() for time in time_units):
            print('Aggregating the sales data on monthly basis')
            output_data['month'] = self.aggregate_day_to_month(sales_data, self.__parameters['table_columns']['sales_data'])
        # Aggregate sales data on yearly basis
        if any('year' in time.lower() for time in time_units):
            print('Aggregate the sales data on yearly basis')
            output_data['year'] = self.aggregate_day_to_year(sales_data, self.__parameters['table_columns']['sales_data'])
        return output_data

    def parse_traffic_data(self, traffic_data, time_units='day'):
        """ Parse the traffic data in Pandas DataFrame format """
        print('Processing the traffic data')
        output_data = {}
        traffic_data['Date'] = pd.to_datetime(traffic_data['Date'])
        # Aggregate traffic data on daily basis
        if any('day' in time.lower() for time in time_units):
            print('Aggregating the traffic data on daily basis')
            output_data['day'] = traffic_data.sort_values(['ShopNo', 'Date'])
        # Aggregate traffic data on weekly basis
        if any('week' in time.lower() for time in time_units):
            print('Aggregating the traffic data on weekly basis')
            output_data['week'] = self.aggregate_day_to_week(traffic_data, self.__parameters['table_columns']['traffic_data'])
        # Aggregate traffic data on monthly basis
        if any('month' in time.lower() for time in time_units):
            print('Aggregating the traffic data on monthly basis')
            output_data['month'] = self.aggregate_day_to_month(traffic_data, self.__parameters['table_columns']['traffic_data'])
        # Aggregate traffic data on yearly basis
        if any('year' in time.lower() for time in time_units):
            print('Aggregate the traffic data on yearly basis')
            output_data['year'] = self.aggregate_day_to_year(traffic_data, self.__parameters['table_columns']['traffic_data'])
        return output_data

    def query_operation_data(self, start_date, end_date, country):
        """ Perform SQL query of the operation data """
        print('Performing SQL query of the operation data ...')
        start_date = datetime.strptime(start_date, '%Y-%m-%d')
        end_date = datetime.strptime(end_date, '%Y-%m-%d')
        cols =  ' sales.ShopNo as ShopNo, ' + \
                ' sales.Date as Date '
        text =  ' inner join ' + self.__parameters['table'][country.lower()]['shop_data'] + ' shops on sales.ShopNo=shops.ShopNo ' + \
                ' inner join ( ' + \
	            ' select items.barcode, items.Plant, materials.Material_Group_Description2, items.brandID from ' + self.__parameters['table'][country.lower()]['item_data'] + ' items ' + \
	            ' inner join ' + self.__parameters['table'][country.lower()]['material_data'] + \
                ' materials on items.Material_Group_ID = materials.Material_Group_ID ' + \
	            ' where items.Plant = 1000 ' + \
		        " and (items.ArticleNo not in ('CK-HANDBAGS','CK-MKTTOTEBAG','CK-BAGPATCHES','CK6-40680378','CK8-40680378', 'CK-MISCINCOME', 'CK_LENS_WIPES') " + \
		        " and materials.Material_Group_ID not in ('09','10','1002','1003','35','88','91','92','93','94','95','96','97','98','99'))) "+ \
                ' item on item.barcode = sales.Barcode and item.Plant = sales.Plant ' + \
                " where sales.Plant = 1000 and item.brandID ='CK' " + \
                ' and ((Year(Date) > ' + str(start_date.year) + ' and Year(Date) < ' + str(end_date.year) + ' ) ' + \
                ' or (Year(Date) = ' + str(start_date.year) + ' and Month(Date) >= ' + str(start_date.month) + ' ) ' + \
                ' or (Year(Date) = ' + str(end_date.year) + ' and Month(Date) <= ' + str(end_date.month) + ' )) ' + \
	            " and shops.StoreCategory not in ('ONLINE','WAREHOUSE','OTHERS') " + \
                ' order by sales.ShopNo, sales.Date '
        # Define SQL query for old CKS operation data
        query = ' select distinct ' + cols + ' from ' + self.__parameters['table'][country.lower()]['old_sales_data'] + ' sales ' + text
        # Perform old CKS operation data query from SQL database
        old_operation_data = self.__sql_parser.execute_once(query, self.__parameters['database']['name'])
        # Define SQL query for new CKS operation data
        query = ' select distinct ' + cols + ' from ' + self.__parameters['table'][country.lower()]['new_sales_data'] + ' sales ' + text
        # Perform new CKS operation data query from SQL database
        new_operation_data = self.__sql_parser.execute_once(query, self.__parameters['database']['name'])
        # Concatenate CKS operation data and filter duplicated values
        operation_data = pd.concat([old_operation_data, new_operation_data])
        operation_data.drop_duplicates(subset=['ShopNo', 'Date'], keep='last', inplace=True)
        return operation_data

    def query_pnl_data(self, start_date, end_date, country):
        """ Perform SQL query of the PnL data """
        print('Performing SQL query of the PnL data ...')
        start_date = datetime.strptime(start_date, '%Y-%m-%d')
        end_date = datetime.strptime(end_date, '%Y-%m-%d')
        # Define SQL query for CKS PnL data
        cols =  ' ShopNo, Year, Month as MonthOfYear, COGS, StaffPayrollCPF, OutletExpenditure,' + \
                ' PUB, Depreciation, OfficeFixedCost, Rental, TotalCost, TotalOperationCost '
        query = ' select ' + cols + ' from ' + self.__parameters['table'][country.lower()]['pnl_data'] + \
                " where BusinessEntity = 'CK' and Country = 'SINGAPORE' " + \
                ' and ((Year > ' + str(start_date.year) + ' and Year < ' + str(end_date.year) + ' ) ' + \
                ' or (Year = ' + str(start_date.year) + ' and Month >= ' + str(start_date.month) + ' ) ' + \
                ' or (Year = ' + str(end_date.year) + ' and Month <= ' + str(end_date.month) + ' )) '
        # Perform PnL data query from SQL database
        return self.__sql_parser.execute_once(query, self.__parameters['database']['name'])

    def query_sales_data(self, start_date, end_date, country):
        """ Perform SQL query of the sales data """
        print('Performing SQL query of the sales data ...')
        start_date = datetime.strptime(start_date, '%Y-%m-%d')
        end_date = datetime.strptime(end_date, '%Y-%m-%d')
        cols =  ' sales.ShopNo as ShopNo, ' + \
                ' sales.Date as Date,' + \
                " sum((case when sales.articleno like '%ROUNDING%' or sales.articleno like '%INCOME%' then 0 else sales.SoldQty end)) as SoldQty, " + \
	            " sum(case when sales.articleno not like '%ROUNDING%' and sales.articleno not like '%INCOME%' and sales.soldQty > 0 then sales.soldQty else 0 end) as SoldQtyExReturns, " + \
        	    ' sum(TotalGrossPriceSGD) as TotalGrossPrice, ' + \
	            ' sum(TotalLandedCostSGD) as TotalLandedCost, ' + \
                ' sum(TotalNetPriceSGD) as TotalNetPrice, ' + \
	            ' count(distinct case when sales.soldQty > 0 then transaction_id end) as TransactionsExReturns '
        text =  ' inner join ' + self.__parameters['table'][country.lower()]['shop_data'] + ' shops on sales.ShopNo=shops.ShopNo ' + \
                ' inner join ( ' + \
	            ' select items.barcode, items.Plant, materials.Material_Group_Description2, items.brandID from ' + self.__parameters['table'][country.lower()]['item_data'] + ' items ' + \
	            ' inner join ' + self.__parameters['table'][country.lower()]['material_data'] + ' materials on items.Material_Group_ID = materials.Material_Group_ID ' + \
	            ' where items.Plant = 1000 ' + \
		        " and (items.ArticleNo not in ('CK-HANDBAGS','CK-MKTTOTEBAG','CK-BAGPATCHES','CK6-40680378','CK8-40680378', 'CK-MISCINCOME', 'CK_LENS_WIPES') " + \
		        " and materials.Material_Group_ID not in ('09','10','1002','1003','35','88','91','92','93','94','95','96','97','98','99'))) " + \
                ' item on item.barcode = sales.Barcode and item.Plant = sales.Plant ' + \
                " where sales.Plant = 1000 and item.brandID ='CK' " + \
                ' and ((Year(Date) > ' + str(start_date.year) + ' and Year(Date) < ' + str(end_date.year) + ' ) ' + \
                ' or (Year(Date) = ' + str(start_date.year) + ' and Month(Date) > ' + str(start_date.month) + ' ) ' + \
                ' or (Year(Date) = ' + str(start_date.year) + ' and Month(Date) = ' + str(start_date.month) + ' and Day(Date) >= ' + str(start_date.day) + ' ) ' + \
                ' or (Year(Date) = ' + str(end_date.year) + ' and Month(Date) < ' + str(end_date.month) + ' ) ' + \
                ' or (Year(Date) = ' + str(end_date.year) + ' and Month(Date) = ' + str(end_date.month) + ' and Day(Date) <= ' + str(end_date.day) + ' )) ' + \
	            " and shops.StoreCategory not in ('ONLINE','WAREHOUSE','OTHERS') " + \
                ' group by sales.ShopNo, sales.Date ' + \
                ' order by sales.ShopNo, sales.Date '
        # Define SQL query for old CKS sales data
        query = ' select distinct ' + cols + ' from ' + self.__parameters['table'][country.lower()]['old_sales_data'] + ' sales ' + text
        # Perform old CKS sales data query from SQL database
        old_sales_data = self.__sql_parser.execute_once(query, self.__parameters['database']['name'])
        # Define SQL query for new CKS sales data
        query = ' select distinct ' + cols + ' from ' + self.__parameters['table'][country.lower()]['new_sales_data'] + ' sales ' + text
        # Perform new CKS sales data query from SQL database
        new_sales_data = self.__sql_parser.execute_once(query, self.__parameters['database']['name'])
        # Concatenate CKS sales data and filter duplicated values
        sales_data = pd.concat([old_sales_data, new_sales_data])
        sales_data.drop_duplicates(subset=['ShopNo', 'Date'], keep='last', inplace=True)
        return sales_data

    def query_traffic_data(self, start_date, end_date, country):
        """ Perform SQL query of the traffic data """
        print('Performing SQL query of the traffic data ...')
        start_date = datetime.strptime(start_date, '%Y-%m-%d')
        end_date = datetime.strptime(end_date, '%Y-%m-%d')
        # Define SQL query for CKS traffic data
        query = ' select traffics.ShopNo, Date, Incoming as Traffic from ' + self.__parameters['table'][country.lower()]['traffic_data'] + ' traffics ' + \
                ' inner join ' + self.__parameters['table'][country.lower()]['shop_data'] + ' shops on shops.ShopNo = traffics.ShopNo ' + \
                " where shops.Plant = 1000 and shops.brandid = 'CK' " + \
                ' and ((Year(Date) > ' + str(start_date.year) + ' and Year(Date) < ' + str(end_date.year) + ' ) ' + \
                ' or (Year(Date) = ' + str(start_date.year) + ' and Month(Date) > ' + str(start_date.month) + ' ) ' + \
                ' or (Year(Date) = ' + str(start_date.year) + ' and Month(Date) = ' + str(start_date.month) + \
                ' and Day(Date) >= ' + str(start_date.day) + ' ) ' + \
                ' or (Year(Date) = ' + str(end_date.year) + ' and Month(Date) < ' + str(end_date.month) + ' ) ' + \
                ' or (Year(Date) = ' + str(end_date.year) + ' and Month(Date) = ' + str(end_date.month) + \
                ' and Day(Date) <= ' + str(end_date.day) + ' )) ' + \
                ' order by ShopNo, Date '
        # Perform PnL data query from SQL database
        return self.__sql_parser.execute_once(query, self.__parameters['database']['name'])

    def get_pnl_data(self, start_date, end_date, country, time_units='month'):
        """ Get the PnL data from SQL database """
        # Perform SQL query of the PnL data
        pnl_data = self.query_pnl_data(start_date, end_date, country)
        # Perform SQL query of the operation data
        operation_data = self.query_operation_data(start_date, end_date, country)
        operation_data = operation_data[operation_data['ShopNo'].isin(pnl_data['ShopNo'].unique())]
        # Parse the PnL data based on the specified time unit(s)
        return self.parse_pnl_data(
            pnl_data=pnl_data,
            operation_data=operation_data,
            time_units=time_units
        )

    def get_sales_data(self, start_date, end_date, country, time_units='month'):
        """ Get the sales data from SQL database """
        # Perform SQL query of the sales data
        sales_data = self.query_sales_data(start_date, end_date, country)
        # Parse the sales data based on the specified time unit(s)
        return self.parse_sales_data(
            sales_data=sales_data,
            time_units=time_units
        )

    def get_traffic_data(self, start_date, end_date, country, time_units='month'):
        """ Get the traffic data from SQl database """
        # Perform SQL query of the traffic data
        traffic_data = self.query_traffic_data(start_date, end_date, country)
        # Parse the traffic data based on the specified time unit(s)
        return self.parse_traffic_data(
            traffic_data=traffic_data,
            time_units=time_units
        )

    def get_data(self, start_date, end_date, country, time_units='month'):
        """ Get the data from SQL database """
        # Get and parse PnL, sales and traffic data from SQL database
        pnl_data = self.get_pnl_data(start_date, end_date, country, time_units)
        sales_data = self.get_sales_data(start_date, end_date, country, time_units)
        traffic_data = self.get_traffic_data(start_date, end_date, country, time_units)
        output_data = {}
        # Merge daily data
        if any('day' in time.lower() for time in time_units):
            print('Merging the daily data')
            merge_columns = ['ShopNo', 'Date']
            temp = pd.merge(
                pnl_data['day'], 
                sales_data['day'], 
                how='left',
                on=merge_columns
            )
            output_data['day'] = pd.merge(
                temp,
                traffic_data['day'], 
                how='left',
                on=merge_columns
            )
        # Merge weekly data
        if any('week' in time.lower() for time in time_units):
            print('Merging the weekly data')
            merge_columns = ['ShopNo', 'Year', 'WeekOfYear', 'Date']
            temp = pd.merge(
                pnl_data['week'], 
                sales_data['week'], 
                how='left',
                on=merge_columns
            )
            output_data['week'] = pd.merge(
                temp,
                traffic_data['week'], 
                how='left',
                on=merge_columns
            )
        # Merge monthly data
        if any('month' in time.lower() for time in time_units):
            print('Merging the monthly data')
            merge_columns = ['ShopNo', 'Year', 'MonthOfYear', 'Date']
            temp = pd.merge(
                pnl_data['month'], 
                sales_data['month'], 
                how='left',
                on=merge_columns
            )
            output_data['month'] = pd.merge(
                temp,
                traffic_data['month'], 
                how='left',
                on=merge_columns
            )
        # Merge yearly data
        if any('year' in time.lower() for time in time_units):
            print('Merging the yearly data')
            merge_columns = ['ShopNo', 'Year', 'Date']
            temp = pd.merge(
                pnl_data['year'], 
                sales_data['year'], 
                how='left',
                on=merge_columns
            )
            output_data['year'] = pd.merge(
                temp,
                traffic_data['year'], 
                how='left',
                on=merge_columns
            )
        return output_data
