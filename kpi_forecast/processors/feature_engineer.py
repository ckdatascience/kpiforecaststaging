import pandas as pd
from datetime import datetime
from datetime import date
from datetime import timedelta
from kpi_forecast.utils.holiday_parser import HolidayParser
from kpi_forecast.utils.rolled_shifted_features_creator import RolledShiftedFeaturesDataCreator
from kpi_forecast.utils.groups_creator import GroupsCreator
from kpi_forecast.utils.date_date_creator import DateDataCreator
from dateutil.relativedelta import relativedelta

class FeatureEngineer(object):
    """ A module that performs feature engineering on the data """
    __date_data_creator = None
    __holiday_parser = None
    __groups_creator = None

    def __init__(self):
        """ Constructor """
        self.__date_data_creator = DateDataCreator()
        self.__holiday_parser = HolidayParser()
        self.__groups_creator = GroupsCreator()

    def filter_traffic_data(self, data):
        """ Filter the traffic values in the data """
        print('Filtering the traffic values in the data')
        # Filter out zero traffic values and those that are less than transaction count
        filter = (data['Traffic'] >= data['Transactions']) & (data['Traffic'] > 0)
        return data[filter], data[~filter]

    def extract_date_and_holidays_features(self, data, country, feature, state=None, time_unit='day'):
        """ Extract features from the dates of each data object """
        print('Extracting date and holidays features')
        if time_unit.lower() == 'day':
            data = self.extract_date_features(data)
            data = self.extract_holiday_features(data, country, state=state)
        elif time_unit.lower() == 'month':
            end_date = max(data.Date)
            calendar_data = self.__date_data_creator.create_calendar_full_data(start='2013-01-01', end=end_date)
            data['Date'] = pd.to_datetime(data['Date'])
            data = self.__date_data_creator.aggregate_day_to_month(data, aggregate_columns=[feature])
            data['Date'] = pd.to_datetime([datetime(year, month, 1).strftime('%Y-%m-%d') for year, month in zip(data['Year'], data['Month'])])
            index = data['Date'] < end_date
            data = data[index]
            data = data.merge(calendar_data, on=['Year', 'Month'], how='left')
            data = data.fillna(0)
            data = self.__date_data_creator.create_workdays_proportion(data)
        return data

    def extract_date_features(self, data):
        """ Extract features from the dates of each data object """
        print('Extracting date features')
        data['Date'] = pd.to_datetime(data['Date'])
        data['Year'] = data['Date'].dt.year
        data['MonthOfYear'] = data['Date'].dt.month
        data['DayOfMonth'] = data['Date'].dt.day
        data['WeekOfYear'] = data['Date'].dt.weekofyear
        data['DayOfWeek'] = data['Date'].dt.dayofweek
        data['DayOfYear'] = data['Date'].dt.dayofyear
        data['QuarterOfYear'] = data['Date'].dt.quarter
        return data

    def extract_holiday_features(self, data, country, state=None):
        """ Extract holiday features from the dates of each data object """
        print('Extracting holiday features')
        start_date = data['Date'].min() - pd.offsets.DateOffset(years=1)
        end_date = data['Date'].max() + pd.offsets.DateOffset(years=1)
        holidays = self.__holiday_parser.get_holidays(start_date, end_date, country, state)
        new_year_holidays = holidays[holidays['Holiday'] == 'New year']
        cny_holidays = holidays[holidays['Holiday'] == 'Chinese Lunar New Year\'s Day']
        christmas_holidays = holidays[holidays['Holiday'] == 'Christmas Day']

        # Create new DataFrame
        rng = pd.date_range(data['Date'].min(), data['Date'].max(), freq='D')
        daterange_frame = pd.DataFrame({'Date': rng})
        # Generate days before the nearest holiday
        print('Generating nearest holiday features')
        daterange_frame['DaysBeforeHoliday'] = daterange_frame['Date'].apply(
            lambda x: (holidays[(holidays['Date'] >= x)]['Date'].iloc[0] - x).days
        )
        # Generate days after the nearest holiday
        daterange_frame['DaysAfterHoliday'] = daterange_frame['Date'].apply(
            lambda x: (x - holidays[(holidays['Date'] <= x)]['Date'].iloc[-1]).days
        )
        print('Generating New Year holiday features')
        # Generate days before New Year holiday
        daterange_frame['DaysBeforeNewYearHoliday'] = daterange_frame['Date'].apply(
            lambda x: (new_year_holidays[(new_year_holidays['Date'] >= x)]['Date'].iloc[0] - x).days
        )
        # Generate days after New Year holiday
        daterange_frame['DaysAfterNewYearHoliday'] = daterange_frame['Date'].apply(
            lambda x: (x - new_year_holidays[(new_year_holidays['Date'] <= x)]['Date'].iloc[-1]).days
        )
        print('Generating Chinese New Year holiday features')
        # Generate days before Chinese New Year holiday
        daterange_frame['DaysBeforeCnyHoliday'] = daterange_frame['Date'].apply(
            lambda x: (cny_holidays[(cny_holidays['Date'] >= x)]['Date'].iloc[0] - x).days
        )
        # Generate days after Chinese New Year holiday
        daterange_frame['DaysAfterCnyHoliday'] = daterange_frame['Date'].apply(
            lambda x: (x - cny_holidays[(cny_holidays['Date'] <= x)]['Date'].iloc[-1]).days
        )
        print('Generating Christmas holiday features')
        # Generate days before Christmas holiday
        daterange_frame['DaysBeforeChristmasHoliday'] = daterange_frame['Date'].apply(
            lambda x: (christmas_holidays[(christmas_holidays['Date'] >= x)]['Date'].iloc[0] - x).days
        )
        # Generate days after Christmas holiday
        daterange_frame['DaysAfterChristmasHoliday'] = daterange_frame['Date'].apply(
            lambda x: (x - christmas_holidays[(christmas_holidays['Date'] <= x)]['Date'].iloc[-1]).days
        )
        return data.merge(daterange_frame, on='Date', how='left')

    def extract_group_features(self, data, time_unit):
        """ Extract shop group features """
        print('Extracting group features')
        return self.__groups_creator.create_groupping_variables(data, time_unit)

    def extract_historical_features(self, data, feature, time_unit):
        """ Extract historical features """
        print('Extracting rolling shifted features')
        historical_feature_creator = RolledShiftedFeaturesDataCreator(feature, time_unit)
        data = historical_feature_creator.create_and_return_features(data)
        return data

    def perform_feature_engineering(self, data, feature, country, state=None, time_unit='day'):
        """ Perform feature engineering """
        data = self.extract_date_and_holidays_features(data, country, feature, state, time_unit=time_unit)
        data = self.extract_group_features(data, time_unit=time_unit)
        return self.extract_historical_features(data, feature, time_unit=time_unit)
