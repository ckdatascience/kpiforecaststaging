import numpy as np
import pandas as pd

def calc_average_selling_price(data, identifier=''):
    """ Calculate average selling price from the data """
    if all(name in data.columns for name in ['TotalNetPrice' + identifier, 'SoldQty' + identifier]):
        data['AverageSellingPrice' + identifier] = data['TotalNetPrice' + identifier] / data['SoldQty' + identifier]
    return data

def calc_average_transaction_value(data, identifier=''):
    """ Calculate average transaction value from the data """
    if all(name in data.columns for name in ['TotalNetPrice' + identifier, 'TransactionsExReturns' + identifier]):
        data['AverageTransactionValue' + identifier] = data['TotalNetPrice' + identifier] / data['TransactionsExReturns' + identifier]
    return data

def calc_cogs_margin(data, identifier=''):
    """ Calculate COGS margin from the data """
    if all(name in data.columns for name in ['COGS' + identifier, 'TotalNetPrice' + identifier]):
        data['COGS%' + identifier] = data['COGS' + identifier] * 100 / data['TotalNetPrice' + identifier]
    return data

def calc_conversion_rate(data, identifier=''):
    """ Calculate conversion rate from the data """
    if all(name in data.columns for name in ['TransactionsExReturns' + identifier, 'Traffic' + identifier]):
        data['ConversionRate' + identifier] = data['TransactionsExReturns' + identifier] * 100 / data['Traffic' + identifier]
    return data

def calc_depreciation_margin(data, identifier=''):
    """ Calculate depreciation margin from the data """
    if all(name in data.columns for name in ['Depreciation' + identifier, 'TotalNetPrice' + identifier]):
        data['Depreciation%' + identifier] = data['Depreciation' + identifier] * 100 / data['TotalNetPrice' + identifier]
    return data

def calc_gross_profit(data, identifier=''):
    """ Calculate gross profit (GP) from the data """
    if all(name in data.columns for name in ['TotalNetPrice' + identifier, 'TotalLandedCost' + identifier]):
        data['GrossProfit' + identifier] = data['TotalNetPrice' + identifier] - data['TotalLandedCost' + identifier]
    return data

def calc_gross_profit_margin(data, identifier=''):
    """ Calculate gross profit margin (GP%) from the data """
    if all(name in data.columns for name in ['GrossProfit' + identifier, 'TotalNetPrice' + identifier]):
        data['GrossProfit%' + identifier] = data['GrossProfit' + identifier] * 100 / data['TotalNetPrice' + identifier]
    return data

def calc_office_fixed_cost_margin(data, identifier=''):
    """ Calculate office fixed cost margin from the data """
    if all(name in data.columns for name in ['OfficeFixedCost' + identifier, 'TotalNetPrice' + identifier]):
        data['OfficeFixedCost%' + identifier] = data['OfficeFixedCost' + identifier] * 100 / data['TotalNetPrice' + identifier]
    return data

def calc_outlet_expenditure_margin(data, identifier=''):
    """ Calculate outlet expenditure margin from the data """
    if all(name in data.columns for name in ['OutletExpenditure' + identifier, 'TotalNetPrice' + identifier]):
        data['OutletExpenditure%' + identifier] = data['OutletExpenditure' + identifier] * 100 / data['TotalNetPrice' + identifier]
    return data

def calc_net_profit(data, identifier=''):
    """ Calculate net profit from the data """
    if all(name in data.columns for name in ['TotalNetPrice' + identifier, 'TotalCost' + identifier]):
        data['NetProfit' + identifier] = data['TotalNetPrice' + identifier] - data['TotalCost' + identifier]
    return data

def calc_net_profit_margin(data, identifier=''):
    """ Calculate net profit margin from the data """
    if all(name in data.columns for name in ['NetProfit' + identifier, 'TotalNetPrice' + identifier]):
        data['NetProfit%' + identifier] = data['NetProfit' + identifier] * 100 / data['TotalNetPrice' + identifier]
    return data

def calc_operation_index(data, identifier=''):
    """ Calculate operation index from the data """
    if all(name in data.columns for name in ['ConversionRate' + identifier, 'QuantityPerReceipt' + identifier]):
        data['OperationIndex' + identifier] = data['ConversionRate' + identifier] / data['QuantityPerReceipt' + identifier]
    return data

def calc_original_retail_price(data, identifier=''):
    """ Calculate original retail price from the data """
    if all(name in data.columns for name in ['TotalGrossPrice' + identifier, 'SoldQty' + identifier]):
        data['OriginalRetailPrice' + identifier] = data['TotalGrossPrice' + identifier] / data['SoldQty' + identifier]
    return data

def calc_profit_dilution(data, identifier=''):
    """ Calculate profit dilution from the data """
    if all(name in data.columns for name in ['TotalGrossPrice' + identifier, 'TotalNetPrice' + identifier]):
        data['ProfitDilution' + identifier] = (data['TotalGrossPrice' + identifier] - data['TotalNetPrice' + identifier]) * 100 / data['TotalGrossPrice' + identifier]
    return data

def calc_pub_margin(data, identifier=''):
    """ Calculate PUB margin from the data """
    if all(name in data.columns for name in ['PUB' + identifier, 'TotalNetPrice' + identifier]):
        data['PUB%' + identifier] = data['PUB' + identifier] * 100 / data['TotalNetPrice' + identifier]
    return data

def calc_quantity_per_receipt(data, identifier=''):
    """ Calculate quantity per receipt from the data """
    if all(name in data.columns for name in ['SoldQtyExReturns' + identifier, 'TransactionsExReturns' + identifier]):
        data['QuantityPerReceipt' + identifier] = data['SoldQtyExReturns' + identifier] / data['TransactionsExReturns' + identifier]
    return data

def calc_rate_of_sales(data, identifier=''):
    """ Calculate rate of sales from the data """
    if all(name in data.columns for name in ['SoldQty' + identifier, 'TotalOperationDays' + identifier]):
        data['RateOfSales' + identifier] = data['SoldQty' + identifier] / data['TotalOperationDays' + identifier]
    return data

def calc_rental_margin(data, identifier=''):
    """ Calculate rental margin from the data """
    if all(name in data.columns for name in ['Rental' + identifier, 'TotalNetPrice' + identifier]):
        data['Rental%' + identifier] = data['Rental' + identifier] * 100 / data['TotalNetPrice' + identifier]
    return data

def calc_staff_payroll_cpf_margin(data, identifier=''):
    """ Calculate staff payroll CPF margin from the data """
    if all(name in data.columns for name in ['StaffPayrollCPF' + identifier, 'TotalNetPrice' + identifier]):
        data['StaffPayrollCPF%' + identifier] = data['StaffPayrollCPF' + identifier] * 100 / data['TotalNetPrice' + identifier]
    return data

def calc_total_cost(data, identifier=''):
    """ Calculate total cost from the data """
    if all(name in data.columns for name in ['COGS' + identifier, 'TotalOperatingCost' + identifier]):
        data['TotalCost' + identifier] = data['COGS' + identifier] + data['TotalOperationCost' + identifier]
    return data

def calc_total_cost_margin(data, identifier=''):
    """ Calculate total cost margin from the data """
    if all(name in data.columns for name in ['TotalCost' + identifier, 'TotalNetPrice' + identifier]):
        data['TotalCost%' + identifier] = data['TotalCost' + identifier] * 100 / data['TotalNetPrice' + identifier]
    return data

def calc_total_operation_cost(data, identifier=''):
    """ Calculate total operation cost from the data """
    columns = ['Depreciation' + identifier, 'OutletExpenditure' + identifier, 'PUB' + identifier, 'Rental' + identifier, 'StaffPayrollCPF' + identifier]
    if all(name in data.columns for name in columns):
        data['TotalOperationCost' + identifier] = data['Depreciation' + identifier] + \
                                                  data['OutletExpenditure' + identifier] + \
                                                  data['PUB' + identifier] + \
                                                  data['Rental' + identifier] + \
                                                  data['StaffPayrollCPF' + identifier]
    return data

def calc_total_operation_cost_margin(data, identifier=''):
    """ Calculate total operation cost margin from the data """
    if all(name in data.columns for name in ['TotalOperationCost' + identifier, 'TotalNetPrice' + identifier]):
        data['TotalOperationCost%' + identifier] = data['TotalOperationCost' + identifier] * 100 / data['TotalNetPrice' + identifier]
    return data

def calc_weekly_turnover(data, identifier=''):
    """ Calculate weekly turnover from the data """
    if all(name in data.columns for name in ['StockOnHand' + identifier, 'SoldQty' + identifier]):
        data['WeeklyTurnover' + identifier] = data['StockOnHand' + identifier] / data['SoldQty' + identifier]
    return data

def calc_year_over_year(data, variables, time_unit='month'):
    """ Calculate year over year (YOY) from the data """
    time_keywords = ['day', 'week', 'month', 'year'] 
    no_value = '-'
    # Create a copy for last year's values
    data_last_year = data.copy()
    # Define variables for daily data processing
    if time_keywords[0] in time_unit.lower():
        print('Calculating YoY values based on daily data')
        data_last_year['Date'] = data['Date'] + pd.offsets.DateOffset(years=1)
        merge_columns = ['ShopNo', 'Date']
    # Define variables for weekly data processing
    elif time_keywords[1] in time_unit.lower():
        print('Calculating YoY values based on weekly data')
        data_last_year['Year'] = data_last_year['Year'].add(1)
        merge_columns = ['ShopNo', 'Year', 'WeekOfYear']
    # Define variables for monthly data processing
    elif time_keywords[2] in time_unit.lower():
        print('Calculating YoY values based on monthly data')
        data_last_year['Year'] = data_last_year['Year'].add(1)
        merge_columns = ['ShopNo', 'Year', 'MonthOfYear']
    # Define variables for yearly data processing
    elif time_keywords[3] in time_unit.lower():
        print('Calculating YoY values based on yearly data')
        data_last_year['Year'] = data_last_year['Year'].add(1)
        merge_columns = ['ShopNo', 'Year']
    # Calculate YOY values from the data
    if any(time_keyword in time_unit.lower() for time_keyword in time_keywords):
        # Merge this year's and last year's data together
        data_yoy = pd.merge(data, data_last_year, how='left', on=merge_columns)
        # Rename column names for each variable
        for column_name in list(data_yoy.columns.values):
            if '_x' in column_name:
                data_yoy.rename(columns={column_name: column_name.replace('_x', '')}, inplace=True)
            elif '_y' in column_name:
                data_yoy.rename(columns={column_name: column_name.replace('_y', 'LastYear')}, inplace=True)
        # Calculate YoY values for each variable
        for variable in variables:
            variable_last_year = variable + 'LastYear'
            yoy_name = variable + 'YoY'
            data_yoy[yoy_name] = (data_yoy[variable] - data_yoy[variable_last_year]) \
                                * 100 / data_yoy[variable_last_year]
        # Fill NaN and infinity values with '-'
        return data_yoy.replace([np.inf, -np.inf, np.nan], no_value)

def calc_daily_kpis(data, identifier=''):
    """ Calculate all KPIs using daily data """
    data = calc_average_selling_price(data, identifier)
    data = calc_average_transaction_value(data, identifier)
    data = calc_conversion_rate(data, identifier)
    data = calc_gross_profit(data, identifier)
    data = calc_gross_profit_margin(data, identifier)
    data = calc_operation_index(data, identifier)
    data = calc_original_retail_price(data, identifier)
    data = calc_profit_dilution(data, identifier)
    data = calc_quantity_per_receipt(data, identifier)
    return data

def calc_monthly_kpis(data, identifier=''):
    """ Calculate all KPIs using monthly data """
    data = calc_average_selling_price(data, identifier)
    data = calc_average_transaction_value(data, identifier)
    data = calc_cogs_margin(data, identifier)
    data = calc_conversion_rate(data, identifier)
    data = calc_depreciation_margin(data, identifier)
    data = calc_gross_profit(data, identifier)
    data = calc_gross_profit_margin(data, identifier)
    data = calc_office_fixed_cost_margin(data, identifier)
    data = calc_outlet_expenditure_margin(data, identifier)
    data = calc_net_profit(data, identifier)
    data = calc_net_profit_margin(data, identifier)
    data = calc_operation_index(data, identifier)
    data = calc_original_retail_price(data, identifier)
    data = calc_profit_dilution(data, identifier)
    data = calc_pub_margin(data, identifier)
    data = calc_quantity_per_receipt(data, identifier)
    data = calc_rental_margin(data, identifier)
    data = calc_staff_payroll_cpf_margin(data, identifier)
    data = calc_total_operation_cost(data, identifier)
    data = calc_total_operation_cost_margin(data, identifier)
    data = calc_total_cost(data, identifier)
    data = calc_total_cost_margin(data, identifier)
    return data

def add_total_summarised_data(predictions_table):

    hd_columns = predictions_table.columns
    agg_dictionary = {col: 'sum' for col in hd_columns if col not in ['ShopNo', 'Date']}
    total_summarised = predictions_table.groupby('Date').agg(agg_dictionary).reset_index()
    total_summarised['ShopNo'] = 'Total'
    predictions_table = pd.concat([predictions_table, total_summarised])
    return predictions_table
