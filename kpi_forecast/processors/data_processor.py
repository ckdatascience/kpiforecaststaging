import datetime
import numpy as np
import pandas as pd
from dateutil.relativedelta import relativedelta
from sklearn.model_selection import train_test_split


def get_categorial_features(data):
    """ Get categorical features that are not represented by floating-point numbers """
    return np.where(data.dtypes != np.float)[0]


def split_train_test(data, labels, train_size, shuffle=True):
    """ Split the dataset into training and testing sets """
    train_data, test_data, train_labels, test_labels = train_test_split(
        data, labels, train_size=train_size, shuffle=shuffle
    )
    return train_data, train_labels, test_data, test_labels


def split_train_test_by_date(data, labels, train_start_date, test_start_date):
    """ Split the dataset into training and testing sets based on the specified dates """
    train_start = datetime.datetime.strptime(train_start_date, '%Y-%m-%d')
    test_start = datetime.datetime.strptime(test_start_date, '%Y-%m-%d')
    data['Date'] = pd.to_datetime(data['Date'])
    train_data = data[(data['Date'] >= train_start) & (data['Date'] < test_start)]
    train_labels = labels.ix[train_data.index.values]
    test_data = data[(data['Date'] >= test_start)]
    test_labels = labels.ix[test_data.index.values]
    return train_data, train_labels, test_data, test_labels


def split_data_by_duration(data, duration, time_unit='day'):
    """ Split the data by duration from the latest data row """
    date_ = data['Date'].max()
    if 'day' in time_unit.lower():
        offset = pd.offsets.DateOffset(days=duration)
    elif 'month' in time_unit.lower():
        offset = pd.offsets.DateOffset(months=duration, days=-1)
    else:
        offset = pd.offsets.DateOffset(days=0)
    date_threshold = date_ - offset
    return data[data['Date'] < date_threshold], data[data['Date'] >= date_threshold]


def crop_dataset(data, start_date):
    """ Crop the dataset based on the starting date """
    data['Date'] = pd.to_datetime(data['Date'])
    return data[(data['Date'] >= datetime.datetime.strptime(start_date, '%Y-%m-%d'))]


def get_train_test_indexes_by_date(data, train_start_date, train_duration_years, test_duration, time_unit):
    """ Return indexes of the data for training and testing """
    # Define a function that changes the given date
    def change_date(date, years, duration, time_unit):
        if 'day' in time_unit.lower():
            return date + relativedelta(years=years, days=duration)
        elif 'month' in time_unit.lower():
            return date + relativedelta(years=years, months=duration)
        else:
            return date

    train_start_date = datetime.datetime.strptime(train_start_date, '%Y-%m-%d')
    train_end_date = change_date(train_start_date, years=train_duration_years, duration=0, time_unit=time_unit)
    test_end_date = change_date(train_start_date, years=train_duration_years, duration=test_duration, time_unit=time_unit)
    if 'day' in time_unit.lower():
        ind_train = (data['Date'] >= train_start_date) & (data['Date'] <= train_end_date)
        ind_test = (data['Date'] > train_end_date) & (data['Date'] <= test_end_date)
    elif 'month' in time_unit.lower():
        ind_train = (data['Date'] >= train_start_date) & (data['Date'] < train_end_date)
        ind_test = (data['Date'] >= train_end_date) & (data['Date'] < test_end_date)
    else:
        ind_train = (data['Date'] >= train_start_date) & (data['Date'] <= train_end_date)
        ind_test = (data['Date'] > train_end_date) & (data['Date'] <= test_end_date)
    return list(data[ind_train].index.values), list(data[ind_test].index.values)


def generate_dataset_by_date(data, labels, drop_columns, is_incremental=False, train_proportion=0.8, 
                             train_start_dates=None, test_start_date=None, train_duration_years=None, test_duration=None, time_unit='day'):
    """ Generate dataset based on the starting dates and learning mode (normal or incremental) """
    if is_incremental == True:
        # Split data into training and testing data based on the dates
        train_data, train_labels, test_data, test_labels = split_train_test_by_date(
            data,
            labels,
            train_start_dates[0],
            test_start_date
        )
        # Split training data into different folds for incremental learning
        train_data_set = []
        train_labels_set = []
        validate_data_set = []
        validate_labels_set = []
        for train_start_date in train_start_dates:
            train_indices, validate_indices = get_train_test_indexes_by_date(
                train_data, train_start_date, train_duration_years, test_duration, time_unit
            )
            train_data_set.append(train_data.ix[train_indices])
            train_labels_set.append(train_labels.ix[train_indices])
            validate_data_set.append(train_data.ix[validate_indices])
            validate_labels_set.append(train_labels.ix[validate_indices])
        processed_train_data = [train_data.drop(drop_columns, axis=1) for train_data in train_data_set]
        processed_validate_data = [validate_data.drop(drop_columns, axis=1) for validate_data in validate_data_set]
    else:
        # Split data into training and testing data
        train_data, train_labels, test_data, test_labels = split_train_test(
            data,
            labels,
            train_size=train_proportion,
            shuffle=False
        )
        # Split data into training and validation data
        train_data, train_labels, validate_data, validate_labels = split_train_test(
            train_data,
            train_labels,
            train_size=train_proportion,
            shuffle=False
        )
        processed_train_data = train_data.drop(drop_columns, axis=1)
        processed_validate_data = validate_data.drop(drop_columns, axis=1)
    processed_test_data = test_data.drop(drop_columns, axis=1)
    processed_train_labels = train_labels_set if is_incremental == True else train_labels
    processed_validate_labels = validate_labels_set if is_incremental == True else validate_labels
    return processed_train_data, processed_train_labels, processed_validate_data, processed_validate_labels, processed_test_data, test_labels


def postprocess_outputs(outputs, is_integer=False):
    """ Postprocess the outputs """
    outputs = [max(x, 0) for x in outputs]
    if is_integer == True:
        outputs = [round(x) for x in outputs]
    return outputs

def generate_train_test_dates(latest_date, train_duration_years, total_train_dates, train_interval_months, validate_duration_months, test_duration, time_unit):
    """ Generate the starting dates for training and testing sets """
    if 'day' in time_unit.lower():
        offset = relativedelta(days=test_duration)
    elif 'month' in time_unit.lower():
        offset = relativedelta(months=test_duration)
    else:
        offset = relativedelta(days=test_duration)
    start_date = (datetime.datetime.strptime(latest_date, '%Y-%m-%d') - offset).replace(day=1)
    test_start_date = start_date.strftime('%Y-%m-%d')
    start_date -= (relativedelta(years=train_duration_years) + relativedelta(months=validate_duration_months))
    train_start_dates = []
    for i in range(total_train_dates):
        train_start_dates.insert(i, start_date.strftime('%Y-%m-%d'))
        start_date -= relativedelta(months=train_interval_months)
    return sorted(train_start_dates), test_start_date