import os
import pandas as pd

def read_excel(workbook_name, sheet_names):
    """ Open an Excel spreadsheet and return the data in Pandas DataFrame format """
    print('Reading Excel spreadsheet:', workbook_name)
    data = {}
    workbook = pd.ExcelFile(workbook_name)
    for sheet_name in sheet_names:
        print('Sheet:', sheet_name)
        data[sheet_name] = pd.read_excel(workbook, sheet_name)
    return data

def write_excel(dataframes, workbook_name, sheet_names, overwrite=False):
    """ Write the Pandas DataFrame into an Excel spreadsheet """
    print('Writing Excel spreadsheet:', workbook_name)
    writer = pd.ExcelWriter(workbook_name, engine='openpyxl')
    names = sheet_names if len(sheet_names) == len(dataframes) else list(dataframes.keys())
    for name in names:
        print('Sheet:', name)
        dataframes[name].to_excel(writer, sheet_name=name, index=False)
    writer.save()
