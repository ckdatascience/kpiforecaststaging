class GroupsCreator(object):
    """Class that creates grouping for the date and the shoptype"""
    def __init__(self):
        pass

    def weekday(self, date, weekend=True):
        """ Function that returns 1 for weekday and for weekend or
            number of the day (0-6)
        """
        day = date.weekday()
        if weekend:
            if day < 5:
                return 1
            else:
                return 0

        return day

    def is_airport(self, ShopName):
        """ Function that returns 1 for airport shops 0 otherwise
        """
        if ShopName in ["T1", "T2", "T3", "T4"]:
            return 1

        return 0

    def is_normal(self, ShopName):
        """ Function that returns 1 for all shops excluding DFS BG and apirport shops 0 otherwise
        """
        if ShopName not in ["T1", "T2", "T3", "T4", "DFS", "BG"]:
            return 1

        return 0

    def is_outlet(self, ShopName):
        """ Function that returns 1 for outlet shops 0 otherwise
        """
        Marquee = ["AP", "CQM", "IMM"]
        if ShopName in Marquee:
            return 1

        return 0

    def is_suburb(self, ShopName):
        """ Function that returns 1 for heartland suburb mall shops 0 otherwise
        """
        if ShopName in ['NEX', 'JP', 'JEM', 'TM', 'CWP', 'WWP', 'BDM']:
            return 1

        return 0

    # Creation of the grouping variables
    def create_groupping_variables(self, kpi, time_unit = 'day'):
        """ Function that calculates groups for all data
        """
        if time_unit == 'day':
            kpi['Weekday'] = [self.weekday(day, weekend=False) for day in kpi.Date]
            kpi['Weekend'] = [self.weekday(day, weekend=True) for day in kpi.Date]
        kpi['Outlet'] = [self.is_outlet(shop) for shop in kpi.ShopNo]
        kpi['Airport'] = [self.is_airport(shop) for shop in kpi.ShopNo]
        kpi['Normal'] = [self.is_normal(shop) for shop in kpi.ShopNo]
        kpi['Suburb'] = [self.is_suburb(shop) for shop in kpi.ShopNo]

        return kpi
