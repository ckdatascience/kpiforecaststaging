import json

def read_json(filename):
    """ Read the data from a JSON file """
    return json.load(open(filename, 'r'))

def write_json(data, filename):
    """ Write the data into a JSON file """
    file = open(filename, 'w')
    file.write(json.dumps(data))
    file.close()
