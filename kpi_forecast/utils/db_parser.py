import copy
import pandas as pd
import pymssql

class MicrosoftSqlParser(object):
    """ A Microsoft SQL database parser """
    __connection = None
    __parameters = None
    __defaults = {
        'development': {
            'server': '192.168.6.11',
            'user': 'dw',
            'password': 'n0@cc355@all'
        },
        'production': {
            'server': '192.168.1.41',
            'user': 'dw',
            'password': 'n0@cc355@all'
        }        
    }

    def __init__(self, parameters=None):
        """ Constructor """
        self.__parameters = copy.deepcopy(self.__defaults)
        # Set defined parameter values if present
        if parameters is not None:
            self.__parameters.update(parameters)
        self.__connection = None

    def open(self, db_type='production'):
        """ Open the SQL connection """
        if db_type == 'development':
            self.__connection = pymssql.connect(
                server=self.__parameters['development']['server'],
                user=self.__parameters['development']['user'],
                password=self.__parameters['development']['password']
            )
        elif db_type == 'production':
            self.__connection = pymssql.connect(
                server=self.__parameters['production']['server'],
                user=self.__parameters['production']['user'],
                password=self.__parameters['production']['password']
            )
        else:
            self.__connection = pymssql.connect(
                server=self.__parameters['production']['server'],
                user=self.__parameters['production']['user'],
                password=self.__parameters['production']['password']
            )

    def close(self):
        """ Close the SQL connection """
        self.__connection.close()

    def execute_once(self, command, database_name):
        """ Establish a one-time connection and execute the SQL command and return data in Pandas DataFrame format """
        self.open(database_name)
        result = pd.read_sql(command, self.__connection)
        self.close()
        return result
