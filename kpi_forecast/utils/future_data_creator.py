import datetime
import pandas as pd
from dateutil.relativedelta import relativedelta

class FutureDataCreator(object):
    """ Future data creator """
    def create_future_data(self, current_shops_csv, duration, data=None, time_unit='day'):
        """ Generate dataframe with future dates for all current shops """
        max_date = max(data.Date)
        if time_unit == 'month':
            end_date = max_date + relativedelta(months=duration)
            duration_days = (end_date - max_date).days - 1
        else:
            duration_days = duration
        start_date = datetime.datetime.now() if data.empty else max(pd.to_datetime(data['Date'])) + datetime.timedelta(days=1)
        max_date = start_date + datetime.timedelta(days=duration_days)
        rng = pd.date_range(start_date.strftime("%Y-%m-%d"), max_date.strftime("%Y-%m-%d"), freq='D')
        date_frame = pd.DataFrame({'Date': rng})
        current_shops = pd.read_csv(current_shops_csv, index_col=False)
        date_frame['tmp'] = 1
        current_shops['tmp'] = 1
        date_frame = date_frame.merge(current_shops, on='tmp')
        del date_frame['tmp']
        return date_frame

    def add_future_data(self, current_shops_csv, duration, data, time_unit):
        """ Add future data to the existing dataframe """
        data['Date'] = pd.to_datetime(data.Date)
        feature_data = self.create_future_data(current_shops_csv, duration, data, time_unit)
        return pd.concat([data, feature_data], sort=False).sort_values(by=['ShopNo', 'Date'], ascending=True).reset_index(drop=True)

