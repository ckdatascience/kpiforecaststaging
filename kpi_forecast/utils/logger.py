import csv
import logging
import io

class CsvFormatter(logging.Formatter):
    """ A CSV formatter class used for logging purpose """

    def __init__(self):
        """ Constructor """ 
        super().__init__()
        self.output = io.StringIO()
        self.writer = csv.writer(self.output, quoting=csv.QUOTE_ALL)
    
    def format(self, record):
        """ Define CSV logging format """
        self.writer.writerow([record.levelname, record.msg])
        data = self.output.getvalue()
        self.output.truncate(0)
        self.output.seek(0)
        return data.strip()

def initialize_csv_logger(filename):
    """ Create a CSV logger """
    logger = logging.getLogger(filename)
    logger.setLevel(logging.DEBUG)
    logger.propagate = False
    console_handler = logging.StreamHandler()
    console_handler.setLevel(logging.DEBUG)
    console_handler.setFormatter(logging.Formatter('[%(asctime)s] %(levelname)s: %(message)s', '%Y-%m-%d %H:%M:%S'))
    file_handler = logging.FileHandler(filename)
    file_handler.setFormatter(logging.Formatter('%(asctime)s, %(levelname)s, %(message)s', '%Y-%m-%d %H:%M:%S'))
    logger.addHandler(console_handler)
    logger.addHandler(file_handler)
    return logger