import pandas as pd

class DataPatcher(object):
    """ A module that patches the missing data entries """

    def add_missing_data(self, data):
        """ Add the values for the missing dates in the data """
        shops = data['ShopNo'].unique()
        missing_data = pd.DataFrame()
        for shop in shops:
            start_date = data[data['ShopNo'] == shop]['Date'].min()
            end_date = data[data['ShopNo'] == shop]['Date'].max()
            all_dates = pd.DataFrame({'Date': pd.date_range(start_date, end_date, freq='D')})
            missing_shop_data = all_dates[~all_dates['Date'].isin(data[data['ShopNo'] == shop]['Date'])]
            missing_shop_data['ShopNo'] = shop
            missing_data = missing_data.append(missing_shop_data, ignore_index=True)
        columns = [column for column in list(data.columns.values) if column not in ('Date', 'ShopNo')]
        for column in columns:
            missing_data[column] = 0
        missing_data['Date'] = pd.to_datetime(missing_data['Date'])
        data['Date'] = pd.to_datetime(data['Date'])
        return data.append(missing_data, ignore_index=True).sort_values(by=['ShopNo', 'Date'], ascending=True).reset_index(drop=True)
