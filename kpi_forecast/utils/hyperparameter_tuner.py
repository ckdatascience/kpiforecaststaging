import pandas as pd
from sklearn.model_selection import GridSearchCV, RandomizedSearchCV

class HyperparameterTuner(object):
    """ Hyperparameter tuning module """
    __parameters = None
    __defaults = {
        'bayesian_search': {
            'init_points': 15,  # Total steps of random exploration to perform
            'n_iter': 25,       # Total steps of Bayesian optimization to perform
            'verbose': 1,       # Verbosity level (the higher the more messages)
            'random_state': 0   # Random seed
        },
        'grid_search': {
            'processes': -1,    # Total parallel jobs (-1: max)
            'folds': 2,         # Total folds that determine the cross validation splitting strategy
            'verbosity': 1      # Verbosity level (the higher the more messages)
        },
        'randomized_search': {
            'iterations': 10,   # Total parameter settings that are sampled
            'processes': -1,    # Total parallel jobs (-1: max)
            'folds': 2,         # Total folds that determine the cross validation splitting strategy
            'verbosity': 1,     # Verbosity level (the higher the more messages)
            'random_seed': 0    # Random seed
        }
    }

    def __init__(self, parameters=None):
        """ Constructor """
        self.__parameters = self.__defaults.copy()
        # Set defined parameter values if present
        if parameters != None:
            self.__parameters['bayesian_search'].update(parameters['bayesian_search'])
            self.__parameters['grid_search'].update(parameters['grid_search'])
            self.__parameters['randomized_search'].update(parameters['randomized_search'])

    def get_parameters(self):
        """ Get the parameters """
        return self.__parameters

    def perform_grid_search(self, model, parameters_list, train_data, train_labels, train_parameters):
        """ Perform grid search of hyperparameters """
        # Instantiate the grid search precedure
        group = GridSearchCV(
            estimator=model,
            param_grid=parameters_list, 
            n_jobs=self.__parameters['grid_search']['processes'],
            cv=self.__parameters['grid_search']['folds'],
            verbose=self.__parameters['grid_search']['verbosity']
        )
        # Perform grid search
        group.fit(train_data, train_labels, **train_parameters)
        # Return the best estimator with the corresponding parameters and score
        return (
            group.best_estimator_, 
            group.best_params_, 
            abs(group.best_score_),
            pd.DataFrame(group.cv_results_).sort_values(
                by=['mean_test_score'], ascending=False, inplace=True
            )
        )

    def perform_randomized_search(self, model, parameters_list, train_data, train_labels, train_parameters):
        """ Perform randomized search of hyperparameters """
        group = RandomizedSearchCV(
            estimator=model,
            param_distributions=parameters_list,
            n_iter=self.__parameters['randomized_search']['iterations'],
            n_jobs=self.__parameters['randomized_search']['processes'],
            cv=self.__parameters['randomized_search']['folds'],
            verbose=self.__parameters['randomized_search']['verbosity'],
            random_state=self.__parameters['randomized_search']['random_seed']
        )
        # Perform randomized search
        group.fit(train_data, train_labels, **train_parameters)
        # Return the best estimator with the corresponding score and parameters
        return (
            group.best_estimator_, 
            group.best_params_, 
            abs(group.best_score_),
            pd.DataFrame(group.cv_results_).sort_values(
                by=['mean_test_score'], ascending=False, inplace=True
            )
        )
