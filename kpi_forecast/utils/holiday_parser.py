import holidays
import numpy as np
import pandas as pd
import workalendar.africa as africa
import workalendar.america as america
import workalendar.asia as asia
import workalendar.europe as europe
import workalendar.oceania as oceania
import workalendar.usa as usa
from datetime import datetime

class HolidayParser(object):
    """ Holiday parser """
    # List of holidays by workalendar module
    __workalendar_country_names = [
        # Europe
        'Austria', 'Belgium', 'Bulgaria', 'Cayman Islands', 'Croatia', 'Cyprus', 'Czech Republic', 'Denmark', 
        'Estonia', 'Finland', 'France', 'Germany', 'Greece', 'Hungary', 'Iceland', 'Ireland', 
        'Italy', 'Latvia', 'Lithuania', 'Luxembourg', 'Malta', 'Netherlands', 'Norway', 'Poland', 
        'Portugal', 'Romania', 'Russia', 'Slovakia', 'Sweden', 'Spain', 'Slovenia', 'Switzerland', 
        'United Kingdom',
        # America
        'Brazil', 'Chile', 'Colombia', 'Mexico', 'Panama', 'Paraguay', 'United States of America', 'Canada',
        # Asia
        'China', 'Hong Kong', 'Japan', 'Malaysia', 'Qatar', 'Singapore', 'South Korea', 'Taiwan',
        'Israel',
        # Oceania
        'Australia', 'Marshall Islands', 'New Zealand',
        # Africa
        'Algeria', 'Angola', 'Benin', 'Ivory Coast', 'Madagascar', 'Sao Tome and Principe', 'South Africa'
    ]
    # List of holidays by holidays module
    __holidays_country_names = [
        'Argentina', 'Belarus', 'India', 'Isle of Man', 'Ukraine', 'Wales'
    ]

    def get_holidays_workalendar(self, start_year, end_year, country, state=None):
        """ Get holiday list of a specified country from workalendar module """
        france_states = {
            'alsace': europe.FranceAlsaceMoselle(),
            'moselle': europe.FranceAlsaceMoselle(),
        }
        spain_states = {
            'catalonia': europe.Catalonia(),
        }
        switzerland_states = {
            'vaud': europe.Vaud(),
        }
        united_kingdom_states = {
            'northern ireland': europe.UnitedKingdomNorthernIreland(),
            'scotland': europe.Scotland(),
            'aberdeen': europe.Aberdeen(),
            'angus': europe.Angus(),
            'ayr': europe.Ayr(),
            'carnoustie': europe.CarnoustieMonifieth(),
            'monifieth': europe.CarnoustieMonifieth(),
            'clydebank': europe.Clydebank(),
            'dumfries': europe.DumfriesGalloway(),
            'galloway': europe.DumfriesGalloway(),
            'dundee': europe.Dundee(),
            'east dunbartonshire': europe.EastDunbartonshire(),
            'edinburgh': europe.Edinburgh(),
            'elgin': europe.Elgin(),
            'falkirk': europe.Falkirk(),
            'fife': europe.Fife(),
            'galashiels': europe.Galashiels(),
            'glasgow': europe.Glasgow(),
            'hawick': europe.Hawick(),
            'inverclyde': europe.Inverclyde(),
            'inverness': europe.Inverness(),
            'kilmarnock': europe.Kilmarnock(),
            'lanark': europe.Lanark(),
            'linlithgow': europe.Linlithgow(),
            'lochaber': europe.Lochaber(),
            'north lanarkshire': europe.NorthLanarkshire(),
            'paisley': europe.Paisley(),
            'perth': europe.Perth(),
            'scottish borders': europe.ScottishBorders(),
            'south lanarkshire': europe.SouthLanarkshire(),
            'stirling': europe.Stirling(),
            'west dunbartonshire': europe.WestDunbartonshire(),
        }
        germany_states = {
            'baden wurttemberg': europe.BadenWurttemberg(),
            'bavaria': europe.Bavaria(),
            'berlin': europe.Berlin(),
            'brandenburg': europe.Brandenburg(),
            'bremen': europe.Bremen(),
            'hamburg': europe.Hamburg(),
            'hesse': europe.Hesse(),
            'lower saxony': europe.LowerSaxony(),
            'mecklenburg-vorpommern': europe.MecklenburgVorpommern(),
            'north rhine-westphalia': europe.NorthRhineWestphalia(),
            'rhineland-palatinate': europe.RhinelandPalatinate(),
            'saarland': europe.Saarland(),
            'saxony': europe.Saxony(),
            'saxony-anhalt': europe.SaxonyAnhalt(),
            'schleswig-holstein': europe.SchleswigHolstein(),
            'thuringia': europe.Thuringia(),
        }
        canada_states = {
            'alberta': america.Alberta(),
            'british columbia': america.BritishColumbia(),
            'manitoba': america.Manitoba(),
            'new brunswick': america.NewBrunswick(),
            'newfoundland': america.Newfoundland(),
            'northwest territories': america.NorthwestTerritories(),
            'nova scotia': america.NovaScotia(),
            'nunavut': america.Nunavut(),
            'ontario': america.Ontario(),
            'prince edward island': america.PrinceEdwardIsland(),
            'quebec': america.Quebec(),
            'saskatchewan': america.Saskatchewan(),
            'yukon': america.Yukon(),
        }
        brazil_states = {
            'acre': america.BrazilAcre(),
            'alagoas': america.BrazilAlagoas(),
            'amapa': america.BrazilAmapa(),
            'amazonas': america.BrazilAmazonas(),
            'bahia': america.BrazilBahia(),
            'brazil bank calendar': america.BrazilBankCalendar(),
            'cariacica': america.BrazilCariacicaCity(),
            'ceara': america.BrazilCeara(),
            'brazil distrito federal': america.BrazilDistritoFederal(),
            'espirito santo': america.BrazilEspiritoSanto(),
            'goias': america.BrazilGoias(),
            'guarapari': america.BrazilGuarapariCity(),
            'maranhao': america.BrazilMaranhao(),
            'mato grosso': america.BrazilMatoGrosso(),
            'mato grosso do sul': america.BrazilMatoGrossoDoSul(),
            'para': america.BrazilPara(),
            'paraiba': america.BrazilParaiba(),
            'pernambuco': america.BrazilPernambuco(),
            'piaui': america.BrazilPiaui(),
            'rio de janeiro': america.BrazilRioDeJaneiro(),
            'rio grande do norte': america.BrazilRioGrandeDoNorte(),
            'rio grande do sul': america.BrazilRioGrandeDoSul(),
            'rondonia': america.BrazilRondonia(),
            'roraima': america.BrazilRoraima(),
            'santa catarina': america.BrazilSantaCatarina(),
            'sao paulo city': america.BrazilSaoPauloCity(),
            'sao paulo': america.BrazilSaoPauloState(),
            'sergipe': america.BrazilSergipe(),
            'serra': america.BrazilSerraCity(),
            'tocantins': america.BrazilTocantins(),
            'vila velha': america.BrazilVilaVelhaCity(),
            'vitoria': america.BrazilVitoriaCity(),
        }
        united_states_of_america_states = {
            'alabama': usa.Alabama(),
            'alabama baldwin county': usa.AlabamaBaldwinCounty(),
            'alabama mobile county': usa.AlabamaMobileCounty(),
            'alabama perry county': usa.AlabamaPerryCounty(),
            'alaska': usa.Alaska(),
            'american samoa': usa.AmericanSamoa(),
            'arizona': usa.Arizona(),
            'arkansas': usa.Arkansas(),
            'california': usa.California(),
            'chicago': usa.ChicagoIllinois(),
            'colorado': usa.Colorado(),
            'connecticut': usa.Connecticut(),
            'delaware': usa.Delaware(),
            'district of columbia': usa.DistrictOfColumbia(),
            'florida': usa.Florida(),
            'georgia': usa.Georgia(),
            'guam': usa.Guam(),
            'hawaii': usa.Hawaii(),
            'idaho': usa.Idaho(),
            'illinois': usa.Illinois(),
            'indiana': usa.Indiana(),
            'iowa': usa.Iowa(),
            'kansas': usa.Kansas(),
            'kentucky': usa.Kentucky(),
            'louisiana': usa.Louisiana(),
            'maine': usa.Maine(),
            'maryland': usa.Maryland(),
            'massachusetts': usa.Massachusetts(),
            'michigan': usa.Michigan(),
            'minnesota': usa.Minnesota(),
            'mississippi': usa.Mississippi(),
            'missouri': usa.Missouri(),
            'montana': usa.Montana(),
            'nebraska': usa.Nebraska(),
            'nevada': usa.Nevada(),
            'new hampshire': usa.NewHampshire(),
            'new jersey': usa.NewJersey(),
            'new mexico': usa.NewMexico(),
            'new york': usa.NewYork(),
            'north carolina': usa.NorthCarolina(),
            'north dakota': usa.NorthDakota(),
            'ohio': usa.Ohio(),
            'oklahoma': usa.Oklahoma(),
            'oregon': usa.Oregon(),
            'pennsylvania': usa.Pennsylvania(),
            'rhode island': usa.RhodeIsland(),
            'south carolina': usa.SouthCarolina(),
            'south dakota': usa.SouthDakota(),
            'suffolk county massachusetts': usa.SuffolkCountyMassachusetts(),
            'tennessee': usa.Tennessee(),
            'texas': usa.Texas(),
            'texas base': usa.TexasBase(),
            'utah': usa.Utah(),
            'vermont': usa.Vermont(),
            'virginia': usa.Virginia(),
            'washington': usa.Washington(),
            'west virginia': usa.WestVirginia(),
            'wisconsin': usa.Wisconsin(),
            'wyoming': usa.Wyoming(),
        }
        australia_states = {
            'australian capital territory': oceania.AustralianCapitalTerritory(),
            'hobart': oceania.Hobart(),
            'new south wales': oceania.NewSouthWales(),
            'northern territory': oceania.NorthernTerritory(),
            'queensland': oceania.Queensland(),
            'south australia': oceania.SouthAustralia(),
            'tasmania': oceania.Tasmania(),
            'victoria': oceania.Victoria(),
            'western australia': oceania.WesternAustralia()
        }
        states = {
            'france': france_states.get(state.lower()),
            'spain': spain_states.get(state.lower()),
            'switzerland': switzerland_states.get(state.lower()),
            'united kingdom': united_kingdom_states.get(state.lower()),
            'germany': germany_states.get(state.lower()),
            'canada': canada_states.get(state.lower()),
            'brazil': brazil_states.get(state.lower()),
            'united states of america': united_states_of_america_states.get(state.lower()),
            'australia': australia_states.get(state.lower())
        } if state is not None else None
        countries = {
            'austria': europe.Austria(),
            'belgium': europe.Belgium(), 
            'bulgaria': europe.Bulgaria(),
            'cayman islands': europe.CaymanIslands(), 
            'croatia': europe.Croatia(), 
            'cyprus': europe.Cyprus(), 
            'czech republic': europe.CzechRepublic(), 
            'denmark': europe.Denmark(), 
            'estonia': europe.Estonia(), 
            'european central bank': europe.EuropeanCentralBank(), 
            'finland': europe.Finland(),
            'france': europe.France(),
            'germany': europe.Germany(), 
            'greece': europe.Greece(), 
            'hungary': europe.Hungary(), 
            'iceland': europe.Iceland(),
            'ireland': europe.Ireland(), 
            'italy': europe.Italy(),
            'latvia': europe.Latvia(), 
            'lithuania': europe.Lithuania(), 
            'luxembourg': europe.Luxembourg(), 
            'malta': europe.Malta(), 
            'netherlands': europe.Netherlands(), 
            'norway': europe.Norway(),
            'poland': europe.Poland(), 
            'portugal': europe.Portugal(), 
            'romania': europe.Romania(), 
            'russia': europe.Russia(), 
            'slovakia': europe.Slovakia(), 
            'sweden': europe.Sweden(),
            'spain': europe.Spain(), 
            'slovenia': europe.Slovenia(),
            'switzerland': europe.Switzerland(),
            'united kingdom': europe.UnitedKingdom(),
            'brazil': america.Brazil(), 
            'chile': america.Chile(), 
            'colombia': america.Colombia(), 
            'mexico': america.Mexico(), 
            'panama': america.Panama(), 
            'paraguay': america.Paraguay(),
            'united states of america': usa.UnitedStates(), 
            'canada': america.Canada(),
            'china': asia.China(), 
            'hong kong': asia.HongKong(), 
            'japan': asia.Japan(), 
            'malaysia': asia.Malaysia(), 
            'qatar': asia.Qatar(), 
            'singapore': asia.Singapore(), 
            'south korea': asia.SouthKorea(), 
            'taiwan': asia.Taiwan(),
            'israel': asia.Israel(),
            'australia': oceania.Australia(), 
            'marshall islands': oceania.MarshallIslands(), 
            'new zealand': oceania.NewZealand(),
            'algeria': africa.Algeria(), 
            'angola': africa.Angola(), 
            'benin': africa.Benin(), 
            'ivory coast': africa.IvoryCoast(), 
            'madagascar': africa.Madagascar(), 
            'sao tome and principe': africa.SaoTomeAndPrincipe(), 
            'south africa': africa.SouthAfrica()
        }
        selected_country = countries.get(country.lower())
        selected_state = states.get(country.lower()) if state is not None else None
        dates = []

        for i in range(start_year, end_year + 1):
            dates += selected_country.holidays(year=i)
            if selected_state is not None:
                dates += selected_state.holidays(year=i)
        return pd.DataFrame(np.asarray(dates).reshape(-1, 2), columns=['Date', 'Holiday'])

    def get_holidays_holidays(self, start_year, end_year, country, state=None):
        """ Get holiday list of a specified country from holidays module """
        years = list(range(start_year, end_year + 1))
        india_states = {
            'assam': 'AS',
            'sikkim': 'SK', 
            'chhattisgarh': 'CG', 
            'karnataka': 'KA', 
            'gujarat': 'GJ', 
            'bihar': 'BR', 
            'rajasthan': 'RJ', 
            'odisha': 'OD', 
            'tamil nadu': 'TN', 
            'andhra pradesh': 'AP', 
            'west bengal': 'WB', 
            'kerala': 'KL', 
            'haryana': 'HR', 
            'maharashtra': 'MH', 
            'madhya pradesh': 'MP', 
            'uttar pradesh': 'UP', 
            'uttarakhand': 'UK'
        }
        countries = {
            'argentina': holidays.Argentina(years=years), 
            'belarus': holidays.Belarus(years=years),
            'india': holidays.India(years=years, state=india_states.get(state)), 
            'isle of man': holidays.IsleOfMan(years=years), 
            'ukraine': holidays.Ukraine(years=years), 
            'wales': holidays.Wales(years=years)
        }
        dates = countries.get(country.lower())
        return pd.DataFrame(dates.items(), columns=['Date', 'Holiday'])

    def get_holidays(self, start_date, end_date, country, state=None):
        """ Get holiday list of a specified country """
        if not isinstance(start_date, pd.Timestamp):
            start_date = pd.to_datetime(start_date)
        if not isinstance(end_date, pd.Timestamp):
            end_date = pd.to_datetime(end_date)
        dates = pd.DataFrame()
        if any(country.lower() in country_name.lower() for country_name in self.__workalendar_country_names):
            dates = self.get_holidays_workalendar(start_date.year, end_date.year, country, state)
        elif any(country.lower() in country_name.lower() for country_name in self.__holidays_country_names):
            dates = self.get_holidays_holidays(start_date.year, end_date.year, country, state)
        dates = dates.drop_duplicates(subset='Date', keep='first')
        dates['Date'] = pd.to_datetime(dates['Date'])
        return dates[(dates['Date'] >= start_date) & (dates['Date'] <= end_date)].reset_index() if not dates.empty else dates.reset_index()
