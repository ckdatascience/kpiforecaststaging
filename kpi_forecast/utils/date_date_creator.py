import pandas as pd
import calendar
from workalendar.asia import Singapore
from collections import Counter


class DateDataCreator(object):
    """Class that creates grouping for the date and the shoptype"""

    def __init__(self):
        self.cal = Singapore()

    def aggregate_day_to_month(self, data, aggregate_columns):
        """ Aggregate daily data on monthly basis """
        monthly_data = data.copy()
        # Extract additional time info from the existing data
        monthly_data['Year'] = monthly_data['Date'].dt.year
        monthly_data['Month'] = monthly_data['Date'].dt.month
        # Aggregate the data
        group_columns = ['ShopNo', 'Year', 'Month']
        agg_dict = {col: 'sum' for col in aggregate_columns}
        agg_dict['Date'] = 'count'
        print(agg_dict)
        monthly_data = monthly_data.groupby(group_columns).agg(agg_dict).reset_index()
        monthly_data['work_days'] = monthly_data['Date']
        # Sort the values
        return monthly_data.sort_values(group_columns)

    def create_workdays_proportion(self, data):
        data['work_days_p'] = data['work_days'] / data['N_days']
        work_days_p_data = data[['ShopNo', 'Month', 'Year', 'work_days_p']].copy()
        work_days_p_data['Year'] = work_days_p_data['Year'] + 1
        work_days_p_data.columns = ['ShopNo', 'Month', 'Year', 'work_days_p_1y']
        data = data.merge(work_days_p_data, on=['ShopNo', 'Month', 'Year'], how='left')
        data['work_days_pp_1y'] = [i / j for i, j in zip(data['work_days_p'], data['work_days_p_1y'])]
        return data

    def week_weekend_days(self, year, month):
        weekday_count = 0
        weekend_count = 0
        cal = calendar.Calendar()
        for week in cal.monthdayscalendar(year, month):
            for i, day in enumerate(week):
                # not this month's day or a weekend
                if day == 0:
                    continue
                if i >= 5:
                    weekend_count += 1
                # or some other control if desired...
                else:
                    weekday_count += 1
        return weekend_count, weekday_count

    # week_weekend_days(2018,1)

    def get_calendar_holiday_df(self, name):
        final_df = pd.DataFrame()
        for year in range(2013, 2021):
            if name == 'All_holidays':
                months = [element[0].month for element in self.cal.holidays(year) if not 'shift' in element[1]]
            else:
                months = [element[0].month for element in self.cal.holidays(year) if name in element[1]]
            freq = Counter(months)
            df = pd.DataFrame.from_dict(freq, orient='index').reset_index()

            df = df.rename(columns={'index': 'Month', 0: name})
            df['Year'] = year
            final_df = pd.concat([df, final_df])
        return final_df

    def create_date_table(self, start='2013-01-01', end='2020-12-31'):
        df = pd.DataFrame({"Date": pd.date_range(start, end, freq='M')})
        df["Month"] = df.Date.dt.month
        df["Quarter"] = df.Date.dt.quarter
        df["Year"] = df.Date.dt.year
        df["Year_half"] = (df.Quarter + 1) // 2

        temp = [self.week_weekend_days(i, j) for i, j in zip(df['Year'], df['Month'])]
        df["N_weekends"] = [i[0] for i in temp]
        df["N_weekdays"] = [i[1] for i in temp]
        df["N_days"] = df["N_weekends"] + df["N_weekdays"]
        temp = [self.week_weekend_days(i - 1, j) for i, j in zip(df['Year'], df['Month'])]
        df["N_weekends_1y"] = [i[0] for i in temp]
        df["N_weekdays_1y"] = [i[1] for i in temp]

        df["N_weekends_p_1y"] = df["N_weekends"] / df["N_weekends_1y"]
        df["N_weekdays_p_1y"] = df["N_weekdays"] / df["N_weekdays_1y"]

        return df

    def create_calendar_full_data(self, start='2013-01-01', end='2020-12-01'):
        calendar_data = self.create_date_table(start=start, end=end)

        all_ = self.get_calendar_holiday_df('All_holidays')
        cny_ = self.get_calendar_holiday_df('Lunar')
        hariraya_ = self.get_calendar_holiday_df('Hari Raya Puasa')

        calendar_data = calendar_data.merge(all_, on=['Year', 'Month'], how='left')
        calendar_data = calendar_data.merge(cny_, on=['Year', 'Month'], how='left')
        calendar_data = calendar_data.merge(hariraya_, on=['Year', 'Month'], how='left')
        del calendar_data['Date']
        calendar_data = calendar_data.fillna(0)

        return calendar_data
