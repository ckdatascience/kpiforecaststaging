import datetime
from datetime import date


class RolledShiftedFeaturesDataCreator():

    def __init__(self, feature, time_unit):
        '''Parameters, data load, and initial transformation'''
        self.features = [feature]
        self.time_unit = time_unit
        self.aggregate_funs = ['median']
        if time_unit == 'day':
            self.group_times = ['Weekend', 'Weekday', '']
            self.group_shops = ['Outlet', 'Normal', 'Airport', 'Suburb', 'ShopNo']
        else:
            self.group_times = [('', '20d'), ('', '350d'), ('Year_half', '350d'), ('Quarter', '350d')]
            self.group_shops = ['ShopNo', 'Outlet', 'Airport', 'Normal', 'Suburb']
        self.peroids = ['1d', '7d', '30d']

    def add_years(self, d, years):
        """Return a date that's `years` years after the date (or datetime)
        object `d`. Return the same calendar date (month and day) in the
        destination year, if it exists, otherwise use the following day
        (thus changing February 29 to March 1).

        """
        try:
            return d.replace(year=d.year + years)
        except ValueError:
            return d + (date(d.year + years, 1, 1) - date(d.year, 1, 1))

    def create_name(self, col, time, group_time, group_shops):
        if col[0] in ['ShopNo', 'Date', 'Weekday', 'Weekend', 'Quarter', 'Year_half', 'Year', 'Month', 'Outlet',
                      'Airport', 'Normal', 'Suburb']:
            return col[0]
        elif 'level' in col[0]:
            return 'Date'
        return '_'.join(col) + '_' + group_time + '_' + group_shops + '_' + time

    def create_shift_name(self, col, days=0, years=1):
        if col not in ['ShopNo', 'Date', 'Weekday', 'Weekend', 'Quarter', 'Year', 'Month', 'Outlet', 'Airport',
                       'Normal', 'Suburb']:
            return col + '_shift_' + str(years) + '_y_' + str(days) + '_d'

        return col

    def create_grouped_rolling_data(self, kpi_, features, group_shops='ShopNo', group_time='Weekday', peroid='7d',
                                    min_peroids=1, time_unit='day',
                                    aggregate_funs=['sum', 'mean', 'min', 'median', 'max']):

        group_aggregate_dict = {feature: 'mean' for feature in features}
        aggregates = {feature: aggregate_funs for feature in features}
        if time_unit == 'day':
            grouping_vars = ['Date']
        else:
            grouping_vars = ['Date', 'Year', 'Month']

        rolling_grouping = []

        if len(group_time) > 0:
            grouping_vars.append(group_time)
            rolling_grouping.append(group_time)
        if len(group_shops) > 0:
            grouping_vars.append(group_shops)
            rolling_grouping.append(group_shops)

        kpi = kpi_.copy()
        kpi_groupped = kpi.groupby(grouping_vars).agg(group_aggregate_dict).reset_index()
        kpi_groupped.set_index('Date', inplace=True)

        kpi_groupped_rolled = kpi_groupped.groupby(rolling_grouping).rolling(peroid, min_peroids=min_peroids).agg(
            aggregates).reset_index()
        kpi_groupped_rolled.columns = [self.create_name(col, peroid, group_time, group_shops) for col in
                                       kpi_groupped_rolled.columns]
        kpi_groupped_rolled = kpi_groupped_rolled.reset_index()

        del kpi_groupped_rolled['index']

        if time_unit == 'month':
            kpi_groupped_rolled['Year'] = kpi_groupped_rolled.Date.dt.year
            kpi_groupped_rolled['Month'] = kpi_groupped_rolled.Date.dt.month

            del kpi_groupped_rolled['Date']
            if len(group_time) > 0:
                del kpi_groupped_rolled[group_time]

        return kpi_groupped_rolled

    def change_date(self, date_col, days, years):

        return [self.add_years(day, years) + datetime.timedelta(days=days) for day in date_col]

    def create_shifted_dataframe(self, kpi_groupped_rolled_, days, years, group_shop):
        kpi_groupped_rolled = kpi_groupped_rolled_.copy()

        kpi_groupped_rolled['Date'] = self.change_date(kpi_groupped_rolled.Date, days=days, years=years)
        groupby_vars = ['Date', group_shop]
        if group_shop == '':
            groupby_vars = ['Date']
        kpi_groupped_rolled = kpi_groupped_rolled.groupby(groupby_vars).agg('mean').reset_index()

        colnames = [self.create_shift_name(col, days, years) for col in kpi_groupped_rolled.columns]
        kpi_groupped_rolled.columns = colnames

        return kpi_groupped_rolled

    def create_rolled_shifted_data_time_unit(self, kpi, features, group_shops='ShopNo', group_time='Weekday',
                                   aggregate_funs=['sum', 'mean', 'min', 'median', 'max'], time_unit='day'):
        if time_unit == 'day':
            kpi_groupped_rolled_shift = self.create_rolled_shifted_data(kpi, features, group_shops, group_time,
                                                                        aggregate_funs)
        elif time_unit == 'month':
            kpi_groupped_rolled_shift = self.create_rolled_shifted_data_monthly(kpi)
        return kpi_groupped_rolled_shift

    def create_rolled_shifted_data_monthly(self, kpi_groupped_rolled_):
        kpi_groupped_rolled = kpi_groupped_rolled_.copy()

        kpi_groupped_rolled['Year'] = kpi_groupped_rolled['Year'] + 1

        colnames = [self.create_shift_name(col) for col in kpi_groupped_rolled.columns]
        kpi_groupped_rolled.columns = colnames

        return kpi_groupped_rolled

    def create_rolled_shifted_data(self, kpi, features, group_shops='ShopNo', group_time='Weekday',
                                   aggregate_funs=['sum', 'mean', 'min', 'median', 'max']):
        kpi_groupped_rolled_1 = self.create_grouped_rolling_data(kpi, features, group_shops, group_time, '1d',
                                                                 min_peroids=1, aggregate_funs=aggregate_funs)
        kpi_groupped_rolled_7 = self.create_grouped_rolling_data(kpi, features, group_shops, group_time, '7d',
                                                                 min_peroids=1, aggregate_funs=aggregate_funs)
        kpi_groupped_rolled_30 = self.create_grouped_rolling_data(kpi, features, group_shops, group_time, '30d',
                                                                  min_peroids=1, aggregate_funs=aggregate_funs)

        if len(group_shops) > 0:
            group_variables = ['Date', group_shops]
        else:
            group_variables = ['Date']

        if len(group_time) > 0:
            del kpi_groupped_rolled_1[group_time]
            del kpi_groupped_rolled_7[group_time]
            del kpi_groupped_rolled_30[group_time]

        kpi_groupped_rolled = kpi_groupped_rolled_1.merge(kpi_groupped_rolled_7, how='left', on=group_variables)
        kpi_groupped_rolled = kpi_groupped_rolled.merge(kpi_groupped_rolled_30, how='left', on=group_variables)

        kpi_groupped_rolled_shift_1y_91d = self.create_shifted_dataframe(kpi_groupped_rolled, days=91, years=1,
                                                                         group_shop=group_shops)
        kpi_groupped_rolled_shift_1y_0d = self.create_shifted_dataframe(kpi_groupped_rolled, days=0, years=1,
                                                                        group_shop=group_shops)
        kpi_groupped_rolled_shift_0y_91d = self.create_shifted_dataframe(kpi_groupped_rolled, days=91, years=0,
                                                                         group_shop=group_shops)

        group_variables = ['Date', group_shops]
        kpi_groupped_rolled_shift = kpi_groupped_rolled_shift_0y_91d.merge(kpi_groupped_rolled_shift_1y_0d, how='left',
                                                                           on=group_variables)
        kpi_groupped_rolled_shift = kpi_groupped_rolled_shift.merge(kpi_groupped_rolled_shift_1y_91d, how='left',
                                                                    on=group_variables)

        return kpi_groupped_rolled_shift

    def create_grouped_rolling_data_all_monthly(self, data, predicted_var, time_groupings, shop_groupings,
                                                aggregate_funs=['mean'], min_peroids=1, time_unit='day'):
        data_all = data.copy()
        for time in time_groupings:
            for shop in shop_groupings:
                grouped_data = self.create_grouped_rolling_data(data, predicted_var, group_shops=shop,
                                                                group_time=time[0], peroid=time[1],
                                                                min_peroids=min_peroids, aggregate_funs=aggregate_funs,
                                                                time_unit=time_unit)
                grouped_data = self.create_rolled_shifted_data_monthly(grouped_data)
                data_all = data_all.merge(grouped_data, on=[shop, 'Month', 'Year'], how='left')
        return data_all

    def kpi_groupped_rolled_shift_all(self, kpi_, features, group_shop='ShopNo', group_time='Weekday',
                                      aggregate_funs=['sum', 'mean', 'min', 'median', 'max'],
                                      peroids=['1d', '7d', '30d'], time_unit='day'):
        kpi = kpi_.copy()
        kpi_groupped_rolled_shift = self.create_rolled_shifted_data_time_unit(kpi, features, group_shops=group_shop,
                                                                              group_time=group_time,
                                                                              aggregate_funs=aggregate_funs,
                                                                              time_unit=time_unit)
        for feature in features:
            for aggregate in aggregate_funs:
                for peroid in peroids:
                    name = feature + '_' + aggregate + '_' + group_time + '_' + group_shop + '_' + peroid
                    ccols = kpi_groupped_rolled_shift.filter(like=name).columns
                    kpi_groupped_rolled_shift[name + '_91d_p_1y'] = kpi_groupped_rolled_shift[ccols[0]] / \
                                                                    kpi_groupped_rolled_shift[ccols[1]]
                    kpi_groupped_rolled_shift[name + '_1y_p_1y_91d'] = kpi_groupped_rolled_shift[ccols[1]] / \
                                                                       kpi_groupped_rolled_shift[ccols[2]]
                    kpi_groupped_rolled_shift[name + '_91d_p_1y_91d'] = kpi_groupped_rolled_shift[ccols[0]] / \
                                                                        kpi_groupped_rolled_shift[ccols[2]]

                    kpi_groupped_rolled_shift[name + '_scaled_1y_by_91d_p_1y_91d'] = \
                        kpi_groupped_rolled_shift[ccols[0]] / \
                        kpi_groupped_rolled_shift[ccols[2]] * \
                        kpi_groupped_rolled_shift[ccols[1]]

        return kpi_groupped_rolled_shift

    def create_master_data_daily(self, kpi, features, group_times, group_shops, aggregate_funs, peroids, time_unit='day'):
        kpi_master = kpi.copy()
        for group_time in group_times:
            for group_shop in group_shops:
                group_variables = ['Date', group_shop]
                kpi_groupped_rolled_shift = self.kpi_groupped_rolled_shift_all(kpi, features, group_shop=group_shop,
                                                                               group_time=group_time,
                                                                               aggregate_funs=aggregate_funs,
                                                                               peroids=peroids,
                                                                               time_unit=time_unit)
                kpi_master = kpi_master.merge(kpi_groupped_rolled_shift, how='left', on=group_variables)

        return kpi_master

    def create_scaled_variables(self, data, predicted_var):
        data[predicted_var + '_median__ShopNo_20d_shift_1y_scaled_weekday'] = \
        [i*j for i, j in zip(data[predicted_var + '_median__ShopNo_20d_shift_1_y_0_d'], data['N_weekdays_p_1y'])]
        data[predicted_var + '_median__ShopNo_20d_shift_1y_scaled_weekend'] = \
        [i*j for i, j in zip(data[predicted_var+'_median__ShopNo_20d_shift_1_y_0_d'], data['N_weekends_p_1y'])]
        data[predicted_var + '_median__ShopNo_20d_shift_1y_scaled_workday'] = \
        [i*j for i, j in zip(data[predicted_var+'_median__ShopNo_20d_shift_1_y_0_d'], data['work_days_pp_1y'])]

        return data

    def create_master_data_monthly(self, kpi, features, group_times, group_shops, aggregate_funs, peroids,
                                   time_unit='day'):

        data = self.create_grouped_rolling_data_all_monthly(kpi, features, group_times, group_shops,
                                                            min_peroids=peroids, aggregate_funs=aggregate_funs,
                                                            time_unit=time_unit)
        data = self.create_scaled_variables(data, self.features[0])
        return data

    def create_and_return_features(self, kpi):
        if self.time_unit == 'day':
            kpi_master = self.create_master_data_daily(kpi, self.features, self.group_times, self.group_shops,
                                                       self.aggregate_funs, self.peroids, self.time_unit)
        else:
            kpi_master = self.create_master_data_monthly(kpi, self.features, self.group_times, self.group_shops,
                                                         aggregate_funs=self.aggregate_funs, peroids=self.peroids,
                                                         time_unit=self.time_unit)

        return kpi_master
