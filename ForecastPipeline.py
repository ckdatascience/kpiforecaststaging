import datetime
import numpy as np
import os
import pandas as pd
from functools import reduce
from kpi_forecast.models.ensemble_model import EnsembleModel
from kpi_forecast.processors import data_processor as data_proc
from kpi_forecast.processors import kpi_calculator as kpi
from kpi_forecast.processors.data_parser import DataParser
from kpi_forecast.processors.feature_engineer import FeatureEngineer
from kpi_forecast.utils.logger import initialize_csv_logger
from kpi_forecast.utils.data_patcher import DataPatcher
from kpi_forecast.utils.future_data_creator import FutureDataCreator
from kpi_forecast.utils.json_parser import read_json, write_json

if __name__ == '__main__':
    # Define parameters
    parameters = {
        'day': {
            'output_variables': [
                'SoldQty', 'SoldQtyExReturns', 'TotalGrossPrice', 'TotalLandedCost', 'TotalNetPrice', 'Traffic', 'TransactionsExReturns'
            ],
            'train_start_dates': ['2015-01-01', '2015-04-01', '2015-07-01', '2015-10-01'],
            'test_start_date': '2019-01-01',
            'train_duration_years': 3,
            'total_train_dates': 4,
            'train_interval_months': 3,
            'validate_duration_months': 3,
            'test_duration': 90,
            'prediction_duration': 90,
            'max_features': 50
        },
        'month': {
            'output_variables': [
                'SoldQty', 'SoldQtyExReturns', 'TotalGrossPrice', 'TotalLandedCost', 'TotalNetPrice', 'Traffic', 'TransactionsExReturns',
                'COGS', 'Depreciation', 'OfficeFixedCost', 'OutletExpenditure', 'PUB', 'Rental', 'StaffPayrollCPF'
            ],
            'train_start_dates': ['2015-01-01', '2015-04-01', '2015-07-01', '2015-10-01'],
            'test_start_date': '2019-01-01',
            'train_duration_years': 3,
            'total_train_dates': 4,
            'train_interval_months': 3,
            'validate_duration_months': 3,
            'test_duration': 3,
            'prediction_duration': 12,
            'max_features': 50
        },
        'data': {
            'integer_variables': ['SoldQty', 'SoldQtyExReturns', 'TransactionsExReturns', 'Traffic'],
            'index_columns': ['Date'],
            'country': 'Singapore',
            'input_csv_header': 'data/raw/cks_retail_data',
            'current_shops_csv': 'data/raw/cks_current_shops.csv',
            'time_unit': 'month'
        },
        'model': {
            'model_names': ['lightgbm'],
            'parameters': {
                'setting': {
                    'is_incremental': True,
                    'is_ensemble': False,
                    'ensemble_name': 'ensemble',
                    'feature_selection_method': 'permutation',
                    'hyperparameter_tuning_methods': ['bayesian'],
                    'confidence_interval': 0.9,
                    'logger_filename': 'log.csv'
                }
            }
        },
        'operation': {
            'load_trained_models': False,
            'per_shop_forecast': False,
            'save_engineered_data': True,
            'feature_selection': True,
            'save_feature_importance': True,
            'hyperparameter_tuning': True,
            'save_model_parameters': True,
            'model_training': True,
            'estimate_prediction_intervals': True,
            'save_trained_model': True,
            'calculate_all_variables': True,
            'save_predictions': True,
            'summarise_for_total': True
        }
    }

    # Initialize CSV logger
    datetime_now = datetime.datetime.now()
    parameters['model']['parameters']['setting']['logger_filename'] = 'outputs/logs/' + datetime_now.strftime('%Y-%m-%d %H.%M.%S') + '.csv'
    logger = initialize_csv_logger(parameters['model']['parameters']['setting']['logger_filename'])

    # Generate the train and test starting dates for data partitioning purpose
    today_date = datetime_now.strftime('%Y-%m-%d')
    train_start_dates, test_start_date = data_proc.generate_train_test_dates(
        today_date,
        parameters[parameters['data']['time_unit']]['train_duration_years'],
        parameters[parameters['data']['time_unit']]['total_train_dates'],
        parameters[parameters['data']['time_unit']]['train_interval_months'],
        parameters[parameters['data']['time_unit']]['validate_duration_months'],
        parameters[parameters['data']['time_unit']]['test_duration'],
        parameters['data']['time_unit']
    )
    parameters[parameters['data']['time_unit']]['train_start_dates'] = train_start_dates
    parameters[parameters['data']['time_unit']]['test_start_date'] = test_start_date

    # Obtain daily raw data from SQL database or Excel spreadsheet
    input_csv = parameters['data']['input_csv_header'] + '_' + today_date + '.csv'
    if os.path.isfile(input_csv):
        # Read raw data from Excel spreadsheet
        logger.info("Reading the raw data: %s ..." % input_csv)
        raw_data = pd.read_csv(input_csv, index_col=False)
    else:
        # Perform SQL query from database
        logger.info('Performing the SQL query to obtain the data on %s ...' % today_date)
        data_parser = DataParser()
        raw_data = data_parser.get_data(
            parameters[parameters['data']['time_unit']]['train_start_dates'][0], today_date, parameters['data']['country'], ['day']
        )['day']
        # Saving the raw data
        logger.info("Saving the raw data: %s ..." % input_csv)
        raw_data.to_csv(input_csv, index=False)
    raw_input_columns = list(raw_data.columns.values)

    # Create empty dataframes for historical data and empty future prediction dataframes if the corresponding files do not exist
    logger.info("Creating or reading the historical and future dataframes ...")
    historical_csv_filenames = {}
    future_csv_filenames = {}
    historical_dataframes = {}
    future_dataframes = {}
    historical_csv_header = 'outputs/spreadsheets/' + parameters['data']['country'] + '_historical_prediction'
    future_csv_header = 'outputs/spreadsheets/' + parameters['data']['country'] + '_future_prediction'
    model_list = parameters['model']['model_names']
    if parameters['model']['parameters']['setting']['is_ensemble'] == True:
        model_list += [parameters['model']['parameters']['setting']['ensemble_name']]
    for model in model_list:
        historical_csv_filenames[model] = historical_csv_header + '_' + model + '_' + parameters['data']['time_unit'] + '.csv'
        future_csv_filenames[model] = future_csv_header + '_' + model + '_' + parameters['data']['time_unit'] + '.csv'
        if os.path.isfile(historical_csv_filenames[model]):
            logger.info("Reading the historical prediction results: %s ..." % historical_csv_filenames[model])
            historical_dataframes[model] = pd.read_csv(historical_csv_filenames[model], index_col=False)
        else:
            historical_dataframes[model] = pd.DataFrame()
        if os.path.isfile(future_csv_filenames[model]):
            logger.info("Reading the future prediction results: %s ..." % future_csv_filenames[model])
            future_dataframes[model] = pd.read_csv(future_csv_filenames[model], index_col=False)
        else:
            future_dataframes[model] = pd.DataFrame()
    historical_columns_list = list(reduce(
        set.intersection, [set(item) for item in [list(historical_dataframes[model]) for model in model_list]]
    ))
    future_columns_list = list(reduce(
        set.intersection, [set(item) for item in [list(future_dataframes[model]) for model in model_list]]
    ))
    historical_columns_list = [column.replace('_Predicted', '') for column in historical_columns_list if '_Predicted' in column]
    future_columns_list = [column.replace('_Predicted', '') for column in future_columns_list if '_Predicted' in column]
    all_columns_list = [value for value in historical_columns_list if value in future_columns_list]

    # Instantiate data processors
    data_patcher = DataPatcher()
    future_data_creator = FutureDataCreator()
    feature_engineer = FeatureEngineer()

    # Instantiate variables used for data processing and partitioning
    data = {}
    labels = {}
    train_data = {}
    train_labels = {}
    validate_data = {}
    validate_labels = {}
    test_data = {}
    test_labels = {}
    ensemble_models = {}
    selected_features = {}
    feature_importances = {}
    model_parameters = {}
    historical_predictions = {}
    future_predictions = {}
    historical_lower_predictions = {}
    historical_upper_predictions = {}
    future_lower_predictions = {}
    future_upper_predictions = {}

    for output_variable in parameters[parameters['data']['time_unit']]['output_variables']:
        engineered_data_csv = 'data/processed/engineered_data_' + output_variable + '_' + parameters['data']['time_unit'] + '.csv'
        future_data_csv = 'data/processed/future_data_' + output_variable + '_' + parameters['data']['time_unit'] + '.csv'
        # Read feature engineered data from Excel spreadsheets
        if os.path.isfile(engineered_data_csv) and os.path.isfile(future_data_csv):
            # Read engineered and future data in CSV format
            logger.info("Reading the engineered data: %s ..." % engineered_data_csv)
            engineered_data = pd.read_csv(engineered_data_csv, index_col=False)
            logger.info("Reading the future data: %s ..." % future_data_csv)
            future_data = pd.read_csv(future_data_csv, index_col=False)
        # Perform feature engineering
        else:
            patched_data = raw_data.copy()
            # TODO: Add mising data correctly based on the time unit
            # Add data rows with missing dates
            patched_data = data_patcher.add_missing_data(patched_data)
            # Create and add future data to the patched data        
            patched_data = future_data_creator.add_future_data(
                parameters['data']['current_shops_csv'],
                duration=parameters[parameters['data']['time_unit']]['prediction_duration'], data=patched_data,
                time_unit=parameters['data']['time_unit']
            )
            # Perform feature engineering
            logger.info("Performing feature engineering ...")
            engineered_data = feature_engineer.perform_feature_engineering(
            	patched_data, output_variable, parameters['data']['country'], time_unit=parameters['data']['time_unit']
           	)
            # Split engineered data into historical and future data
            engineered_data, future_data = data_proc.split_data_by_duration(
                engineered_data, parameters[parameters['data']['time_unit']]['prediction_duration'], time_unit=parameters['data']['time_unit']
            )
            if parameters['operation']['save_engineered_data'] == True:
                logger.info("Saving the feature engineered historical data: %s ..." % engineered_data_csv)
                engineered_data.to_csv(engineered_data_csv, index=False)
                logger.info("Saving the feature engineered future data: %s ..." % future_data_csv)
                future_data.to_csv(future_data_csv, index=False)

        # Get categorical features from the data
        # TODO: Replace workaround with permanent solution
        logger.info("Extracting the categorical features from the data ...")
        input_columns = [col for col in raw_input_columns if col in engineered_data.columns]
        input_columns_filter = [column for column in input_columns if
                                column not in parameters['data']['index_columns']]
        categorical_features = data_proc.get_categorial_features(engineered_data.drop(input_columns, axis=1))

        # Prepare data
        logger.info("Preparing the data ...")
        current_shop_list = pd.read_csv(parameters['data']['current_shops_csv'], index_col=False)['ShopNo'].astype(str).unique().tolist()
        engineered_shop_list = engineered_data['ShopNo'].astype(str).unique().tolist()
        shop_list = sorted([value for value in current_shop_list if value in engineered_shop_list])
        data.clear()
        labels.clear()
        if parameters['operation']['per_shop_forecast'] == True:
            for shop in shop_list:
                shop_data = engineered_data[engineered_data['ShopNo'].astype(str) == shop]
                data[shop] = shop_data.drop(input_columns_filter, axis=1).reset_index(drop=True)
                labels[shop] = shop_data[[output_variable]].reset_index(drop=True)
        else:
            data['All'] = engineered_data.drop(input_columns_filter, axis=1).reset_index(drop=True)
            labels['All'] = engineered_data[[output_variable]].reset_index(drop=True)

        # Generate training, validation, and testing data
        train_data.clear()
        train_labels.clear()
        validate_data.clear()
        validate_labels.clear()
        test_data.clear()
        test_labels.clear()
        for key in data.keys():
            logger.info("Generating the training-validation-testing data for %s from dataset: %s ..." % (output_variable, key))
            train_data[key], train_labels[key], validate_data[key], validate_labels[key], test_data[key], test_labels[key] = data_proc.generate_dataset_by_date(
                data[key],
                labels[key], 
                parameters['data']['index_columns'],
                is_incremental=parameters['model']['parameters']['setting']['is_incremental'],
                train_start_dates=parameters[parameters['data']['time_unit']]['train_start_dates'],
                test_start_date=parameters[parameters['data']['time_unit']]['test_start_date'], 
                train_duration_years=parameters[parameters['data']['time_unit']]['train_duration_years'], 
                test_duration=parameters[parameters['data']['time_unit']]['test_duration'],
                time_unit=parameters['data']['time_unit']
            )

        # Initialize the models
        model_identifier = output_variable + '_' + parameters['data']['time_unit']
        ensemble_models.clear()
        print('Initialize the models')
        for key in data.keys():
            ensemble_models[key] = EnsembleModel(parameters['model']['model_names'], parameters['model']['parameters'])
            # Load the trained model(s) from file(s)
            if parameters['operation']['load_trained_models'] == True:
                logger.info("Loading the trained model(s) for %s on dataset: %s ..." % (output_variable, key))
                ensemble_models[key].load_models(model_identifier)
                if parameters['operation']['estimate_prediction_intervals'] == True:
                    logger.info("Loading the trained quantile regression models for %s on dataset: %s ..." % (output_variable, key))
                    ensemble_models[key].load_quantile_models(model_identifier)

        # Perform feature selection
        if parameters['operation']['feature_selection'] == True:
            selected_features.clear()
            feature_importances.clear()
            for key in data.keys():
                feature_importance_csv = 'outputs/spreadsheets/feature_importance_' + parameters['model']['parameters']['setting']['feature_selection_method'] + '_' + output_variable + '_' + key + '_' + parameters['data']['time_unit'] + '.csv'
                if os.path.isfile(feature_importance_csv):
                    logger.info("Reading the feature importance data: %s ..." % feature_importance_csv)
                    feature_importances[key] = pd.read_csv(feature_importance_csv, index_col=False)
                else:
                    # Select features based on the feature importance
                    logger.info("Studying the feature importance for %s on dataset: %s ..." % (output_variable, key))
                    feature_importances[key] = ensemble_models[key].get_feature_importance(
                        output_variable,
                        train_data[key],
                        train_labels[key],
                        categorical_features,
                        validate_data[key],
                        validate_labels[key],
                        identifier=(key + ' - ' + parameters['data']['time_unit']),
                        max_features=parameters[parameters['data']['time_unit']]['max_features'],
                        aggregate=True,
                        plot_figure=True
                    )['ensemble']
                    # Save the feature importance into a CSV file
                    if parameters['operation']['save_feature_importance'] == True:
                        logger.info("Saving the feature importance data: %s ..." % feature_importance_csv)
                        feature_importances[key].to_csv(feature_importance_csv, index=False)
                # Filter out features with negative scores
                feature_importances[key] = feature_importances[key][feature_importances[key]['Score'] > 0]
                if parameters[parameters['data']['time_unit']]['max_features'] > 0 and parameters[parameters['data']['time_unit']]['max_features'] < feature_importances[key].shape[0]:
                    total_features = parameters[parameters['data']['time_unit']]['max_features']
                else:
                    total_features = feature_importances[key].shape[0]
                selected_features[key] = feature_importances[key].head(total_features)['Feature'].values
                # Filter out less important features
                data[key] = data[key][np.append(selected_features[key], parameters['data']['index_columns'])]
                # Generate training, validation, and testing data based on the pruned dataset with selected features
                logger.info("Generating the training-validation-testing data for %s from dataset: %s ..." % (output_variable, key))
                train_data[key], train_labels[key], validate_data[key], validate_labels[key], test_data[key], test_labels[key] = data_proc.generate_dataset_by_date(
                    data[key],
                    labels[key],
                    parameters['data']['index_columns'], 
                    is_incremental=parameters['model']['parameters']['setting']['is_incremental'], 
                    train_start_dates=parameters[parameters['data']['time_unit']]['train_start_dates'], 
                    test_start_date=parameters[parameters['data']['time_unit']]['test_start_date'], 
                    train_duration_years=parameters[parameters['data']['time_unit']]['train_duration_years'], 
                    test_duration=parameters[parameters['data']['time_unit']]['test_duration'],
                    time_unit=parameters['data']['time_unit']
                )

        # Perform hyperparameter tuning
        if parameters['operation']['hyperparameter_tuning'] == True:
            model_parameters.clear()
            for key in data.keys():
                model_parameters_json = 'models/model_parameters_' + key + '_' + output_variable + '_' + parameters['data']['time_unit'] + '.json'
                if os.path.isfile(model_parameters_json):
                    logger.info("Reading the tuned model parameters: %s ..." % model_parameters_json)
                    model_parameters[key] = read_json(model_parameters_json)
                else:
                    logger.info("Performing hyperparameter tuning for %s on dataset: %s ..." % (output_variable, key))
                    model_parameters[key] = ensemble_models[key].tune_hyperparameters(
                        train_data[key], 
                        train_labels[key],
                        categorical_features,
                        validate_data[key], 
                        validate_labels[key]
                    )
                    if parameters['operation']['save_model_parameters'] == True:
                        logger.info("Saving the tuned model parameters: %s ..." % model_parameters_json)
                        write_json(model_parameters[key], model_parameters_json)
                # Reinitialize model(s) based on the tuned parameters
                logger.info("Reinitializing the model(s) based on the tuned model parameters ...")
                print('Reinitialize model(s) based on the tuned parameters')
                model_parameters[key].update(parameters['model']['parameters'])
                ensemble_models[key] = EnsembleModel(parameters['model']['model_names'], model_parameters[key])

        # Perform model training
        if parameters['operation']['model_training'] == True and not output_variable in all_columns_list:
            is_integer = True if output_variable in parameters['data']['integer_variables'] else False
            historical_shops = []
            historical_dates = []
            historical_labels = []
            historical_predictions.clear()
            future_shops = []
            future_dates = []
            future_predictions.clear()
            for model in model_list:
                historical_predictions[model] = []
                future_predictions[model] = []
            # Create empty variables for prediction intervals
            if parameters['operation']['estimate_prediction_intervals'] == True:
                historical_lower_predictions.clear()
                historical_upper_predictions.clear()
                future_lower_predictions.clear()
                future_upper_predictions.clear()
                for model in model_list:
                    historical_lower_predictions[model] = []
                    historical_upper_predictions[model] = []
                    future_lower_predictions[model] = []
                    future_upper_predictions[model] = []
            for key in data.keys():
                if parameters['operation']['load_trained_models'] == False:
                    # Train the model(s)
                    logger.info("Training the model for %s on dataset: %s ..." % (output_variable, key))
                    ensemble_models[key].train(
                        train_data[key],
                        train_labels[key],
                        categorical_features,
                        validate_data[key],
                        validate_labels[key],
                        parameters['operation']['save_trained_model'],
                        model_identifier
                    )
                    # Train the quantile regression model(s)
                    if parameters['operation']['estimate_prediction_intervals'] == True:
                        logger.info("Training the quantile regression models for %s on dataset: %s ..." % (output_variable, key))
                        ensemble_models[key].train_quantile(
                            train_data[key],
                            train_labels[key],
                            categorical_features,
                            validate_data[key],
                            validate_labels[key],
                            parameters['operation']['save_trained_model'],
                            model_identifier
                        )

                # Plot the performance metrics of the trained model(s)
                logger.info("Plotting the performance metrics of the trained model(s) for %s on dataset: %s ..." % (output_variable, key))
                ensemble_models[key].plot_train_validate_test_metrics(
                    train_data[key],
                    train_labels[key],
                    validate_data[key],
                    validate_labels[key],
                    test_data[key],
                    test_labels[key],
                    '[%s] Model performance metrics (%s - %s)' % (output_variable, key, parameters['data']['time_unit'])
                )
                
                # Plot the output predictions for all data (including testing data)
                if not output_variable in historical_columns_list:
                    # Plot the output predictions for all data
                    logger.info("Plotting the output predictions of all historical data for %s on dataset: %s ..." % (output_variable, key))
                    if key != 'All':                        
                        all_shop_data = data[key]
                        all_shop_labels = labels[key]
                        lower_quantiles = None
                        upper_quantiles = None
                        # Perform predictions on the data
                        outputs = ensemble_models[key].predict(all_shop_data.drop(['Date'], axis=1))
                        historical_shops.extend([key] * all_shop_data.shape[0])
                        historical_dates.extend(all_shop_data['Date'])
                        historical_labels.extend(all_shop_labels[output_variable])
                        for model in outputs.keys():
                            outputs[model] = data_proc.postprocess_outputs(outputs[model], is_integer)
                            historical_predictions[model].extend(outputs[model])
                        # Generate prediction intervals
                        if parameters['operation']['estimate_prediction_intervals'] == True:
                            lower_quantiles, upper_quantiles = ensemble_models[key].predict_quantile(all_shop_data.drop(['Date'], axis=1))
                            for model in lower_quantiles.keys():
                                lower_quantiles[model] = data_proc.postprocess_outputs(lower_quantiles[model], is_integer)
                                upper_quantiles[model] = data_proc.postprocess_outputs(upper_quantiles[model], is_integer)
                                historical_lower_predictions[model].extend(lower_quantiles[model])
                                historical_upper_predictions[model].extend(upper_quantiles[model])
                        # Plot the data and outputs
                        ensemble_models[key].plot_data(
                            all_shop_data,
                            all_shop_labels,
                            outputs,
                            '[%s] Prediction results - all data (%s - %s)' % (output_variable, key, parameters['data']['time_unit']),
                            output_variable,
                            lower_quantiles,
                            upper_quantiles
                        )
                    else:
                        for shop in shop_list:
                            shop_data = engineered_data[engineered_data['ShopNo'].astype(str) == shop]
                            shop_labels = shop_data[[output_variable]]
                            if parameters['operation']['feature_selection'] == True:
                                input_data = shop_data[selected_features['All']]
                            else:
                                input_data = shop_data.drop(input_columns, axis=1)
                            lower_quantiles = None
                            upper_quantiles = None
                            # Perform predictions on the data
                            outputs = ensemble_models[key].predict(input_data)
                            historical_shops.extend(shop_data['ShopNo'])
                            historical_dates.extend(shop_data['Date'])
                            historical_labels.extend(shop_labels[output_variable])
                            for model in outputs.keys():
                                outputs[model] = data_proc.postprocess_outputs(outputs[model], is_integer)
                                historical_predictions[model].extend(outputs[model])
                            # Generate prediction intervals
                            if parameters['operation']['estimate_prediction_intervals'] == True:
                                lower_quantiles, upper_quantiles = ensemble_models[key].predict_quantile(input_data)
                                for model in lower_quantiles.keys():
                                    lower_quantiles[model] = data_proc.postprocess_outputs(lower_quantiles[model], is_integer)
                                    upper_quantiles[model] = data_proc.postprocess_outputs(upper_quantiles[model], is_integer)
                                    historical_lower_predictions[model].extend(lower_quantiles[model])
                                    historical_upper_predictions[model].extend(upper_quantiles[model])
                            # Plot the data and outputs
                            ensemble_models[key].plot_data(
                                shop_data,
                                shop_labels,
                                outputs,
                                '[%s] Prediction results - all data (%s - %s)' % (output_variable, shop, parameters['data']['time_unit']),
                                output_variable,
                                lower_quantiles,
                                upper_quantiles
                            )
                    # Plot the output predictions for testing data
                    logger.info("Plotting the output predictions of testing data for %s on dataset: %s ..." % (output_variable, key))
                    if key != 'All':
                        lower_quantiles = None
                        upper_quantiles = None
                        # Perform predictions on the data
                        outputs = ensemble_models[key].predict(test_data[key])                    
                        for model in outputs.keys():
                            outputs[model] = data_proc.postprocess_outputs(outputs[model], is_integer)
                        # Generate prediction intervals
                        if parameters['operation']['estimate_prediction_intervals'] == True:
                            lower_quantiles, upper_quantiles = ensemble_models[key].predict_quantile(test_data[key])
                            for model in outputs.keys():
                                lower_quantiles[model] = data_proc.postprocess_outputs(lower_quantiles[model], is_integer)
                                upper_quantiles[model] = data_proc.postprocess_outputs(upper_quantiles[model], is_integer)
                        # Plot the data and outputs
                        ensemble_models[key].plot_data(
                            test_data[key].assign(
                                Date=data_proc.crop_dataset(data[key], parameters[parameters['data']['time_unit']]['test_start_date'])['Date']
                            ),
                            test_labels[key],
                            outputs,
                            '[%s] Prediction results - testing data (%s - %s)' % (output_variable, key, parameters['data']['time_unit']),
                            output_variable,
                            lower_quantiles,
                            upper_quantiles
                        )
                    else:
                        for shop in shop_list:
                            shop_data = data_proc.crop_dataset(
                                engineered_data[engineered_data['ShopNo'].astype(str) == shop], 
                                parameters[parameters['data']['time_unit']]['test_start_date']
                            )
                            shop_labels = shop_data[[output_variable]]
                            if parameters['operation']['feature_selection'] == True:
                                input_data = shop_data[selected_features['All']]
                            else:
                                input_data = shop_data.drop(input_columns, axis=1)
                            lower_quantiles = None
                            upper_quantiles = None
                            # Perform predictions on the data
                            outputs = ensemble_models[key].predict(input_data)
                            for model in outputs.keys():
                                outputs[model] = data_proc.postprocess_outputs(outputs[model], is_integer)
                            # Generate prediction intervals
                            if parameters['operation']['estimate_prediction_intervals'] == True:
                                lower_quantiles, upper_quantiles = ensemble_models[key].predict_quantile(input_data)
                                for model in lower_quantiles.keys():
                                    lower_quantiles[model] = data_proc.postprocess_outputs(lower_quantiles[model], is_integer)
                                    upper_quantiles[model] = data_proc.postprocess_outputs(upper_quantiles[model], is_integer)
                            # Plot the data and outputs
                            ensemble_models[key].plot_data(
                                shop_data,
                                shop_labels,
                                outputs,
                                '[%s] Prediction results - testing data (%s - %s)' % (output_variable, shop, parameters['data']['time_unit']),
                                output_variable,
                                lower_quantiles,
                                upper_quantiles
                            )
                # Plot the output predictions for future data
                if not output_variable in future_columns_list:
                    logger.info("Plotting the output predictions of future data for %s on dataset: %s ..." % (output_variable, key))
                    if key != 'All':
                        shop_data = future_data[future_data['ShopNo'].astype(str) == key]
                        shop_labels = shop_data[[output_variable]]
                        if parameters['operation']['feature_selection'] == True:
                            input_data = shop_data[selected_features[key]]
                        else:
                            input_data = shop_data.drop(input_columns, axis=1)
                        lower_quantiles = None
                        upper_quantiles = None
                        # Perform predictions on the data
                        outputs = ensemble_models[key].predict(input_data)
                        future_shops.extend(shop_data['ShopNo'])
                        future_dates.extend(shop_data['Date'])
                        for model in outputs.keys():
                            outputs[model] = data_proc.postprocess_outputs(outputs[model], is_integer)
                            future_predictions[model].extend(outputs[model])
                        # Generate prediction intervals
                        if parameters['operation']['estimate_prediction_intervals'] == True:
                            lower_quantiles, upper_quantiles = ensemble_models[key].predict_quantile(input_data)
                            for model in lower_quantiles.keys():
                                lower_quantiles[model] = data_proc.postprocess_outputs(lower_quantiles[model], is_integer)
                                upper_quantiles[model] = data_proc.postprocess_outputs(upper_quantiles[model], is_integer)
                                future_lower_predictions[model].extend(lower_quantiles[model])
                                future_upper_predictions[model].extend(upper_quantiles[model])
                        ensemble_models[key].plot_data(
                            shop_data,
                            None,
                            outputs,
                            '[%s] Prediction results - future data (%s - %s)' % (output_variable, key, parameters['data']['time_unit']),
                            output_variable,
                            lower_quantiles,
                            upper_quantiles
                        )
                    else:
                        for shop in shop_list:
                            shop_data = future_data[future_data['ShopNo'].astype(str) == shop]
                            shop_labels = shop_data[[output_variable]]
                            if parameters['operation']['feature_selection'] == True:
                                input_data = shop_data[selected_features['All']]
                            else:
                                input_data = shop_data.drop(input_columns, axis=1)
                            lower_quantiles = None
                            upper_quantiles = None
                            # Perform predictions on the data
                            outputs = ensemble_models[key].predict(input_data)
                            future_shops.extend(shop_data['ShopNo'])
                            future_dates.extend(shop_data['Date'])
                            for model in outputs.keys():
                                outputs[model] = data_proc.postprocess_outputs(outputs[model], is_integer)
                                future_predictions[model].extend(outputs[model])
                            # Generate prediction intervals
                            if parameters['operation']['estimate_prediction_intervals'] == True:
                                lower_quantiles, upper_quantiles = ensemble_models[key].predict_quantile(input_data)
                                for model in outputs.keys():
                                    lower_quantiles[model] = data_proc.postprocess_outputs(lower_quantiles[model], is_integer)
                                    upper_quantiles[model] = data_proc.postprocess_outputs(upper_quantiles[model], is_integer)
                                    future_lower_predictions[model].extend(lower_quantiles[model])
                                    future_upper_predictions[model].extend(upper_quantiles[model])
                            # Plot the data and outputs
                            ensemble_models[key].plot_data(
                                shop_data,
                                None,
                                outputs,
                                '[%s] Prediction results - future data (%s - %s)' % (output_variable, shop, parameters['data']['time_unit']),
                                output_variable,
                                lower_quantiles,
                                upper_quantiles
                            )

            # Assign values to the dataframes for historical data
            if not output_variable in historical_columns_list:
                for model in historical_dataframes.keys():
                    if not 'ShopNo' in historical_dataframes[model].columns:
                        historical_dataframes[model]['ShopNo'] = historical_shops
                    if not 'Date' in historical_dataframes[model].columns:
                        historical_dataframes[model]['Date'] = historical_dates
                    if not (output_variable + '_Actual') in historical_dataframes[model].columns:
                        historical_dataframes[model][output_variable + '_Actual'] = historical_labels
                    historical_dataframes[model][output_variable + '_Predicted'] = historical_predictions[model]
                    if parameters['operation']['estimate_prediction_intervals'] == True:
                        historical_dataframes[model][output_variable + '_Predicted_Lower'] = historical_lower_predictions[model]
                        historical_dataframes[model][output_variable + '_Predicted_Upper'] = historical_upper_predictions[model]
            # Assign values to the future prediction dataframes
            if not output_variable in future_columns_list:
                for model in future_dataframes.keys():
                    if not 'ShopNo' in future_dataframes[model].columns:
                        future_dataframes[model]['ShopNo'] = future_shops
                    if not 'Date' in future_dataframes[model].columns:
                        future_dataframes[model]['Date'] = future_dates
                    future_dataframes[model][output_variable + '_Predicted'] = future_predictions[model]
                    if parameters['operation']['estimate_prediction_intervals'] == True:
                        future_dataframes[model][output_variable + '_Predicted_Lower'] = future_lower_predictions[model]
                        future_dataframes[model][output_variable + '_Predicted_Upper'] = future_upper_predictions[model]

    # Add total summarised values to the historical and future dataframes
    if parameters['operation']['summarise_for_total'] == True:
        logger.info("Summarising predicted kpis for total ... ")
        for model in historical_dataframes.keys():
            historical_dataframes[model] = kpi.add_total_summarised_data(historical_dataframes[model])
            future_dataframes[model] = kpi.add_total_summarised_data(future_dataframes[model])

    # Calculate other variables based on the predicted output variables
    if parameters['operation']['calculate_all_variables'] == True:
        logger.info("Calculating other variables based on the predicted output variables ...")
        for model in historical_dataframes.keys():
            if 'day' in parameters['data']['time_unit'].lower():
                historical_dataframes[model] = kpi.calc_daily_kpis(historical_dataframes[model], '_Actual')
                historical_dataframes[model] = kpi.calc_daily_kpis(historical_dataframes[model], '_Predicted')
                future_dataframes[model] = kpi.calc_daily_kpis(future_dataframes[model], '_Predicted')
            elif 'month' in parameters['data']['time_unit'].lower():
                historical_dataframes[model] = kpi.calc_monthly_kpis(historical_dataframes[model], '_Actual')
                historical_dataframes[model] = kpi.calc_monthly_kpis(historical_dataframes[model], '_Predicted')
                future_dataframes[model] = kpi.calc_monthly_kpis(future_dataframes[model], '_Predicted')

    # Save the prediction results to the Excel spreadsheets
    if parameters['operation']['save_predictions'] == True:
        logger.info("Saving the prediction results to Excel spreadsheets ...")
        for model in historical_dataframes.keys():
            historical_dataframes[model].to_csv(historical_csv_filenames[model], index=False)
            future_dataframes[model].to_csv(future_csv_filenames[model], index=False)
